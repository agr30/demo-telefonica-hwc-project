import config from '../../config';
import { logError } from './log';
import { methods } from 'csb-price-server-orm';

/**
 * Saves a function with a public id and returns a closure to put additional
 * data based on that id.
 * @param {Object} Entity The database entity.
 * @param {Object} data The data to save.
 * @param {Function} A function where put additional data.
 */
function saveWithId(Entity, data) {
  data = methods.idGenerate(data);
  return (additionalData = {}) => {
    Object.assign(data, additionalData);
    return methods.save(Entity, data).catch(logError);
  };
}

/**
 * Saves a function with an href based on the id.
 * @param {Object} Entity The database entity.
 * @param {Object} data The data to save.
 * @param {String} startDateTime The startDateTime as ISO String.
 * @returns {Promise} The save promise.
 */
export function saveWithHref(Entity, data, startDateTime) {
  return saveWithId(Entity, data)({
    validFor: { startDateTime },
    href: `${config.hrefBasePath}${Entity.collection.collectionName}/${data.id}`
  }).catch(logError);
}

/**
* Deletes all entities using an optional select.
* @param {String?} select Select to delete entity.
* @return {Promise} The promise that all documents have been removed.
*/
export function deleteEntity(Entity, select) {
  return methods.remove(Entity, select).catch(logError);
}
