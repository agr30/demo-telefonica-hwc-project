import cli from '../';

/**
 * Logs an error to CLI.
 * @param {String} error
 */
export function logError(error) {
  console.log(error);
  error && cli.log(cli.chalk.red('ERROR'), error);
}
