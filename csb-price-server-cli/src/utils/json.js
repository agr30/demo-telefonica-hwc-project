/**
 * Creates a new property into record and unflattens it.
 * @param {Object} record The record object.
 * @param {String} property The property header to be created.
 * @param {Object} value The value to be stored by new property.
 * @param {Object} delimiter The property delimiter.
 */
export function createUnflattenedProperty(record, property, value, delimiter) {
  record[property] = value;
  unflatten(record, property, delimiter);
}

/**
 * Unflattens a delimiter separated header to be converted into JSON.
 * @param {Object} record The record object.
 * @param {String} header The flattened header to be converted into JSON.
 * @param {String} delimiter The property delimiter.
 */
export function unflatten(record, header, delimiter) {
  const properties = header.split(delimiter);

  if (record[header] !== '') {
    properties.reduce((prev, head, propIdx, headerArray) => {
      if (propIdx === headerArray.length - 1) {
        // Assign property if it's last one.
        prev[head] = record[header];
      } else {
        // Check if object exists or should be initialized as object or array.
        prev[head] = prev[head] || (isNaN(headerArray[propIdx + 1]) ? {} : []);
      }
      return prev[head];
    }, record);
  }
  // Delete flat record.
  if (properties.length > 1) {
    delete record[header];
  }
}
