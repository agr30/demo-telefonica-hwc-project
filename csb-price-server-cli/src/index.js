import 'babel-polyfill';

import addUser from './commands/add-user';
import convertCSV from './commands/convert-csv';
import currencyLast from './commands/currency-last';
import currencySync from './commands/currency-sync';
import logger from 'logops';
import offeringsBootstrap from './commands/offerings-bootstrap';
import offeringsDelta from './commands/offerings-delta';
import priceReboot from './commands/price-reboot';
import priceValidate from './commands/price-validate';
import productsBootstrap from './commands/products-bootstrap';
import productsDelta from './commands/products-delta';
import vorpal from 'vorpal';

const IS_CLI = process.env.CLI_ENV;
let cli, chalk;

if (IS_CLI) {
  // Start vorpal as usual
  cli = vorpal();
  chalk = cli.chalk;

  // Supercharge logging methods
  cli.debug = cli.warn = cli.info = cli.error = cli.fatal = cli.log;

  productsBootstrap(cli);
  offeringsBootstrap(cli);
  productsDelta(cli);
  offeringsDelta(cli);
  priceReboot(cli);
  addUser(cli);
  currencyLast(cli);
  currencySync(cli);
  priceValidate(cli);
  convertCSV(cli);

  cli.log(
    `Welcome to ${chalk.cyan('Price Server CLI')}, type ${chalk.cyan('"help"')} ` +
      `to see available commands!`
  );
} else {
  // Fake vorpal to beahave properly as outern script.
  const fakeChalk = str => str;
  chalk = {
    cyan: fakeChalk,
    magenta: fakeChalk,
    green: fakeChalk,
    red: fakeChalk
  };

  cli = {
    delimiter: () => {
      return {
        show: () => {
          return {
            debug: logger.info,
            warn: logger.info,
            log: logger.info,
            info: logger.info,
            error: logger.info,
            fatal: logger.fatal,
            chalk
          };
        }
      };
    }
  };
}

export default cli.delimiter(chalk.magenta('>')).show();
