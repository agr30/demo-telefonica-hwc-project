import { db, methods, models } from 'csb-price-server-orm';
import { saveWithHref } from '../utils/db';

import cli from '../';
import config from '../../config';
import fs from 'fs';
import { logError } from '../utils/log';
import parse from 'csv-parse';
import streamToPromise from 'stream-to-promise';
import { unflatten } from '../utils/json';

const Product = models.Product;
let PROPERTY_DELIMITER;
let manyUpdates = 0;

/**
 * Saves a product to database.
 * @param {Object} data The data to save.
 * @param {String} startDateTime The startDateTime as ISO String.
 * @returns {Promise} The save promise.
 */
function saveProduct(data, startDateTime) {
  return saveWithHref(Product, data, startDateTime);
}

/**
 * Checks if productSpecCharacteristic isPresent, in that case deletes existing
 * index data and stores notPresent into array to not save next index records.
 * @param {Object} record The record data.
 * @param {String} header The flattened header to be converted to JSON.
 * @param {Array<String>} notPresentProdSpecChars An array storing not present
 *  productSpecCharacteristics.
 */
function checkProdSpecIsPresent(record, header, notPresentProdSpecChars) {
  const indexMatch = header.match(/^productSpecCharacteristic\/(\d{1,2})\/.+$/),
    index = indexMatch[1];

  if (/^productSpecCharacteristic\/\d{1,2}\/isPresent$/.test(header)) {
    // Check if isPresent header has false value, and then add to notPresent
    // array and delete previous stored data on record.
    if (record[header] === 'false') {
      notPresentProdSpecChars.push(index);
      record.productSpecCharacteristic &&
        record.productSpecCharacteristic.splice(index, 1);
    }
  } else if (!notPresentProdSpecChars.includes(index)) {
    // If is not isPresent header and index is not on notPresent array,
    // unflatten header as usual.
    unflatten(record, header, PROPERTY_DELIMITER);
  }

  // Delete productSpecCharacteristic headers from record.
  delete record[header];
}

/**
 * Parses the CSV data into JSON and saves it to the collection.
 * @param {Object} record The key-values record object.
 * @param {String} startDateTime The startDateTime date in yyyy-MM-dd format.
 * @returns {Promise} The save promise.
 */
function csvToJson(record, startDateTime) {
  if (record) {
    const notPresentProdSpecChars = [];

    // Check id productSpecCharacteristic isPresent and unflatten as JSON other
    // columns.
    Object.keys(record).forEach(header => {
      if (header.startsWith('productSpecCharacteristic')) {
        checkProdSpecIsPresent(record, header, notPresentProdSpecChars);
      } else {
        unflatten(record, header, PROPERTY_DELIMITER);
      }
    });

    // Filter to remove null values due to isPresent logic.
    record.productSpecCharacteristic =
      record.productSpecCharacteristic &&
      record.productSpecCharacteristic.filter(value => value);

    // console.log('Parsed product', JSON.stringify(record));
    // Save product
    return updateServiceEntity(record, startDateTime);
  }
  return Promise.resolve();
}

/**
 * Updates all entities for categorized service.
 * @param {String} category The category to update.
 * @return {Promise} The promise that all documents have been updated.
 */
function updateServiceEntity(record, startDateTime) {
  return methods
    .find(Product, { name: record.name })
    .then(result => {
      if (Object.keys(result).length === 0) {
        manyUpdates += 1;
        return saveProduct(record, new Date(startDateTime).toISOString());
      }
      return Promise.resolve();
    })
    .catch(err => logError(err));
}

export default ({
  filePath,
  startDateTime,
  category,
  csvDelimiter = config.csvDelimiter,
  propertyDelimiter = config.propertyDelimiter,
  dbUri = config.dbUri
}) => {
  const parser = parse({
    delimiter: csvDelimiter,
    auto_parse: true,
    columns: true
  }),
    chalk = cli.chalk,
    cyan = chalk.cyan,
    green = chalk.green;

  // Catch any error.
  parser.on('error', error => {
    logError(error);
    db.disconnect();
  });

  return db.connect({ dbUri, dbOptions: config.dbOptions }, cli).then(() => {
    cli.log(`Initializing ${cyan(filePath)} CSV import...`);

    PROPERTY_DELIMITER = propertyDelimiter;

    // Parse CSV and save it to database.
    return streamToPromise(fs.createReadStream(filePath).pipe(parser))
      .then(records => {
        const obsLength =
          (config.billing && Object.keys(config.billing).length) || 0;

        cli.log(
          green(
            `${cyan(records.length)} records read from file ${cyan(filePath)}!`
          )
        );
        cli.log(green(`${cyan(obsLength)} OBs read from config file!`));

        // Empty category data.
        cli.log(
          green(
            `Updating existing categorized "${cyan(category)}" products and offerings data...`
          )
        );

        return Promise.all(
          records.map(record => csvToJson(record, startDateTime))
        ).then(savePromises => {
          cli.log(
            green(
              `CSV file ${cyan(filePath)} saved ${cyan(manyUpdates)} of ${cyan(savePromises.length)} records!`
            )
          );
        });
      })
      .then(db.disconnect)
      .catch(err => {
        logError(err);
        throw new Error();
      })
      .catch(db.disconnect);
  });
};
