import cli from '../';
import config from '../../config';
import { db } from 'csb-price-server-orm';

export default ({
  userName,
  password,
  role = 'read',
  dbUri = config.dbUri
}) => {
  const chalk = cli.chalk,
    cyan = chalk.cyan,
    red = chalk.red,
    green = chalk.green;

  return db
    .connect({ dbUri, dbOptions: config.dbOptions }, cli)
    .then(connection => {
      return connection.db.addUser(
        userName,
        password,
        {
          roles: [role]
        },
        error => {
          cli.log(
            error
              ? `${red('ERROR')} ${error}`
              : green(
                  `User ${cyan(userName)} added to db with ${cyan(role)} rights!`
                )
          );
          db.disconnect();
        }
      );
    });
};
