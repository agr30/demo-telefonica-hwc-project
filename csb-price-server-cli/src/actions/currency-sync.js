import { db, methods, models } from 'csb-price-server-orm';

import Q from 'q';
import cli from '../';
import config from '../../config';
import httpStatus from 'http-status-codes';
import moment from 'moment';
import request from 'request';

const Currency = models.Currency;

/**
 * Catchs an error, logs and disconnect db.
 * @param {String} error
 */
function catchError(error) {
  error &&
    cli.log(
      `${cli.chalk.red('ERROR')} ${error.message}${error.description ? `: ${error.description}` : ''}`
    );
  db.disconnect();
}

/**
 * Calls openexchangerates api.
 * @param {Object} options Options object with needed options to call the endpoint.
 * @return {Promise} Promise-
 */
function callApi(options) {
  let qs = {
    app_id: config.currencies.appId
  },
    defer = new Q.defer(),
    url = config.currencies.server + config.currencies.entities[options.entity];

  // If entity is historical, add date to url.
  if (options.entity === 'historical') {
    url += `/${options.dateHistorical}.json`;
  }
  request(
    {
      url: url,
      qs: qs,
      method: 'GET',
      headers: {
        Accept: 'application/json'
      }
    },
    (err, response, body) => {
      if (!err && response.statusCode === httpStatus.OK) {
        defer.resolve(JSON.parse(body));
      } else {
        defer.reject(err || JSON.parse(body));
      }
    }
  );

  return defer.promise;
}

/**
 * Checks if next date is valid.
 * @param {Moment} date Moment.js date
 * @return {boolean}
 */
function checkNext(date) {
  return date.isBefore(moment());
}

/**
 * Gets record requested (Mixed recursive).
 * @param {number} date Date in format 'YYYYMMDD'.
 * @param {Object} defer Deferred object.
 * @return {Promise} */
function getRecord(date, defer) {
  let _nextDate = moment(date, 'YYYYMMDD').add(1, 'd');

  Currency.findOne(
    {
      _id: date
    },
    (err, record) => {
      if (!record) {
        return callApi({
          entity: 'historical',
          dateHistorical: moment(date, 'YYYYMMDD').format('YYYY-MM-DD')
        })
          .then(result => {
            return methods.save(
              Currency,
              Object.assign({}, result, { _id: date })
            );
          })
          .then(() => {
            if (checkNext(_nextDate)) {
              getRecord(parseInt(moment(_nextDate).format('YYYYMMDD')), defer);
            } else {
              defer.resolve();
            }
          })
          .catch(error => {
            defer.reject(error);
          });
      } else if (checkNext(_nextDate)) {
        getRecord(parseInt(moment(_nextDate).format('YYYYMMDD')), defer);
      } else {
        defer.resolve();
      }
    }
  );

  return defer.promise;
}

export default ({ startDate, dbUri = config.dbUri }) => {
  return db
    .connect({ dbUri, dbOptions: config.dbOptions }, cli)
    .then(() => {
      let _start = parseInt(
        startDate
          ? moment(startDate, 'YYYY-MM-DD').format('YYYYMMDD')
          : moment(
              config.currencies.startRecordingDateByDefault,
              'YYYYMMDD'
            ).format('YYYYMMDD'),
        10
      ),
        defer = new Q.defer();

      return getRecord(_start, defer);
    })
    .then(() => {
      cli.log('Synchronized!');
      db.disconnect();
    })
    .catch(catchError);
};
