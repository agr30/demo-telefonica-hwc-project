import { db, methods } from 'csb-price-server-orm';

import Decimal from 'decimal.js';
import cli from '../';
import config from '../../config';
import fs from 'fs';
import parse from 'csv-parse';
import streamToPromise from 'stream-to-promise';

let errors;

/**
 * Logs an error to CLI.
 * @param {String} error
 */
function logError(error) {
  error && cli.log(cli.chalk.red('ERROR'), error);
}

/**
 * Fins a list of prices with priceLocation and price.amount field matching the
 * name specified.
 * @param {Object} name The name to search for.
 * @param {Promise} The promise of the price found.
 */
function getDbPrices(name) {
  return methods
    .findPriceByName(name, 'priceLocation price.amount priceConsumer.id')
    .catch(logError);
}

/**
 * Checks if prices on CSV record exists on DB.
 * @param {Object} record The key-values record object.
 * @param {Number} priceDecimals The number of decimal positions.
 * @param {Object} chalk The chalk object.
 * @returns {Promise} The validation promise.
 */
function validatePrice(record, priceDecimals, chalk) {
  if (record) {
    // IMPORTANT: Headers and product code values have spaces around.
    const productCode = record[' PRODUCTCODE (METERING VIVO) '].trim(),
      hasFreeTier = record['Free Tier'],
      // green = chalk.green,
      // cyan = chalk.cyan,
      red = chalk.red,
      magenta = chalk.magenta,
      blue = chalk.blue;

    // Get price headers from CSV.
    Object.keys(record)
      // Make a header normalization.
      .map(header => {
        const normalizedHeader = header.trim().toUpperCase();

        record[normalizedHeader] = record[header];
        return normalizedHeader;
      })
      // Filter price columns.
      .filter(header => header.startsWith('PRICE'))
      // Make price headers more convenient.
      .forEach(header => {
        const priceHeader = header.match(/^PRICE\s+([A-Z]+).*$/)[1];

        record[priceHeader] = record[header];
      });

    return getDbPrices(productCode)
      .then((prices = {}) => {
        const productOfferingPrice = prices.productOfferingPrice || [],
          obs = config.billing;
        let dbPrices = {};

        // Get prices stored on DB.
        productOfferingPrice.forEach(price => {
          const ob = price.priceConsumer[0].id;
          // Transfrom into a convenient structure i.e
          // {
          //   VIVO: {
          //     BR: 0.072807813,
          //     PE: 0.080088594,
          //     CL: 0.080088594,
          //     AR: 0.080088594,
          //     MX: 0.080088594,
          //     US: 0.080088594
          //   }
          // }
          if (!dbPrices[ob]) {
            dbPrices[ob] = {};
          }
          return (dbPrices[ob][price.priceLocation] = price.price.amount);
        });

        // Compare!
        Object.keys(obs).forEach(ob => {
          const countryPrices = dbPrices[ob];

          // Check if record has prices for OB.
          if (countryPrices === undefined) {
            cli.log(
              blue(
                `OB "${magenta(ob)}" for product code ` +
                  `"${magenta(productCode)}" not found on database`
              )
            );
            return;
          }

          obs[ob].forEach(priceConfig => {
            const countryCode = priceConfig.location,
              countryPrice = countryPrices[countryCode];

            // Check if record has prices for location.
            if (countryPrice === undefined) {
              cli.log(
                blue(
                  `DB price for product code "${magenta(productCode)}", OB ` +
                    `"${magenta(ob)}" and country code ` +
                    `"${magenta(countryCode)}" not found on database`
                )
              );
              return;
            }

            // Get DB price.
            let dbPrice = new Decimal(countryPrice).toDecimalPlaces(
              priceDecimals
            ),
              // Get final price using location factor.
              csvPrice = new Decimal(record[priceConfig.base])
                .mul(new Decimal(priceConfig.factor))
                .toDecimalPlaces(priceDecimals);

            // Some prices may have free tiers so, we need to set CSV price to
            // match Free Tier DB price.
            if (hasFreeTier && parseInt(dbPrice, 10) === 0) {
              csvPrice = dbPrice;
            }

            // Compare csv price with db price.
            if (!csvPrice.equals(dbPrice)) {
              errors++;
              cli.log(
                red(
                  `DB price ${magenta(dbPrice)} for OB ` +
                    `"${magenta(ob)}", product code ` +
                    `"${magenta(productCode)}" and country code ` +
                    `"${magenta(countryCode)}" does not match CSV price ` +
                    `${magenta(csvPrice)}`
                )
              );
              // Uncomment when verbose option is implemented.
              // } else {
              //   cli.log(
              //     green(
              //       `DB price ${cyan(dbPrice)} for product code ` +
              //         `"${cyan(productCode)}" and country code ` +
              //         `"${cyan(countryCode)}" matched CSV price ` +
              //         `${cyan(csvPrice)}!`
              //     )
              //   );
            }
          });
        });
      })
      .catch(logError);
  }
  return Promise.resolve();
}

export default ({
  filePath,
  csvDelimiter = config.csvDelimiter,
  priceDecimals = config.priceDecimals,
  dbUri = config.dbUri
}) => {
  const parser = parse({
    delimiter: csvDelimiter,
    auto_parse: false,
    columns: true
  }),
    chalk = cli.chalk,
    red = chalk.red,
    magenta = chalk.magenta,
    cyan = chalk.cyan,
    green = chalk.green;

  // Catch any error.
  parser.on('error', error => {
    logError(error);
    db.disconnect();
  });

  return db.connect({ dbUri, dbOptions: config.dbOptions }, cli).then(() => {
    cli.log(`Initializing ${cyan(filePath)} CSV import...`);
    // Parse CSV and save it to database.
    return streamToPromise(fs.createReadStream(filePath).pipe(parser))
      .then(records => {
        cli.log(
          green(
            `${cyan(records.length)} records readed from file ${cyan(filePath)}!`
          )
        );

        errors = 0;
        return Promise.all(
          records.map(record => validatePrice(record, priceDecimals, chalk))
        ).then(validatePromises => {
          cli.log(
            green(
              `${cyan(validatePromises.length)} records from CSV file ${cyan(filePath)} validated!`
            )
          );
          errors && cli.log(red(`${magenta(errors)} errors found!`));
        });
      })
      .then(db.disconnect)
      .catch(db.disconnect);
  });
};
