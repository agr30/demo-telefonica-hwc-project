import {
  addConfigData,
  checkBillingConfig,
  checkPaidTier
} from '../utils/price';
import { db, methods, models } from 'csb-price-server-orm';

import cli from '../';
import config from '../../config';
import fs from 'fs';
import { logError } from '../utils/log';
import parse from 'csv-parse';
import { saveWithHref } from '../utils/db';
import streamToPromise from 'stream-to-promise';
import { unflatten } from '../utils/json';

const Product = models.Product,
  Offering = models.Offering,
  PRODUCTS_COLLECTION = 'products',
  OFFERINGS_COLLECTION = 'offerings';
let PROPERTY_DELIMITER, numSavedOfferings;

/**
 * Saves a product to database.
 * @param {Object} data The data to save.
 * @param {String} startDateTime The startDateTime as ISO String.
 * @returns {Promise} The save promise.
 */
function saveProducts(data, startDateTime) {
  return saveWithHref(Product, data, startDateTime);
}

/**
 * Saves an offering and related prices to database.
 * @param {Object} data The data to save.
 * @param {String} startDateTime The startDateTime as ISO String.
 * @returns {Promise} The save promise.
 */
function saveOfferings(data, startDateTime) {
  // Set prices validFor startDateTime.
  data.productOfferingPrice.forEach(
    productOfferingPrice => productOfferingPrice.validFor = { startDateTime }
  );

  return saveWithHref(Offering, data, startDateTime);
}

/**
 * Saves a parsed record into database.
 * @param {Object} record The record data.
 * @param {String} collection The collection name.
 * @param {String} startDateTime The startDateTime as ISO String.
 * @returns {Promise} The save promise.
 */
function saveRecord(record, collection, startDateTime) {
  switch (collection) {
    case PRODUCTS_COLLECTION:
      return saveProducts(record, startDateTime);
    case OFFERINGS_COLLECTION:
      return saveOfferings(record, startDateTime);
    default:
      return Promise.resolve();
  }
}

/**
 * Transforms productOfferingPriceProps related data using config, to keep
 * proper location prices.
 * @param {Object} record The record object
 * @param {Object} productOfferingPriceProps A key/value pair array of prices
 *  props.
 */
function storePricesByLocation(record, productOfferingPriceProps) {
  const billing = config.billing;
  let idx = 0;

  // Iterate over OBs
  Object.keys(billing).forEach(ob => {
    const obConfig = config.billing[ob];

    // Iterate over each OB location prices.
    obConfig.price.forEach(price => {
      // Por cada registro, compruebo sobre la OB-Location para comprobar si subo el precio.

      if (
        record[`${ob}-${price.location}`] === 'undefined' ||
        record[`${ob}-${price.location}`] !== 'false'
      ) {
        // For each header transform infromation to populate each price location
        // for each OB.
        Object.keys(productOfferingPriceProps).forEach(header => {
          const value = record[header];

          // Price related header.
          if (/\/price\/amount/.test(header)) {
            // If there is no value for base price, there is some misconfiguration.
            checkBillingConfig(price, productOfferingPriceProps);
            // Add price related data from configuration.
            addConfigData(
              record,
              ob,
              idx,
              price,
              obConfig.tax,
              productOfferingPriceProps,
              PROPERTY_DELIMITER
            );
          } else {
            // Create a indexed header for each location price and link it with
            // record value.
            let transformedHeader = header.replace(
              /^(productOfferingPrice\/)(.+)$/,
              `$1${idx}/$2`
            );
            record[transformedHeader] = value;

            // Finally unflatten as JSON.
            unflatten(record, transformedHeader, PROPERTY_DELIMITER);
          }
        });

        idx = checkPaidTier(record, idx);

        idx++;
      }
    });
  });

  // Delete productOfferingPriceProps headers from record.
  Object.keys(productOfferingPriceProps).forEach(
    header => delete record[header]
  );
}

/**
 * Parses the CSV data into JSON and saves it to the collection.
 * @param {Object} record The key-values record object.
 * @param {String} startDateTime The startDateTime date in yyyy-MM-dd format.
 * @returns {Promise} The save promise.
 */
function csvToJson(record, startDateTime) {
  if (record) {
    const productOfferingPriceProps = {};

    // Store productOfferingPrice properties and unflatten as JSON other
    // columns.
    Object.keys(record).forEach(header => {
      if (header.startsWith('productOfferingPrice')) {
        productOfferingPriceProps[header] = record[header];
      } else {
        unflatten(record, header, PROPERTY_DELIMITER);
      }
    });

    // Store prices by location
    storePricesByLocation(record, productOfferingPriceProps);
    record.productOfferingPrice = record.productOfferingPrice
      ? record.productOfferingPrice.filter(price => price)
      : [];

    // Search for products with the same name and generate product specs
    return Product.find({ name: record.name })
      .exec()
      .then(products => {
        if (products && products.length) {
          record.productSpecification = [];

          products.forEach(product =>
            record.productSpecification.push({
              id: product.id,
              href: product.href,
              name: product.name
            })
          );

          // Finally save offering.
          // console.log('Parsed offering', JSON.stringify(record));
          return saveRecord(
            record,
            OFFERINGS_COLLECTION,
            new Date(startDateTime).toISOString()
          ).then(offering => {
            numSavedOfferings++;

            return Promise.all(
              products.map(product =>
                // Make product -> offering relationship.
                methods.updateById(
                  Product,
                  product.id,
                  {
                    offerings: [
                      {
                        id: offering.id,
                        href: offering.href,
                        name: offering.name,
                        validFor: offering.validFor,
                        description: offering.description
                      }
                    ]
                  },
                  'id'
                )
              )
            );
          });
        } else {
          const chalk = cli.chalk, blue = chalk.blue, magenta = chalk.magenta;

          cli.log(
            blue(
              `No product found with name ${magenta(record.name)}. Offering will not be loaded into database.`
            )
          );
          return Promise.resolve();
        }
      })
      .catch(logError);
  }
  return Promise.resolve();
}

/**
 * Deletes all entities for categorized service.
 * @param {String} category The category to delete.
 * @return {Promise} The promise that all documents have been removed.
 */
function deleteServiceEntity(Entity, category) {
  return methods
    .remove(Entity, {
      'category.id': category
    })
    .catch(logError);
}

export default ({
  filePath,
  startDateTime,
  category,
  csvDelimiter = config.csvDelimiter,
  propertyDelimiter = config.propertyDelimiter,
  dbUri = config.dbUri
}) => {
  const parser = parse({
    delimiter: csvDelimiter,
    auto_parse: true,
    columns: true
  }),
    chalk = cli.chalk,
    cyan = chalk.cyan,
    green = chalk.green;

  // Catch any error.
  parser.on('error', error => {
    logError(error);
    db.disconnect();
  });

  return db.connect({ dbUri, dbOptions: config.dbOptions }, cli).then(() => {
    cli.log(`Initializing ${cyan(filePath)} CSV import...`);

    PROPERTY_DELIMITER = propertyDelimiter;
    numSavedOfferings = 0;

    // Parse CSV and save it to database.
    return streamToPromise(fs.createReadStream(filePath).pipe(parser))
      .then(records => {
        const obsLength =
          (config.billing && Object.keys(config.billing).length) || 0;

        cli.log(
          green(
            `${cyan(records.length)} records readed from file ${cyan(filePath)}!`
          )
        );
        cli.log(green(`${cyan(obsLength)} OBs readed from config file!`));

        // Empty category data.
        cli.log(green(`Deleting "${cyan(category)}" categorized offerings...`));

        return deleteServiceEntity(Offering, category).then(() => {
          cli.log(green(`Loading and saving "${cyan(category)}" prices...`));

          return Promise.all(
            records.map(record => csvToJson(record, startDateTime))
          ).then(() => {
            cli.log(
              green(
                `CSV file ${cyan(filePath)} saved ${cyan(numSavedOfferings)} records along ${cyan(obsLength)} OBs!`
              )
            );
          });
        });
      })
      .then(db.disconnect)
      .catch(err => {
        logError(err);
        throw new Error();
      })
      .catch(db.disconnect);
  });
};
