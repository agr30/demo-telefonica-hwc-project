import {
  addConfigData,
  checkBillingConfig,
  checkPaidTier
} from '../utils/price';
import { db, methods, models } from 'csb-price-server-orm';

import cli from '../';
import config from '../../config';
import fs from 'fs';
import { logError } from '../utils/log';
import parse from 'csv-parse';
import streamToPromise from 'stream-to-promise';
import { unflatten } from '../utils/json';

const Offering = models.Offering;
let PROPERTY_DELIMITER, numUpdatedPrices;

/**
 * Saves an offering and related prices to database.
 * @param {Object} id The id for the offering to update.
 * @param {Object} data The data to save.
 * @param {Object} rebootOb The ob to which apply price reboot.
 * @returns {Promise} The save promise.
 */
function updateOffering(offering, { productOfferingPrice }, rebootOb) {
  // Set prices validFor.
  productOfferingPrice.forEach(
    productOfferingPrice =>
      productOfferingPrice.validFor = { validFor: offering.validFor }
  );

  // Replace existing prices for the OB with new ones.
  const updatedPrices = offering.productOfferingPrice.map(
    currentOfferingPrice =>
      (currentOfferingPrice.priceConsumer[0].id === rebootOb
        ? productOfferingPrice.filter(
            price => price.priceLocation === currentOfferingPrice.priceLocation
          )[0]
        : currentOfferingPrice)
  );

  // Update offering with new OB price.
  return methods.update(
    Offering,
    { id: offering.id },
    { productOfferingPrice: updatedPrices }
  );
}

/**
 * Transforms productOfferingPriceProps related data using config, to keep
 * proper location prices.
 * @param {Object} record The record object
 * @param {Object} rebootOb The ob to which apply price reboot.
 * @param {Object} productOfferingPriceProps A key/value pair array of prices
 *  props.
 */
function storeRebootObPrices(record, rebootOb, productOfferingPriceProps) {
  const obConfig = config.billing[rebootOb];
  let idx = 0;

  // Iterate over OB location prices.
  obConfig.price.forEach(price => {
    // For each header transform infromation to populate each price location
    // for each OB.
    Object.keys(productOfferingPriceProps).forEach(header => {
      const value = record[header];

      // Price related header.
      if (/\/price\/amount/.test(header)) {
        // If there is no value for base price, there is some misconfiguration.
        checkBillingConfig(price, productOfferingPriceProps);
        // Add price related data from configuration.
        addConfigData(
          record,
          rebootOb,
          idx,
          price,
          obConfig.tax,
          productOfferingPriceProps,
          PROPERTY_DELIMITER
        );
      } else {
        // Create a indexed header for each location price and link it with
        // record value.
        let transformedHeader = header.replace(
          /^(productOfferingPrice\/)(.+)$/,
          `$1${idx}/$2`
        );
        record[transformedHeader] = value;

        // Finally unflatten as JSON.
        unflatten(record, transformedHeader, PROPERTY_DELIMITER);
      }
    });

    idx = checkPaidTier(record, idx);

    idx++;
  });

  // Delete productOfferingPriceProps headers from record.
  Object.keys(productOfferingPriceProps).forEach(
    header => delete record[header]
  );
}

/**
 * Parses the CSV data into JSON and saves it to the collection.
 * @param {Object} record The key-values record object.
 * @param {Object} rebootOb The ob to which apply price reboot.
 * @returns {Promise} The save promise.
 */
function csvToJson(record, rebootOb) {
  if (record) {
    const productOfferingPriceProps = {};

    // Only productOfferingPrices must be stored for update.
    Object.keys(record).forEach(header => {
      if (header.startsWith('productOfferingPrice')) {
        productOfferingPriceProps[header] = record[header];
      } else {
        delete record[header];
      }
    });

    // Store prices by ob specified
    storeRebootObPrices(record, rebootOb, productOfferingPriceProps);

    // First find active offering to get the startDateTime
    return Offering.findOne({
      name: record.productOfferingPrice[0].name,
      deleted: { $ne: true },
      lifeCycleStatus: 'active'
    }).then(offering => {
      if (offering) {
        // Finally update offering.
        // console.log('Original offering', JSON.stringify(offering));
        // console.log('Parsed offering', JSON.stringify(record));
        return updateOffering(offering, record, rebootOb)
          .then(() => numUpdatedPrices += record.productOfferingPrice.length)
          .catch(logError);
      } else {
        const chalk = cli.chalk, blue = chalk.blue, magenta = chalk.magenta;

        cli.log(
          blue(
            `No offering found with name ${magenta(record.name)}. Offering will not be updated with ${magenta(rebootOb)} prices.`
          )
        );
        return Promise.resolve();
      }
    });
  }
  return Promise.resolve();
}

export default ({
  filePath,
  ob,
  category,
  csvDelimiter = config.csvDelimiter,
  propertyDelimiter = config.propertyDelimiter,
  dbUri = config.dbUri
}) => {
  const parser = parse({
    delimiter: csvDelimiter,
    auto_parse: true,
    columns: true
  }),
    chalk = cli.chalk,
    cyan = chalk.cyan,
    green = chalk.green;

  // Catch any error.
  parser.on('error', error => {
    logError(error);
    db.disconnect();
  });

  return db.connect({ dbUri, dbOptions: config.dbOptions }, cli).then(() => {
    cli.log(`Initializing ${cyan(filePath)} CSV import...`);

    PROPERTY_DELIMITER = propertyDelimiter;
    numUpdatedPrices = 0;

    // Parse CSV and save it to database.
    return streamToPromise(fs.createReadStream(filePath).pipe(parser))
      .then(records => {
        const obsLength =
          (config.billing && Object.keys(config.billing).length) || 0;

        cli.log(
          green(
            `${cyan(records.length)} records readed from file ${cyan(filePath)}!`
          )
        );
        cli.log(green(`${cyan(obsLength)} OBs readed from config file!`));

        cli.log(
          green(`Rebooting "${cyan(category)}" prices for OB "${cyan(ob)}"...`)
        );

        return Promise.all(
          records.map(record => csvToJson(record, ob))
        ).then(() => {
          cli.log(
            green(
              `CSV file ${cyan(filePath)} updated ${cyan(numUpdatedPrices)} records for ${cyan(ob)} OB!`
            )
          );
        });
      })
      .then(db.disconnect)
      .catch(err => {
        logError(err);
        throw new Error();
      })
      .catch(db.disconnect);
  });
};
