import { db, methods, models } from 'csb-price-server-orm';

import Q from 'q';
import cli from '../';
import config from '../../config';
import httpStatus from 'http-status-codes';
import moment from 'moment';
import request from 'request';

const Currency = models.Currency;

/**
 * Catchs an error, logs and disconnect db.
 * @param {String} error
 */
function catchError(error) {
  error &&
    cli.log(
      `${cli.chalk.red('ERROR')} ${error.message}${error.description ? `: ${error.description}` : ''}`
    );
  db.disconnect();
}

/**
 * Calls openexchangerates api.
 * @param {Object} options Options object with needed options to call the endpoint.
 * @return {Promise} Promise-
 */
function callApi(options) {
  let qs = {
    app_id: config.currencies.appId
  },
    defer = new Q.defer(),
    url = config.currencies.server + config.currencies.entities[options.entity];

  request(
    {
      url: url,
      qs: qs,
      method: 'GET',
      headers: {
        Accept: 'application/json'
      }
    },
    (err, response, body) => {
      if (!err && response.statusCode === httpStatus.OK) {
        defer.resolve(JSON.parse(body));
      } else {
        defer.reject(err || JSON.parse(body));
      }
    }
  );

  return defer.promise;
}

export default ({ dbUri = config.dbUri }) => {
  return db
    .connect({ dbUri, dbOptions: config.dbOptions }, cli)
    .then(() => {
      return Currency.findOne({
        _id: parseInt(moment().format('YYYYMMDD'))
      });
    })
    .then(record => {
      if (record) {
        cli.log(
          `Record ${cli.chalk.cyan(record._id)} is already stored, avoiding API request...`
        );
        db.disconnect();
      } else {
        cli.log('Requesting API last currency...');
        return callApi({ entity: 'latest' })
          .then(result => {
            let _date = new Date(result.timestamp * 1000);
            return methods.save(
              Currency,
              Object.assign({}, result, {
                _id: parseInt(moment(_date).format('YYYYMMDD'))
              })
            );
          })
          .then(() => {
            cli.log('Done!');
            db.disconnect();
          });
      }
    })
    .catch(catchError);
};
