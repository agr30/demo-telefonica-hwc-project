import currencySync from './actions/currency-sync';

// Start currency sync with optional specified date.
currencySync({ startDate: process.argv[2] });
