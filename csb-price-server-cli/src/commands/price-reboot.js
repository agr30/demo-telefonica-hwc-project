import config from '../../config';
import fsAutocomplete from 'vorpal-autocomplete-fs';
import path from 'path';
import priceReboot from '../actions/price-reboot';

export default cli => {
  cli
    .command(
      'price reboot <filePath> <ob> <category>',
      `Loads bootstrap offering CSV file prices into a running mongoDB instance collection for the specified OB. ` +
        `parameters perform a previous deletion of offerings related to that ` +
        `OB and category.`
    )
    .autocomplete(fsAutocomplete())
    .alias('pricere')
    .option(
      '--d, -delimiter  <CSV delimiter>',
      `The CSV file delimiter. Defaults to "${config.csvDelimiter}"`
    )
    .option(
      '--p, -propertyDelimiter  <CSV property delimiter>',
      `The CSV header delimiter for object properties and arrays indexes. ` +
        `Defaults to "${config.propertyDelimiter}"`
    )
    .option(
      '--dburi <database URI>',
      `Connection string for the database instance having the format ` +
        `"username:password@host:port/database?options...." Defaults to ` +
        `${config.dbUri}`
    )
    .validate(args => {
      const file = args.filePath;
      if (path.extname(file) === '.csv') {
        return true;
      } else {
        return `${cli.chalk.cyan(file)} file is not CSV.`;
      }
    })
    .action(args => {
      const options = args.options;

      return priceReboot({
        filePath: args.filePath,
        ob: args.ob,
        category: args.category,
        csvDelimiter: options.d,
        propertyDelimiter: options.p,
        dbUri: options.dburi
      });
    });
};
