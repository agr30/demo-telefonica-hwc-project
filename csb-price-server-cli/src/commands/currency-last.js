import currencyLast from '../actions/currency-last';

export default cli => {
  cli
    .command(
      'currency last',
      'Gets and storage the currency info (from openexchangerates.org) of current day.'
    )
    .alias('currlast')
    .action(args => {
      const options = args.options;
      return currencyLast({
        dbUri: options.dburi
      });
    });
};
