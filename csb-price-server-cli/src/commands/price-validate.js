import config from '../../config';
import fsAutocomplete from 'vorpal-autocomplete-fs';
import path from 'path';
import priceValidation from '../actions/price-validate';

export default cli => {
  cli
    .command(
      'price validate <filePath>',
      'Validates prices against a CSV file.'
    )
    .autocomplete(fsAutocomplete())
    .alias('priceval')
    .option(
      '--d, -delimiter  <CSV delimiter>',
      `The CSV file delimiter. Defaults to "${config.csvDelimiter}"`
    )
    .option(
      '--p, -price-decimals  <Number of decimals>',
      `The number od price decimal postions. Defaults to "${config.priceDecimals}"`
    )
    .option(
      '--dburi <database URI>',
      `Connection string for the database instance having the format ` +
        `"username:password@host:port/database?options...." Defaults to ` +
        `${config.dbUri}`
    )
    .validate(args => {
      const file = args.filePath;
      if (path.extname(file) === '.csv') {
        return true;
      } else {
        return `${cli.chalk.cyan(file)} file is not CSV.`;
      }
    })
    .action(args => {
      const options = args.options;

      return priceValidation({
        filePath: args.filePath,
        csvDelimiter: options.d,
        priceDecimals: options.p,
        dbUri: options.dburi
      });
    });
};
