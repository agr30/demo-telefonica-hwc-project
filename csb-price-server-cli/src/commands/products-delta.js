import config from '../../config';
import fsAutocomplete from 'vorpal-autocomplete-fs';
import path from 'path';
import productsDelta from '../actions/products-delta';

const DATE_FORMAT = 'yyyy-MM-dd';

export default cli => {
  cli
    .command(
      'products delta <filePath> <category> <startDateTime>',
      `Insert a products CSV file into a running mongoDB instance collection. ` +
        `startDateTime must have ${DATE_FORMAT} format. The category ` +
        `parameter performs a previous deletion of products and offerings ` +
        `related to that category.`
    )
    .autocomplete(fsAutocomplete())
    .alias('proddelta')
    .option(
      '--d, -delimiter  <CSV delimiter>',
      `The CSV file delimiter. Defaults to "${config.csvDelimiter}"`
    )
    .option(
      '--p, -propertyDelimiter  <CSV property delimiter>',
      `The CSV header delimiter for object properties and arrays indexes. ` +
        `Defaults to "${config.propertyDelimiter}"`
    )
    .option(
      '--dburi <database URI>',
      `Connection string for the database instance having the format ` +
        `"username:password@host:port/database?options...." Defaults to ` +
        `${config.dbUri}`
    )
    .validate(args => {
      const file = args.filePath;
      if (path.extname(file) === '.csv') {
        return true;
      } else {
        return `${cli.chalk.cyan(file)} file is not CSV.`;
      }
    })
    .validate(args => {
      const startDateTime = args.startDateTime;

      if (/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/.test(startDateTime)) {
        /* eslint-disable no-new */
        if (isNaN(new Date(startDateTime))) {
          /* eslint-enable no-new */
          return `${cli.chalk.cyan(startDateTime)} is not a valid date.`;
        } else {
          return true;
        }
      } else {
        return `${cli.chalk.cyan(startDateTime)} does not have ${DATE_FORMAT} format.`;
      }
    })
    .action(args => {
      const options = args.options;

      return productsDelta({
        filePath: args.filePath,
        startDateTime: args.startDateTime,
        category: args.category,
        csvDelimiter: options.d,
        propertyDelimiter: options.p,
        dbUri: options.dburi
      });
    });
};
