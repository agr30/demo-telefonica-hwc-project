import config from '../../config';
import currencySync from '../actions/currency-sync';
import moment from 'moment';

export default cli => {
  cli
    .command(
      'currency sync [date]',
      `Synchronizes currencies. Default date is ${cli.chalk.cyan(config.currencies.startRecordingDateByDefault)}`
    )
    .alias('currsync')
    .validate(args => {
      const startDate = args.date;
      if (startDate) {
        return (
          moment(startDate, 'YYYY-MM-DD').isValid() &&
          moment(startDate).isBefore(moment())
        );
      } else {
        return true;
      }
    })
    .action(args => {
      const options = args.options;

      return currencySync({
        startDate: args.date,
        dbUri: options.dburi
      });
    });
};
