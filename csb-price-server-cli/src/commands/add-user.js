import addUser from '../actions/add-user';
import config from '../../config';

const USER_SEPARATOR = '@',
  USER_MUST_EXIST_MSG = `User must be specified as "username${USER_SEPARATOR}password".`;

export default cli => {
  let userName, password;

  cli
    .command(
      'adduser <user>',
      `Adds a user to a database. ${USER_MUST_EXIST_MSG}`
    )
    .option('--role <user role>', 'User role. Defaults to "read".', [
      'read',
      'readWrite'
    ])
    .option(
      '--dburi <database URI>',
      `Connection string for the database instance having the format ` +
        `"username:password@host:port/database?options...." Defaults to ` +
        `${config.dbUri}`
    )
    .validate(args => {
      const user = args.user.split(USER_SEPARATOR);

      userName = user[0];
      password = user[1];
      if (userName && password) {
        return true;
      } else {
        return USER_MUST_EXIST_MSG;
      }
    })
    .action(args => {
      const options = args.options;

      return addUser({
        userName: userName,
        password: password,
        role: options.role,
        dbUri: options.dburi
      });
    });
};
