'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.logError = logError;

var _ = require('../');

var _2 = _interopRequireDefault(_);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Logs an error to CLI.
 * @param {String} error
 */
function logError(error) {
  console.log(error);
  error && _2.default.log(_2.default.chalk.red('ERROR'), error);
}