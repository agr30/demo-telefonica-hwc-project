'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.addConfigData = addConfigData;
exports.checkPaidTier = checkPaidTier;
exports.checkBillingConfig = checkBillingConfig;

var _decimal = require('decimal.js');

var _decimal2 = _interopRequireDefault(_decimal);

var _ = require('../');

var _2 = _interopRequireDefault(_);

var _json = require('./json');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Adds config data to record at specified index.
 * @param {Object} record The record object.
 * @param {String} ob Current OB to be added.
 * @param {Number} idx The index to create config data.
 * @param {Object} price The price config properties.
 * @param {Object} price The tax config properties.
 * @param {Array} productOfferingPriceProps The price properties of the record.
 * @param {Object} delimiter The property delimiter.
 */
function addConfigData(record, ob, idx, price, tax, productOfferingPriceProps, delimiter) {
  if (record[`productOfferingPrice/${idx}/price/amount`] !== 0 && !record[`productOfferingPrice/${idx}/price/amount`]) {
    // OB
    (0, _json.createUnflattenedProperty)(record, `productOfferingPrice/${idx}/priceConsumer/0/id`, ob, delimiter);
    // Price location
    (0, _json.createUnflattenedProperty)(record, `productOfferingPrice/${idx}/priceLocation`, price.location, delimiter);
    // Price amount -> apply factor and taxes
    (0, _json.createUnflattenedProperty)(record, `productOfferingPrice/${idx}/price/amount`, new _decimal2.default(productOfferingPriceProps[`productOfferingPrice/price/amount/${price.base}`]).mul(new _decimal2.default(price.factor)), delimiter);
    // Tax rate
    (0, _json.createUnflattenedProperty)(record, `productOfferingPrice/${idx}/taxRate`, tax.rate, delimiter);
    // Tax type
    (0, _json.createUnflattenedProperty)(record, `productOfferingPrice/${idx}/taxType`, tax.type, delimiter);
  }
}

/**
 * Checks if current record idx has different tiers, and sets its prices.
 * @param {Object} record The record object.
 * @param {Number} idx Current OB + price location index.
 */
function checkPaidTier(record, idx) {
  if (record.productOfferingPrice[idx].unitOfMeasure.maximum) {
    // Create the next usage section record cloning all properties.
    var nextUsageRecord = JSON.parse(JSON.stringify(record.productOfferingPrice[idx]));
    // First tier costs 0.
    record.productOfferingPrice[idx].price.amount = 0;
    // Set limits for paid tier.
    nextUsageRecord.unitOfMeasure.minimum = nextUsageRecord.unitOfMeasure.maximum + 1;
    delete nextUsageRecord.unitOfMeasure.maximum;

    // Assign paid tier to next index.
    record.productOfferingPrice[++idx] = nextUsageRecord;
  }
  return idx;
}

/**
 * Checks if record has price for current billing price location. If there is
 * no price, a message is shown.
 * @param {Object} price The proce config properties.
 * @param {Array} productOfferingPriceProps The price properties of the record.
 */
function checkBillingConfig(price, productOfferingPriceProps) {
  var chalk = _2.default.chalk,
      blue = chalk.blue,
      magenta = chalk.magenta;

  if (productOfferingPriceProps[`productOfferingPrice/price/amount/${price.base}`] === undefined || productOfferingPriceProps[`productOfferingPrice/price/amount/${price.base}`] === null || productOfferingPriceProps[`productOfferingPrice/price/amount/${price.base}`] === '') {
    _2.default.log(blue(`Record found with no ${magenta(price.base)} price. This is probably due to a billing mismatch in config file. Aborting...`));
    process.exit(0);
  }
}