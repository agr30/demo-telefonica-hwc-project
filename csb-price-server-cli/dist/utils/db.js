'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.saveWithHref = saveWithHref;
exports.deleteEntity = deleteEntity;

var _config = require('../../config');

var _config2 = _interopRequireDefault(_config);

var _log = require('./log');

var _csbPriceServerOrm = require('csb-price-server-orm');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Saves a function with a public id and returns a closure to put additional
 * data based on that id.
 * @param {Object} Entity The database entity.
 * @param {Object} data The data to save.
 * @param {Function} A function where put additional data.
 */
function saveWithId(Entity, data) {
  data = _csbPriceServerOrm.methods.idGenerate(data);
  return function () {
    var additionalData = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    Object.assign(data, additionalData);
    return _csbPriceServerOrm.methods.save(Entity, data).catch(_log.logError);
  };
}

/**
 * Saves a function with an href based on the id.
 * @param {Object} Entity The database entity.
 * @param {Object} data The data to save.
 * @param {String} startDateTime The startDateTime as ISO String.
 * @returns {Promise} The save promise.
 */
function saveWithHref(Entity, data, startDateTime) {
  return saveWithId(Entity, data)({
    validFor: { startDateTime },
    href: `${_config2.default.hrefBasePath}${Entity.collection.collectionName}/${data.id}`
  }).catch(_log.logError);
}

/**
* Deletes all entities using an optional select.
* @param {String?} select Select to delete entity.
* @return {Promise} The promise that all documents have been removed.
*/
function deleteEntity(Entity, select) {
  return _csbPriceServerOrm.methods.remove(Entity, select).catch(_log.logError);
}