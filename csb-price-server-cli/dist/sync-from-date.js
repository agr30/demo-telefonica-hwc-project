'use strict';

var _currencySync = require('./actions/currency-sync');

var _currencySync2 = _interopRequireDefault(_currencySync);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Start currency sync with optional specified date.
(0, _currencySync2.default)({ startDate: process.argv[2] });