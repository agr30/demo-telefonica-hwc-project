'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

require('babel-polyfill');

var _addUser = require('./commands/add-user');

var _addUser2 = _interopRequireDefault(_addUser);

var _convertCsv = require('./commands/convert-csv');

var _convertCsv2 = _interopRequireDefault(_convertCsv);

var _currencyLast = require('./commands/currency-last');

var _currencyLast2 = _interopRequireDefault(_currencyLast);

var _currencySync = require('./commands/currency-sync');

var _currencySync2 = _interopRequireDefault(_currencySync);

var _logops = require('logops');

var _logops2 = _interopRequireDefault(_logops);

var _offeringsBootstrap = require('./commands/offerings-bootstrap');

var _offeringsBootstrap2 = _interopRequireDefault(_offeringsBootstrap);

var _offeringsDelta = require('./commands/offerings-delta');

var _offeringsDelta2 = _interopRequireDefault(_offeringsDelta);

var _priceReboot = require('./commands/price-reboot');

var _priceReboot2 = _interopRequireDefault(_priceReboot);

var _priceValidate = require('./commands/price-validate');

var _priceValidate2 = _interopRequireDefault(_priceValidate);

var _productsBootstrap = require('./commands/products-bootstrap');

var _productsBootstrap2 = _interopRequireDefault(_productsBootstrap);

var _productsDelta = require('./commands/products-delta');

var _productsDelta2 = _interopRequireDefault(_productsDelta);

var _vorpal = require('vorpal');

var _vorpal2 = _interopRequireDefault(_vorpal);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var IS_CLI = process.env.CLI_ENV;
var cli = void 0,
    chalk = void 0;

if (IS_CLI) {
  // Start vorpal as usual
  cli = (0, _vorpal2.default)();
  chalk = cli.chalk;

  // Supercharge logging methods
  cli.debug = cli.warn = cli.info = cli.error = cli.fatal = cli.log;

  (0, _productsBootstrap2.default)(cli);
  (0, _offeringsBootstrap2.default)(cli);
  (0, _productsDelta2.default)(cli);
  (0, _offeringsDelta2.default)(cli);
  (0, _priceReboot2.default)(cli);
  (0, _addUser2.default)(cli);
  (0, _currencyLast2.default)(cli);
  (0, _currencySync2.default)(cli);
  (0, _priceValidate2.default)(cli);
  (0, _convertCsv2.default)(cli);

  cli.log(`Welcome to ${chalk.cyan('Price Server CLI')}, type ${chalk.cyan('"help"')} ` + `to see available commands!`);
} else {
  // Fake vorpal to beahave properly as outern script.
  var fakeChalk = function fakeChalk(str) {
    return str;
  };
  chalk = {
    cyan: fakeChalk,
    magenta: fakeChalk,
    green: fakeChalk,
    red: fakeChalk
  };

  cli = {
    delimiter: function delimiter() {
      return {
        show: function show() {
          return {
            debug: _logops2.default.info,
            warn: _logops2.default.info,
            log: _logops2.default.info,
            info: _logops2.default.info,
            error: _logops2.default.info,
            fatal: _logops2.default.fatal,
            chalk
          };
        }
      };
    }
  };
}

exports.default = cli.delimiter(chalk.magenta('>')).show();