'use strict';

var _currencyLast = require('./actions/currency-last');

var _currencyLast2 = _interopRequireDefault(_currencyLast);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Start currency last.
(0, _currencyLast2.default)({});