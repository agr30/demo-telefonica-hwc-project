'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('../../config');

var _config2 = _interopRequireDefault(_config);

var _vorpalAutocompleteFs = require('vorpal-autocomplete-fs');

var _vorpalAutocompleteFs2 = _interopRequireDefault(_vorpalAutocompleteFs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _priceReboot = require('../actions/price-reboot');

var _priceReboot2 = _interopRequireDefault(_priceReboot);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (cli) {
  cli.command('price reboot <filePath> <ob> <category>', `Loads bootstrap offering CSV file prices into a running mongoDB instance collection for the specified OB. ` + `parameters perform a previous deletion of offerings related to that ` + `OB and category.`).autocomplete((0, _vorpalAutocompleteFs2.default)()).alias('pricere').option('--d, -delimiter  <CSV delimiter>', `The CSV file delimiter. Defaults to "${_config2.default.csvDelimiter}"`).option('--p, -propertyDelimiter  <CSV property delimiter>', `The CSV header delimiter for object properties and arrays indexes. ` + `Defaults to "${_config2.default.propertyDelimiter}"`).option('--dburi <database URI>', `Connection string for the database instance having the format ` + `"username:password@host:port/database?options...." Defaults to ` + `${_config2.default.dbUri}`).validate(function (args) {
    var file = args.filePath;
    if (_path2.default.extname(file) === '.csv') {
      return true;
    } else {
      return `${cli.chalk.cyan(file)} file is not CSV.`;
    }
  }).action(function (args) {
    var options = args.options;

    return (0, _priceReboot2.default)({
      filePath: args.filePath,
      ob: args.ob,
      category: args.category,
      csvDelimiter: options.d,
      propertyDelimiter: options.p,
      dbUri: options.dburi
    });
  });
};