'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('../../config');

var _config2 = _interopRequireDefault(_config);

var _currencySync = require('../actions/currency-sync');

var _currencySync2 = _interopRequireDefault(_currencySync);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (cli) {
  cli.command('currency sync [date]', `Synchronizes currencies. Default date is ${cli.chalk.cyan(_config2.default.currencies.startRecordingDateByDefault)}`).alias('currsync').validate(function (args) {
    var startDate = args.date;
    if (startDate) {
      return (0, _moment2.default)(startDate, 'YYYY-MM-DD').isValid() && (0, _moment2.default)(startDate).isBefore((0, _moment2.default)());
    } else {
      return true;
    }
  }).action(function (args) {
    var options = args.options;

    return (0, _currencySync2.default)({
      startDate: args.date,
      dbUri: options.dburi
    });
  });
};