'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _config = require('../../config');

var _config2 = _interopRequireDefault(_config);

var _vorpalAutocompleteFs = require('vorpal-autocomplete-fs');

var _vorpalAutocompleteFs2 = _interopRequireDefault(_vorpalAutocompleteFs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _convertCsv = require('../actions/convert-csv');

var _convertCsv2 = _interopRequireDefault(_convertCsv);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (cli) {
    cli.command('convert csv', 'Generates a CSV with the needed format for bulk load from CLI (bootstrap option) from the input CSVs.').autocomplete((0, _vorpalAutocompleteFs2.default)()).alias('csvconv').option('--t, -items-type  [products|offerings]', `The type of items in the CSV file, products or offerings ("products" by default)`).option('--c, -category  [opencloud|cloudserver]', `The main category associated to the items in the CSV ("opencoud" by default)`).option('--f, -input-folder  <inputFolder>', `Basis path where the input CSV files are stored ("./files/input/" by default)`).option('--i, -input-file  <fileName>', `Filename ot the CSV where the input values are stored ("price_catalog.csv" by default)`).option('--m, -multiple  <boolean>', `Indicate if there are multiple CSV to use for product generation (false by default)`).option('--f2, -input-folder2  <inputFolder>', `Basis path where the second input CSV files are stored ("./files/input/" by default)`).option('--i2, -input-file2  <fileName>', `Filename ot the second CSV where the input values are stored ("price_catalog.csv" by default)`).option('--o, -output-file  <filePath>', `Path to the file to store the converted csv ("./files/output/converted.csv" by default)`).option('--e, -ecs-details  <filePath>', `The CSV file containing ECS-details ("ecs_details.csv" by default)`).option('--r, -rcs-details  <filePath>', `The CSV file containing ECS-details ("rcs_details.csv" by default)`).option('--b, -baremetal-details  <filePath>', `The CSV file containing ECS-details ("baremetal_details.csv" by default)`).option('--m, -mrs-details  <filePath>', `The CSV file containing ECS-details ("mrs_details.csv" by default)`).option('--n, -nat-gateway-details  <filePath>', `The CSV file containing ECS-details ("natGateway_details.csv" by default)`).option('--d, -dedicated-host-details  <filePath>', `The CSV file containing ECS-details ("dedicatedHost_details.csv" by default)`).option('--w, -workspace-details  <filePath>', `The CSV file containing ECS-details ("workspace_details.csv" by default)`).action(function (args) {
        var options = args.options;

        return (0, _convertCsv2.default)({
            itemsType: options.t,
            category: options.c,
            inputFolder: options.f,
            inputFile: options.i,
            multiple: options.m,
            inputFolder2: options.f2,
            inputFile2: options.i2,
            ecsFile: options.e,
            rdsFile: options.r,
            baremetalFile: options.b,
            mrsFile: options.m,
            natGatewayFile: options.n,
            dedicatedHostFile: options.d,
            workspaceFile: options.w,
            outputPath: options.o
        });
    });
};