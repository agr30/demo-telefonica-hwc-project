'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('../../config');

var _config2 = _interopRequireDefault(_config);

var _vorpalAutocompleteFs = require('vorpal-autocomplete-fs');

var _vorpalAutocompleteFs2 = _interopRequireDefault(_vorpalAutocompleteFs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _priceValidate = require('../actions/price-validate');

var _priceValidate2 = _interopRequireDefault(_priceValidate);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (cli) {
  cli.command('price validate <filePath>', 'Validates prices against a CSV file.').autocomplete((0, _vorpalAutocompleteFs2.default)()).alias('priceval').option('--d, -delimiter  <CSV delimiter>', `The CSV file delimiter. Defaults to "${_config2.default.csvDelimiter}"`).option('--p, -price-decimals  <Number of decimals>', `The number od price decimal postions. Defaults to "${_config2.default.priceDecimals}"`).option('--dburi <database URI>', `Connection string for the database instance having the format ` + `"username:password@host:port/database?options...." Defaults to ` + `${_config2.default.dbUri}`).validate(function (args) {
    var file = args.filePath;
    if (_path2.default.extname(file) === '.csv') {
      return true;
    } else {
      return `${cli.chalk.cyan(file)} file is not CSV.`;
    }
  }).action(function (args) {
    var options = args.options;

    return (0, _priceValidate2.default)({
      filePath: args.filePath,
      csvDelimiter: options.d,
      priceDecimals: options.p,
      dbUri: options.dburi
    });
  });
};