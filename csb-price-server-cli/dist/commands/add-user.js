'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _addUser = require('../actions/add-user');

var _addUser2 = _interopRequireDefault(_addUser);

var _config = require('../../config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var USER_SEPARATOR = '@',
    USER_MUST_EXIST_MSG = `User must be specified as "username${USER_SEPARATOR}password".`;

exports.default = function (cli) {
  var userName = void 0,
      password = void 0;

  cli.command('adduser <user>', `Adds a user to a database. ${USER_MUST_EXIST_MSG}`).option('--role <user role>', 'User role. Defaults to "read".', ['read', 'readWrite']).option('--dburi <database URI>', `Connection string for the database instance having the format ` + `"username:password@host:port/database?options...." Defaults to ` + `${_config2.default.dbUri}`).validate(function (args) {
    var user = args.user.split(USER_SEPARATOR);

    userName = user[0];
    password = user[1];
    if (userName && password) {
      return true;
    } else {
      return USER_MUST_EXIST_MSG;
    }
  }).action(function (args) {
    var options = args.options;

    return (0, _addUser2.default)({
      userName: userName,
      password: password,
      role: options.role,
      dbUri: options.dburi
    });
  });
};