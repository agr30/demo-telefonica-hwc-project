'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('../../config');

var _config2 = _interopRequireDefault(_config);

var _vorpalAutocompleteFs = require('vorpal-autocomplete-fs');

var _vorpalAutocompleteFs2 = _interopRequireDefault(_vorpalAutocompleteFs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _productsBootstrap = require('../actions/products-bootstrap');

var _productsBootstrap2 = _interopRequireDefault(_productsBootstrap);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DATE_FORMAT = 'yyyy-MM-dd';

exports.default = function (cli) {
  cli.command('products bootstrap <filePath> <category> <startDateTime>', `Loads a products CSV file into a running mongoDB instance collection. ` + `startDateTime must have ${DATE_FORMAT} format. The category ` + `parameter performs a previous deletion of products and offerings ` + `related to that category.`).autocomplete((0, _vorpalAutocompleteFs2.default)()).alias('prodboot').option('--d, -delimiter  <CSV delimiter>', `The CSV file delimiter. Defaults to "${_config2.default.csvDelimiter}"`).option('--p, -propertyDelimiter  <CSV property delimiter>', `The CSV header delimiter for object properties and arrays indexes. ` + `Defaults to "${_config2.default.propertyDelimiter}"`).option('--dburi <database URI>', `Connection string for the database instance having the format ` + `"username:password@host:port/database?options...." Defaults to ` + `${_config2.default.dbUri}`).validate(function (args) {
    var file = args.filePath;
    if (_path2.default.extname(file) === '.csv') {
      return true;
    } else {
      return `${cli.chalk.cyan(file)} file is not CSV.`;
    }
  }).validate(function (args) {
    var startDateTime = args.startDateTime;

    if (/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/.test(startDateTime)) {
      /* eslint-disable no-new */
      if (isNaN(new Date(startDateTime))) {
        /* eslint-enable no-new */
        return `${cli.chalk.cyan(startDateTime)} is not a valid date.`;
      } else {
        return true;
      }
    } else {
      return `${cli.chalk.cyan(startDateTime)} does not have ${DATE_FORMAT} format.`;
    }
  }).action(function (args) {
    var options = args.options;

    return (0, _productsBootstrap2.default)({
      filePath: args.filePath,
      startDateTime: args.startDateTime,
      category: args.category,
      OB: args.obSelected,
      csvDelimiter: options.d,
      propertyDelimiter: options.p,
      dbUri: options.dburi
    });
  });
};