'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _currencyLast = require('../actions/currency-last');

var _currencyLast2 = _interopRequireDefault(_currencyLast);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (cli) {
  cli.command('currency last', 'Gets and storage the currency info (from openexchangerates.org) of current day.').alias('currlast').action(function (args) {
    var options = args.options;
    return (0, _currencyLast2.default)({
      dbUri: options.dburi
    });
  });
};