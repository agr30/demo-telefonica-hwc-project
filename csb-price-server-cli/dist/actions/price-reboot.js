'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _price = require('../utils/price');

var _csbPriceServerOrm = require('csb-price-server-orm');

var _ = require('../');

var _2 = _interopRequireDefault(_);

var _config = require('../../config');

var _config2 = _interopRequireDefault(_config);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _log = require('../utils/log');

var _csvParse = require('csv-parse');

var _csvParse2 = _interopRequireDefault(_csvParse);

var _streamToPromise = require('stream-to-promise');

var _streamToPromise2 = _interopRequireDefault(_streamToPromise);

var _json = require('../utils/json');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Offering = _csbPriceServerOrm.models.Offering;
var PROPERTY_DELIMITER = void 0,
    numUpdatedPrices = void 0;

/**
 * Saves an offering and related prices to database.
 * @param {Object} id The id for the offering to update.
 * @param {Object} data The data to save.
 * @param {Object} rebootOb The ob to which apply price reboot.
 * @returns {Promise} The save promise.
 */
function updateOffering(offering, _ref, rebootOb) {
  var productOfferingPrice = _ref.productOfferingPrice;

  // Set prices validFor.
  productOfferingPrice.forEach(function (productOfferingPrice) {
    return productOfferingPrice.validFor = { validFor: offering.validFor };
  });

  // Replace existing prices for the OB with new ones.
  var updatedPrices = offering.productOfferingPrice.map(function (currentOfferingPrice) {
    return currentOfferingPrice.priceConsumer[0].id === rebootOb ? productOfferingPrice.filter(function (price) {
      return price.priceLocation === currentOfferingPrice.priceLocation;
    })[0] : currentOfferingPrice;
  });

  // Update offering with new OB price.
  return _csbPriceServerOrm.methods.update(Offering, { id: offering.id }, { productOfferingPrice: updatedPrices });
}

/**
 * Transforms productOfferingPriceProps related data using config, to keep
 * proper location prices.
 * @param {Object} record The record object
 * @param {Object} rebootOb The ob to which apply price reboot.
 * @param {Object} productOfferingPriceProps A key/value pair array of prices
 *  props.
 */
function storeRebootObPrices(record, rebootOb, productOfferingPriceProps) {
  var obConfig = _config2.default.billing[rebootOb];
  var idx = 0;

  // Iterate over OB location prices.
  obConfig.price.forEach(function (price) {
    // For each header transform infromation to populate each price location
    // for each OB.
    Object.keys(productOfferingPriceProps).forEach(function (header) {
      var value = record[header];

      // Price related header.
      if (/\/price\/amount/.test(header)) {
        // If there is no value for base price, there is some misconfiguration.
        (0, _price.checkBillingConfig)(price, productOfferingPriceProps);
        // Add price related data from configuration.
        (0, _price.addConfigData)(record, rebootOb, idx, price, obConfig.tax, productOfferingPriceProps, PROPERTY_DELIMITER);
      } else {
        // Create a indexed header for each location price and link it with
        // record value.
        var transformedHeader = header.replace(/^(productOfferingPrice\/)(.+)$/, `$1${idx}/$2`);
        record[transformedHeader] = value;

        // Finally unflatten as JSON.
        (0, _json.unflatten)(record, transformedHeader, PROPERTY_DELIMITER);
      }
    });

    idx = (0, _price.checkPaidTier)(record, idx);

    idx++;
  });

  // Delete productOfferingPriceProps headers from record.
  Object.keys(productOfferingPriceProps).forEach(function (header) {
    return delete record[header];
  });
}

/**
 * Parses the CSV data into JSON and saves it to the collection.
 * @param {Object} record The key-values record object.
 * @param {Object} rebootOb The ob to which apply price reboot.
 * @returns {Promise} The save promise.
 */
function csvToJson(record, rebootOb) {
  if (record) {
    var productOfferingPriceProps = {};

    // Only productOfferingPrices must be stored for update.
    Object.keys(record).forEach(function (header) {
      if (header.startsWith('productOfferingPrice')) {
        productOfferingPriceProps[header] = record[header];
      } else {
        delete record[header];
      }
    });

    // Store prices by ob specified
    storeRebootObPrices(record, rebootOb, productOfferingPriceProps);

    // First find active offering to get the startDateTime
    return Offering.findOne({
      name: record.productOfferingPrice[0].name,
      deleted: { $ne: true },
      lifeCycleStatus: 'active'
    }).then(function (offering) {
      if (offering) {
        // Finally update offering.
        // console.log('Original offering', JSON.stringify(offering));
        // console.log('Parsed offering', JSON.stringify(record));
        return updateOffering(offering, record, rebootOb).then(function () {
          return numUpdatedPrices += record.productOfferingPrice.length;
        }).catch(_log.logError);
      } else {
        var chalk = _2.default.chalk,
            blue = chalk.blue,
            magenta = chalk.magenta;

        _2.default.log(blue(`No offering found with name ${magenta(record.name)}. Offering will not be updated with ${magenta(rebootOb)} prices.`));
        return Promise.resolve();
      }
    });
  }
  return Promise.resolve();
}

exports.default = function (_ref2) {
  var filePath = _ref2.filePath,
      ob = _ref2.ob,
      category = _ref2.category,
      _ref2$csvDelimiter = _ref2.csvDelimiter,
      csvDelimiter = _ref2$csvDelimiter === undefined ? _config2.default.csvDelimiter : _ref2$csvDelimiter,
      _ref2$propertyDelimit = _ref2.propertyDelimiter,
      propertyDelimiter = _ref2$propertyDelimit === undefined ? _config2.default.propertyDelimiter : _ref2$propertyDelimit,
      _ref2$dbUri = _ref2.dbUri,
      dbUri = _ref2$dbUri === undefined ? _config2.default.dbUri : _ref2$dbUri;

  var parser = (0, _csvParse2.default)({
    delimiter: csvDelimiter,
    auto_parse: true,
    columns: true
  }),
      chalk = _2.default.chalk,
      cyan = chalk.cyan,
      green = chalk.green;

  // Catch any error.
  parser.on('error', function (error) {
    (0, _log.logError)(error);
    _csbPriceServerOrm.db.disconnect();
  });

  return _csbPriceServerOrm.db.connect({ dbUri, dbOptions: _config2.default.dbOptions }, _2.default).then(function () {
    _2.default.log(`Initializing ${cyan(filePath)} CSV import...`);

    PROPERTY_DELIMITER = propertyDelimiter;
    numUpdatedPrices = 0;

    // Parse CSV and save it to database.
    return (0, _streamToPromise2.default)(_fs2.default.createReadStream(filePath).pipe(parser)).then(function (records) {
      var obsLength = _config2.default.billing && Object.keys(_config2.default.billing).length || 0;

      _2.default.log(green(`${cyan(records.length)} records readed from file ${cyan(filePath)}!`));
      _2.default.log(green(`${cyan(obsLength)} OBs readed from config file!`));

      _2.default.log(green(`Rebooting "${cyan(category)}" prices for OB "${cyan(ob)}"...`));

      return Promise.all(records.map(function (record) {
        return csvToJson(record, ob);
      })).then(function () {
        _2.default.log(green(`CSV file ${cyan(filePath)} updated ${cyan(numUpdatedPrices)} records for ${cyan(ob)} OB!`));
      });
    }).then(_csbPriceServerOrm.db.disconnect).catch(function (err) {
      (0, _log.logError)(err);
      throw new Error();
    }).catch(_csbPriceServerOrm.db.disconnect);
  });
};