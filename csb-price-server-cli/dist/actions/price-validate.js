'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _csbPriceServerOrm = require('csb-price-server-orm');

var _decimal = require('decimal.js');

var _decimal2 = _interopRequireDefault(_decimal);

var _ = require('../');

var _2 = _interopRequireDefault(_);

var _config = require('../../config');

var _config2 = _interopRequireDefault(_config);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _csvParse = require('csv-parse');

var _csvParse2 = _interopRequireDefault(_csvParse);

var _streamToPromise = require('stream-to-promise');

var _streamToPromise2 = _interopRequireDefault(_streamToPromise);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var errors = void 0;

/**
 * Logs an error to CLI.
 * @param {String} error
 */
function logError(error) {
  error && _2.default.log(_2.default.chalk.red('ERROR'), error);
}

/**
 * Fins a list of prices with priceLocation and price.amount field matching the
 * name specified.
 * @param {Object} name The name to search for.
 * @param {Promise} The promise of the price found.
 */
function getDbPrices(name) {
  return _csbPriceServerOrm.methods.findPriceByName(name, 'priceLocation price.amount priceConsumer.id').catch(logError);
}

/**
 * Checks if prices on CSV record exists on DB.
 * @param {Object} record The key-values record object.
 * @param {Number} priceDecimals The number of decimal positions.
 * @param {Object} chalk The chalk object.
 * @returns {Promise} The validation promise.
 */
function validatePrice(record, priceDecimals, chalk) {
  if (record) {
    // IMPORTANT: Headers and product code values have spaces around.
    var productCode = record[' PRODUCTCODE (METERING VIVO) '].trim(),
        hasFreeTier = record['Free Tier'],

    // green = chalk.green,
    // cyan = chalk.cyan,
    red = chalk.red,
        magenta = chalk.magenta,
        blue = chalk.blue;

    // Get price headers from CSV.
    Object.keys(record)
    // Make a header normalization.
    .map(function (header) {
      var normalizedHeader = header.trim().toUpperCase();

      record[normalizedHeader] = record[header];
      return normalizedHeader;
    })
    // Filter price columns.
    .filter(function (header) {
      return header.startsWith('PRICE');
    })
    // Make price headers more convenient.
    .forEach(function (header) {
      var priceHeader = header.match(/^PRICE\s+([A-Z]+).*$/)[1];

      record[priceHeader] = record[header];
    });

    return getDbPrices(productCode).then(function () {
      var prices = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      var productOfferingPrice = prices.productOfferingPrice || [],
          obs = _config2.default.billing;
      var dbPrices = {};

      // Get prices stored on DB.
      productOfferingPrice.forEach(function (price) {
        var ob = price.priceConsumer[0].id;
        // Transfrom into a convenient structure i.e
        // {
        //   VIVO: {
        //     BR: 0.072807813,
        //     PE: 0.080088594,
        //     CL: 0.080088594,
        //     AR: 0.080088594,
        //     MX: 0.080088594,
        //     US: 0.080088594
        //   }
        // }
        if (!dbPrices[ob]) {
          dbPrices[ob] = {};
        }
        return dbPrices[ob][price.priceLocation] = price.price.amount;
      });

      // Compare!
      Object.keys(obs).forEach(function (ob) {
        var countryPrices = dbPrices[ob];

        // Check if record has prices for OB.
        if (countryPrices === undefined) {
          _2.default.log(blue(`OB "${magenta(ob)}" for product code ` + `"${magenta(productCode)}" not found on database`));
          return;
        }

        obs[ob].forEach(function (priceConfig) {
          var countryCode = priceConfig.location,
              countryPrice = countryPrices[countryCode];

          // Check if record has prices for location.
          if (countryPrice === undefined) {
            _2.default.log(blue(`DB price for product code "${magenta(productCode)}", OB ` + `"${magenta(ob)}" and country code ` + `"${magenta(countryCode)}" not found on database`));
            return;
          }

          // Get DB price.
          var dbPrice = new _decimal2.default(countryPrice).toDecimalPlaces(priceDecimals),

          // Get final price using location factor.
          csvPrice = new _decimal2.default(record[priceConfig.base]).mul(new _decimal2.default(priceConfig.factor)).toDecimalPlaces(priceDecimals);

          // Some prices may have free tiers so, we need to set CSV price to
          // match Free Tier DB price.
          if (hasFreeTier && parseInt(dbPrice, 10) === 0) {
            csvPrice = dbPrice;
          }

          // Compare csv price with db price.
          if (!csvPrice.equals(dbPrice)) {
            errors++;
            _2.default.log(red(`DB price ${magenta(dbPrice)} for OB ` + `"${magenta(ob)}", product code ` + `"${magenta(productCode)}" and country code ` + `"${magenta(countryCode)}" does not match CSV price ` + `${magenta(csvPrice)}`));
            // Uncomment when verbose option is implemented.
            // } else {
            //   cli.log(
            //     green(
            //       `DB price ${cyan(dbPrice)} for product code ` +
            //         `"${cyan(productCode)}" and country code ` +
            //         `"${cyan(countryCode)}" matched CSV price ` +
            //         `${cyan(csvPrice)}!`
            //     )
            //   );
          }
        });
      });
    }).catch(logError);
  }
  return Promise.resolve();
}

exports.default = function (_ref) {
  var filePath = _ref.filePath,
      _ref$csvDelimiter = _ref.csvDelimiter,
      csvDelimiter = _ref$csvDelimiter === undefined ? _config2.default.csvDelimiter : _ref$csvDelimiter,
      _ref$priceDecimals = _ref.priceDecimals,
      priceDecimals = _ref$priceDecimals === undefined ? _config2.default.priceDecimals : _ref$priceDecimals,
      _ref$dbUri = _ref.dbUri,
      dbUri = _ref$dbUri === undefined ? _config2.default.dbUri : _ref$dbUri;

  var parser = (0, _csvParse2.default)({
    delimiter: csvDelimiter,
    auto_parse: false,
    columns: true
  }),
      chalk = _2.default.chalk,
      red = chalk.red,
      magenta = chalk.magenta,
      cyan = chalk.cyan,
      green = chalk.green;

  // Catch any error.
  parser.on('error', function (error) {
    logError(error);
    _csbPriceServerOrm.db.disconnect();
  });

  return _csbPriceServerOrm.db.connect({ dbUri, dbOptions: _config2.default.dbOptions }, _2.default).then(function () {
    _2.default.log(`Initializing ${cyan(filePath)} CSV import...`);
    // Parse CSV and save it to database.
    return (0, _streamToPromise2.default)(_fs2.default.createReadStream(filePath).pipe(parser)).then(function (records) {
      _2.default.log(green(`${cyan(records.length)} records readed from file ${cyan(filePath)}!`));

      errors = 0;
      return Promise.all(records.map(function (record) {
        return validatePrice(record, priceDecimals, chalk);
      })).then(function (validatePromises) {
        _2.default.log(green(`${cyan(validatePromises.length)} records from CSV file ${cyan(filePath)} validated!`));
        errors && _2.default.log(red(`${magenta(errors)} errors found!`));
      });
    }).then(_csbPriceServerOrm.db.disconnect).catch(_csbPriceServerOrm.db.disconnect);
  });
};