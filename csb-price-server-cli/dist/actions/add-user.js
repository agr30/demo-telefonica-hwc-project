'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ = require('../');

var _2 = _interopRequireDefault(_);

var _config = require('../../config');

var _config2 = _interopRequireDefault(_config);

var _csbPriceServerOrm = require('csb-price-server-orm');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (_ref) {
  var userName = _ref.userName,
      password = _ref.password,
      _ref$role = _ref.role,
      role = _ref$role === undefined ? 'read' : _ref$role,
      _ref$dbUri = _ref.dbUri,
      dbUri = _ref$dbUri === undefined ? _config2.default.dbUri : _ref$dbUri;

  var chalk = _2.default.chalk,
      cyan = chalk.cyan,
      red = chalk.red,
      green = chalk.green;

  return _csbPriceServerOrm.db.connect({ dbUri, dbOptions: _config2.default.dbOptions }, _2.default).then(function (connection) {
    return connection.db.addUser(userName, password, {
      roles: [role]
    }, function (error) {
      _2.default.log(error ? `${red('ERROR')} ${error}` : green(`User ${cyan(userName)} added to db with ${cyan(role)} rights!`));
      _csbPriceServerOrm.db.disconnect();
    });
  });
};