'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _price = require('../utils/price');

var _csbPriceServerOrm = require('csb-price-server-orm');

var _ = require('../');

var _2 = _interopRequireDefault(_);

var _config = require('../../config');

var _config2 = _interopRequireDefault(_config);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _log = require('../utils/log');

var _csvParse = require('csv-parse');

var _csvParse2 = _interopRequireDefault(_csvParse);

var _db = require('../utils/db');

var _streamToPromise = require('stream-to-promise');

var _streamToPromise2 = _interopRequireDefault(_streamToPromise);

var _json = require('../utils/json');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Product = _csbPriceServerOrm.models.Product,
    Offering = _csbPriceServerOrm.models.Offering,
    PRODUCTS_COLLECTION = 'products',
    OFFERINGS_COLLECTION = 'offerings';
var PROPERTY_DELIMITER = void 0,
    numSavedOfferings = void 0;

/**
 * Saves a product to database.
 * @param {Object} data The data to save.
 * @param {String} startDateTime The startDateTime as ISO String.
 * @returns {Promise} The save promise.
 */
function saveProducts(data, startDateTime) {
  return (0, _db.saveWithHref)(Product, data, startDateTime);
}

/**
 * Saves an offering and related prices to database.
 * @param {Object} data The data to save.
 * @param {String} startDateTime The startDateTime as ISO String.
 * @returns {Promise} The save promise.
 */
function saveOfferings(data, startDateTime) {
  // Set prices validFor startDateTime.
  data.productOfferingPrice.forEach(function (productOfferingPrice) {
    return productOfferingPrice.validFor = { startDateTime };
  });

  return (0, _db.saveWithHref)(Offering, data, startDateTime);
}

/**
 * Saves a parsed record into database.
 * @param {Object} record The record data.
 * @param {String} collection The collection name.
 * @param {String} startDateTime The startDateTime as ISO String.
 * @returns {Promise} The save promise.
 */
function saveRecord(record, collection, startDateTime) {
  switch (collection) {
    case PRODUCTS_COLLECTION:
      return saveProducts(record, startDateTime);
    case OFFERINGS_COLLECTION:
      return saveOfferings(record, startDateTime);
    default:
      return Promise.resolve();
  }
}

/**
 * Transforms productOfferingPriceProps related data using config, to keep
 * proper location prices.
 * @param {Object} record The record object
 * @param {Object} productOfferingPriceProps A key/value pair array of prices
 *  props.
 */
function storePricesByLocation(record, productOfferingPriceProps) {
  var billing = _config2.default.billing;
  var idx = 0;

  // Iterate over OBs
  Object.keys(billing).forEach(function (ob) {
    var obConfig = _config2.default.billing[ob];

    // Iterate over each OB location prices.
    obConfig.price.forEach(function (price) {
      // Por cada registro, compruebo sobre la OB-Location para comprobar si subo el precio.

      if (record[`${ob}-${price.location}`] === 'undefined' || record[`${ob}-${price.location}`] !== 'false') {
        // For each header transform infromation to populate each price location
        // for each OB.
        Object.keys(productOfferingPriceProps).forEach(function (header) {
          var value = record[header];

          // Price related header.
          if (/\/price\/amount/.test(header)) {
            // If there is no value for base price, there is some misconfiguration.
            (0, _price.checkBillingConfig)(price, productOfferingPriceProps);
            // Add price related data from configuration.
            (0, _price.addConfigData)(record, ob, idx, price, obConfig.tax, productOfferingPriceProps, PROPERTY_DELIMITER);
          } else {
            // Create a indexed header for each location price and link it with
            // record value.
            var transformedHeader = header.replace(/^(productOfferingPrice\/)(.+)$/, `$1${idx}/$2`);
            record[transformedHeader] = value;

            // Finally unflatten as JSON.
            (0, _json.unflatten)(record, transformedHeader, PROPERTY_DELIMITER);
          }
        });

        idx = (0, _price.checkPaidTier)(record, idx);

        idx++;
      }
    });
  });

  // Delete productOfferingPriceProps headers from record.
  Object.keys(productOfferingPriceProps).forEach(function (header) {
    return delete record[header];
  });
}

/**
 * Parses the CSV data into JSON and saves it to the collection.
 * @param {Object} record The key-values record object.
 * @param {String} startDateTime The startDateTime date in yyyy-MM-dd format.
 * @returns {Promise} The save promise.
 */
function csvToJson(record, startDateTime) {
  if (record) {
    var productOfferingPriceProps = {};

    // Store productOfferingPrice properties and unflatten as JSON other
    // columns.
    Object.keys(record).forEach(function (header) {
      if (header.startsWith('productOfferingPrice')) {
        productOfferingPriceProps[header] = record[header];
      } else {
        (0, _json.unflatten)(record, header, PROPERTY_DELIMITER);
      }
    });

    // Store prices by location
    storePricesByLocation(record, productOfferingPriceProps);
    record.productOfferingPrice = record.productOfferingPrice ? record.productOfferingPrice.filter(function (price) {
      return price;
    }) : [];

    // Search for products with the same name and generate product specs
    return Product.find({ name: record.name }).exec().then(function (products) {
      if (products && products.length) {
        record.productSpecification = [];

        products.forEach(function (product) {
          return record.productSpecification.push({
            id: product.id,
            href: product.href,
            name: product.name
          });
        });

        // Finally save offering.
        // console.log('Parsed offering', JSON.stringify(record));
        return saveRecord(record, OFFERINGS_COLLECTION, new Date(startDateTime).toISOString()).then(function (offering) {
          numSavedOfferings++;

          return Promise.all(products.map(function (product) {
            return (
              // Make product -> offering relationship.
              _csbPriceServerOrm.methods.updateById(Product, product.id, {
                offerings: [{
                  id: offering.id,
                  href: offering.href,
                  name: offering.name,
                  validFor: offering.validFor,
                  description: offering.description
                }]
              }, 'id')
            );
          }));
        });
      } else {
        var chalk = _2.default.chalk,
            blue = chalk.blue,
            magenta = chalk.magenta;

        _2.default.log(blue(`No product found with name ${magenta(record.name)}. Offering will not be loaded into database.`));
        return Promise.resolve();
      }
    }).catch(_log.logError);
  }
  return Promise.resolve();
}

/**
 * Deletes all entities for categorized service.
 * @param {String} category The category to delete.
 * @return {Promise} The promise that all documents have been removed.
 */
function deleteServiceEntity(Entity, category) {
  return _csbPriceServerOrm.methods.remove(Entity, {
    'category.id': category
  }).catch(_log.logError);
}

exports.default = function (_ref) {
  var filePath = _ref.filePath,
      startDateTime = _ref.startDateTime,
      category = _ref.category,
      _ref$csvDelimiter = _ref.csvDelimiter,
      csvDelimiter = _ref$csvDelimiter === undefined ? _config2.default.csvDelimiter : _ref$csvDelimiter,
      _ref$propertyDelimite = _ref.propertyDelimiter,
      propertyDelimiter = _ref$propertyDelimite === undefined ? _config2.default.propertyDelimiter : _ref$propertyDelimite,
      _ref$dbUri = _ref.dbUri,
      dbUri = _ref$dbUri === undefined ? _config2.default.dbUri : _ref$dbUri;

  var parser = (0, _csvParse2.default)({
    delimiter: csvDelimiter,
    auto_parse: true,
    columns: true
  }),
      chalk = _2.default.chalk,
      cyan = chalk.cyan,
      green = chalk.green;

  // Catch any error.
  parser.on('error', function (error) {
    (0, _log.logError)(error);
    _csbPriceServerOrm.db.disconnect();
  });

  return _csbPriceServerOrm.db.connect({ dbUri, dbOptions: _config2.default.dbOptions }, _2.default).then(function () {
    _2.default.log(`Initializing ${cyan(filePath)} CSV import...`);

    PROPERTY_DELIMITER = propertyDelimiter;
    numSavedOfferings = 0;

    // Parse CSV and save it to database.
    return (0, _streamToPromise2.default)(_fs2.default.createReadStream(filePath).pipe(parser)).then(function (records) {
      var obsLength = _config2.default.billing && Object.keys(_config2.default.billing).length || 0;

      _2.default.log(green(`${cyan(records.length)} records readed from file ${cyan(filePath)}!`));
      _2.default.log(green(`${cyan(obsLength)} OBs readed from config file!`));

      // Empty category data.
      _2.default.log(green(`Deleting "${cyan(category)}" categorized offerings...`));

      return deleteServiceEntity(Offering, category).then(function () {
        _2.default.log(green(`Loading and saving "${cyan(category)}" prices...`));

        return Promise.all(records.map(function (record) {
          return csvToJson(record, startDateTime);
        })).then(function () {
          _2.default.log(green(`CSV file ${cyan(filePath)} saved ${cyan(numSavedOfferings)} records along ${cyan(obsLength)} OBs!`));
        });
      });
    }).then(_csbPriceServerOrm.db.disconnect).catch(function (err) {
      (0, _log.logError)(err);
      throw new Error();
    }).catch(_csbPriceServerOrm.db.disconnect);
  });
};