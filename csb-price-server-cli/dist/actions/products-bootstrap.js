'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _csbPriceServerOrm = require('csb-price-server-orm');

var _db = require('../utils/db');

var _ = require('../');

var _2 = _interopRequireDefault(_);

var _config = require('../../config');

var _config2 = _interopRequireDefault(_config);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _log = require('../utils/log');

var _csvParse = require('csv-parse');

var _csvParse2 = _interopRequireDefault(_csvParse);

var _streamToPromise = require('stream-to-promise');

var _streamToPromise2 = _interopRequireDefault(_streamToPromise);

var _json = require('../utils/json');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Product = _csbPriceServerOrm.models.Product,
    Offering = _csbPriceServerOrm.models.Offering;
var PROPERTY_DELIMITER = void 0;

/**
 * Saves a product to database.
 * @param {Object} data The data to save.
 * @param {String} startDateTime The startDateTime as ISO String.
 * @returns {Promise} The save promise.
 */
function saveProduct(data, startDateTime) {
  return (0, _db.saveWithHref)(Product, data, startDateTime);
}

/**
 * Checks if productSpecCharacteristic isPresent, in that case deletes existing
 * index data and stores notPresent into array to not save next index records.
 * @param {Object} record The record data.
 * @param {String} header The flattened header to be converted to JSON.
 * @param {Array<String>} notPresentProdSpecChars An array storing not present
 *  productSpecCharacteristics.
 */
function checkProdSpecIsPresent(record, header, notPresentProdSpecChars) {
  var indexMatch = header.match(/^productSpecCharacteristic\/(\d{1,2})\/.+$/),
      index = indexMatch[1];

  if (/^productSpecCharacteristic\/\d{1,2}\/isPresent$/.test(header)) {
    // Check if isPresent header has false value, and then add to notPresent
    // array and delete previous stored data on record.
    if (record[header] === 'false') {
      notPresentProdSpecChars.push(index);
      record.productSpecCharacteristic && record.productSpecCharacteristic.splice(index, 1);
    }
  } else if (!notPresentProdSpecChars.includes(index)) {
    // If is not isPresent header and index is not on notPresent array,
    // unflatten header as usual.
    (0, _json.unflatten)(record, header, PROPERTY_DELIMITER);
  }

  // Delete productSpecCharacteristic headers from record.
  delete record[header];
}

/**
 * Parses the CSV data into JSON and saves it to the collection.
 * @param {Object} record The key-values record object.
 * @param {String} startDateTime The startDateTime date in yyyy-MM-dd format.
 * @returns {Promise} The save promise.
 */
function csvToJson(record, startDateTime) {
  if (record) {
    var notPresentProdSpecChars = [];

    // Check id productSpecCharacteristic isPresent and unflatten as JSON other
    // columns.
    Object.keys(record).forEach(function (header) {
      if (header.startsWith('productSpecCharacteristic')) {
        checkProdSpecIsPresent(record, header, notPresentProdSpecChars);
      } else {
        (0, _json.unflatten)(record, header, PROPERTY_DELIMITER);
      }
    });

    // Filter to remove null values due to isPresent logic.
    record.productSpecCharacteristic = record.productSpecCharacteristic && record.productSpecCharacteristic.filter(function (value) {
      return value;
    });

    // console.log('Parsed product', JSON.stringify(record));
    // Save product
    return saveProduct(record, new Date(startDateTime).toISOString());
  }
  return Promise.resolve();
}

/**
 * Deletes all entities for categorized service.
 * @param {String} category The category to delete.
 * @return {Promise} The promise that all documents have been removed.
 */
function deleteServiceEntity(Entity, category) {
  return (0, _db.deleteEntity)(Entity, {
    'category.id': category
  });
}

exports.default = function (_ref) {
  var filePath = _ref.filePath,
      startDateTime = _ref.startDateTime,
      category = _ref.category,
      _ref$csvDelimiter = _ref.csvDelimiter,
      csvDelimiter = _ref$csvDelimiter === undefined ? _config2.default.csvDelimiter : _ref$csvDelimiter,
      _ref$propertyDelimite = _ref.propertyDelimiter,
      propertyDelimiter = _ref$propertyDelimite === undefined ? _config2.default.propertyDelimiter : _ref$propertyDelimite,
      _ref$dbUri = _ref.dbUri,
      dbUri = _ref$dbUri === undefined ? _config2.default.dbUri : _ref$dbUri;

  var parser = (0, _csvParse2.default)({
    delimiter: csvDelimiter,
    auto_parse: true,
    columns: true
  }),
      chalk = _2.default.chalk,
      cyan = chalk.cyan,
      green = chalk.green;

  // Catch any error.
  parser.on('error', function (error) {
    (0, _log.logError)(error);
    _csbPriceServerOrm.db.disconnect();
  });

  return _csbPriceServerOrm.db.connect({ dbUri, dbOptions: _config2.default.dbOptions }, _2.default).then(function () {
    _2.default.log(`Initializing ${cyan(filePath)} CSV import...`);

    PROPERTY_DELIMITER = propertyDelimiter;

    // Parse CSV and save it to database.
    return (0, _streamToPromise2.default)(_fs2.default.createReadStream(filePath).pipe(parser)).then(function (records) {
      var obsLength = _config2.default.billing && Object.keys(_config2.default.billing).length || 0;

      _2.default.log(green(`${cyan(records.length)} records readed from file ${cyan(filePath)}!`));
      _2.default.log(green(`${cyan(obsLength)} OBs readed from config file!`));

      // Empty category data.
      _2.default.log(green(`Deleting existing categorized "${cyan(category)}" products and offerings data...`));

      return deleteServiceEntity(Product, category).then(deleteServiceEntity(Offering, category)).then(function () {
        _2.default.log(green(`Loading and saving "${cyan(category)}" products...`));

        return Promise.all(records.map(function (record) {
          return csvToJson(record, startDateTime);
        })).then(function (savePromises) {
          _2.default.log(green(`CSV file ${cyan(filePath)} saved ${cyan(savePromises.length)} records!`));
        });
      });
    }).then(_csbPriceServerOrm.db.disconnect).catch(function (err) {
      (0, _log.logError)(err);
      throw new Error();
    }).catch(_csbPriceServerOrm.db.disconnect);
  });
};