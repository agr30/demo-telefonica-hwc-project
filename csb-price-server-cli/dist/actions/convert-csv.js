'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _q = require('q');

var _q2 = _interopRequireDefault(_q);

var _ = require('../');

var _2 = _interopRequireDefault(_);

var _config = require('../../config');

var _config2 = _interopRequireDefault(_config);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _csvtojson = require('csvtojson');

var _csvtojson2 = _interopRequireDefault(_csvtojson);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Logs an error to CLI.
 * @param {String} error
 */
function logError(error) {
    error && _2.default.log(_2.default.chalk.red('ERROR'), error);
}

/**
 * Make the CSV conversion
 * @param {String} type of items in the CSV ("products" || "offerings")
 * @param {String} main category associated to the items in the CSV ("opencloud" || "cloudserver")
 * @param {String} path where input CSV files are stored
 * @param {String} file with CSV including "price-catalog" sheet
 * @param {String} file with CSV including "ECS details" sheet
 * @param {String} file with CSV including "RDS details" sheet
 * @param {String} file with CSV including "Baremetal details" sheet
 * @param {String} path to file where the output CSV will be written
 * @returns (Promise) error or a string confirming the success of the operations
 */
function csvConversion(itemsType, category, inputFolder, inputFile, multiple, inputFolder2, inputFile2, ecsFile, rdsFile, baremetalFile, mrsFile, natGatewayFile, dedicatedHostFile, workspaceFile, outputPath) {
    var defer = new _q2.default.defer();

    if (itemsType === 'offerings') {
        generateOfferingsCSVPriceServer(inputFolder + inputFile, category || 'opencloud', function (error, lines) {
            if (error) {
                defer.reject(error);
            } else {
                _fs2.default.writeFile(outputPath, lines, function () {
                    console.log('Offerings CSV file saved');
                    defer.resolve({ message: 'Offerings CSV file generated' });
                });
            }
        });
    } else {
        generateProductsCSVPriceServer(inputFolder, inputFile, multiple, inputFolder2, inputFile2, category || 'opencloud', ecsFile, rdsFile, baremetalFile, mrsFile, natGatewayFile, dedicatedHostFile, workspaceFile, function (error, lines) {
            if (error) {
                defer.reject(error);
            } else {
                _fs2.default.writeFile(outputPath, lines, function () {
                    console.log('Products CSV file saved');
                    defer.resolve({ message: 'Products CSV file generated' });
                });
            }
        });
    }

    return defer.promise;
}

function generateOfferingsCSVPriceServer(path, category, callback) {
    var i, j, productCode, priceLatamColumn, priceOthersColumn, priceBrazilColumn;
    var additionalDatas = ['resourceTypeCode', 'measureType', 'measureField', 'elegible4Discount', 'pricingElement', 'measureUnits', 'units', 'unitDivisor'];
    var OBs = ['VIVO', 'AR', 'US', 'PE', 'CL', 'MX'];
    var regions = ['BR', 'AR', 'US', 'PE', 'CL', 'MX'];
    var lines = 'name;description;category/0/id;category/1/id;productOfferingPrice/name;productOfferingPrice/unitOfMeasure/amount;' + 'productOfferingPrice/unitOfMeasure/units;productOfferingPrice/unitOfMeasure/minimum;productOfferingPrice/unitOfMeasure/maximum;' + 'productOfferingPrice/price/amount/LATAM;productOfferingPrice/price/amount/OTHER;productOfferingPrice/price/amount/BRAZIL;productOfferingPrice/price/units;' + 'productOfferingPrice/priceConsumer/0/entityType;productOfferingPrice/pricedComponents/0/key;productOfferingPrice/pricedComponents/0/value;' + 'productOfferingPrice/additionalData/0/key;productOfferingPrice/additionalData/0/value;lifeCycleStatus;';

    for (i = 0; i < additionalDatas.length; i++) {
        lines += additionalDataHeader(i) + ';';
    }

    // Include the column names specifying the service availability for each OB and region (OB/Region)
    for (i = 0; i < OBs.length - 1; i++) {
        for (j = 0; j < regions.length; j++) {
            lines += OBs[i] + '-' + regions[j] + ';';
        }
    }
    for (j = 0; j < regions.length - 1; j++) {
        lines += OBs[i] + '-' + regions[j] + ';';
    }
    lines += OBs[i] + '-' + regions[j] + '\n';

    (0, _csvtojson2.default)({ delimiter: ';' }).fromFile(path).on('json', function (jsonObj, rowIndex) {
        if (rowIndex == 0) {
            // Determine the exact name of the column including the price for each country, to avoid problems with spaces, etc.
            Object.keys(jsonObj).forEach(function (key) {
                if (key.indexOf('PRICE LATAM') != -1) {
                    priceLatamColumn = key;
                } else if (key.indexOf('PRICE OTHERS') != -1) {
                    priceOthersColumn = key;
                } else if (key.indexOf('PRICE BRAZIL') != -1) {
                    priceBrazilColumn = key;
                }
            });
        }

        productCode = getProductCode(jsonObj);

        if (productCode) {
            var newLine = productCode + ';';
            newLine += jsonObj.productName.trim() + ';';
            newLine += category + ';';
            newLine += jsonObj.cloudServiceTypeCode.trim() + ';';
            newLine += productCode + ';';

            newLine += '1' + ';';
            newLine += jsonObj.units.trim() + ';';

            // Huawei requested to split amount and units, but finally, to not break backwards compatibility,
            // amount will always be taken as 1 and units will include the whole units string
            /*var units = jsonObj.units.trim().split(' ');
            if (units.length == 1 || isNaN(units[0])) {
                newLine += '1' + ';';
                newLine += jsonObj.units.trim() + ';';
            } else {
                newLine += units[0] + ';';
                newLine += units[1] + ';';
            }*/

            // productOfferingPrice/unitOfMeasure/minimum  and  productOfferingPrice/unitOfMeasure/maximum fields, based on Excel column "R"
            if (jsonObj.promotions || jsonObj.Promotions) {
                if (!jsonObj.promotions) {
                    jsonObj.promotions = jsonObj.Promotions;
                }
                var max = jsonObj.promotions.substring(jsonObj.promotions.lastIndexOf('-') + 1).trim();
                newLine += '1;' + max + ';';
            } else {
                newLine += ';;';
            }

            newLine += priceLatamColumn ? (jsonObj[priceLatamColumn].replace(',', '.') || 0) + ';' : ';';
            newLine += priceOthersColumn ? (jsonObj[priceOthersColumn].replace(',', '.') || 0) + ';' : ';';
            newLine += priceBrazilColumn ? (jsonObj[priceBrazilColumn].replace(',', '.') || 0) + ';' : ';';
            newLine += 'USD' + ';';
            newLine += 'OB' + ';';
            newLine += 'accumulateFactorName' + ';';
            newLine += jsonObj.accumulateFactorName.trim() + ';';
            newLine += 'divisionType' + ';';
            newLine += (jsonObj.promotions ? jsonObj.divisionType || 'step' : 'step') + ';';
            newLine += 'active' + ';';

            newLine += 'resourceTypeCode' + ';';
            newLine += jsonObj.resourceTypeCode.trim() + ';';
            newLine += 'measureType' + ';';
            newLine += jsonObj.measureType.trim() + ';';
            newLine += 'measureField' + ';';
            newLine += jsonObj.measureField.trim() + ';';
            newLine += 'elegible4Discount' + ';';
            newLine += (jsonObj['Committed Discount'] === 'YES') + ';';
            newLine += 'pricingElement' + ';';
            newLine += jsonObj.pricingElement.trim() + ';';
            newLine += 'measureUnits' + ';';
            newLine += jsonObj.measureUnits.trim() + ';';
            newLine += 'units' + ';';
            newLine += jsonObj.units.trim() + ';';
            newLine += 'unitDivisor' + ';';
            newLine += jsonObj.unitDivisor + ';';

            // Include the columns specifying the service availability for each OB and region (OB/Region)
            for (i = 0; i < OBs.length - 1; i++) {
                for (j = 0; j < regions.length; j++) {
                    newLine += (jsonObj[OBs[i] + '/' + regions[j]] === 'YES') + ';';
                }
            }
            for (j = 0; j < regions.length - 1; j++) {
                newLine += (jsonObj[OBs[i] + '/' + regions[j]] === 'YES') + ';';
            }
            newLine += (jsonObj[OBs[i] + '/' + regions[j]] === 'YES') + '\n';

            if (jsonObj.Fase && jsonObj.Fase === '2' && !jsonObj[priceBrazilColumn]) {
                // avoid loading CS4VDC products ofr phase 2 until they would have a price defined
                newLine = '';
            }

            lines += newLine;
        }
    }).on('done', function (error) {
        if (error) {
            console.log('error:', error);
            return callback(error);
        }
        return callback(null, lines);
    });
}

function additionalDataHeader(index) {
    var addData = 'additionalData/' + index + '/key;';
    addData += 'additionalData/' + index + '/value';
    return addData;
}

function prodSpecCharHeader(index) {
    var prodSpecH = '',
        i;
    var pscHeaders = ['/isPresent;', '/name;', '/description;', '/valueType;', '/productSpecCharacteristicValue/0/unitOfMeasure/amount;', '/productSpecCharacteristicValue/0/unitOfMeasure/units;', '/productSpecCharacteristicValue/0/default;', '/productSpecCharacteristicValue/0/value;', '/productSpecCharacteristicValue/0/valueFrom;', '/productSpecCharacteristicValue/0/valueTo;', '/additionalData/0/key;', '/additionalData/0/value'];

    for (i = 0; i < pscHeaders.length; i++) {
        prodSpecH += 'productSpecCharacteristic/' + index + pscHeaders[i];
    }

    return prodSpecH;
}

function getLinearAttr(jsonObj, prodSpecs, category, index) {
    var name = jsonObj[prodSpecs[category][index].column];
    var isPresent = name ? 'true' : 'false';
    var defaultValue = false;

    var prodSpec = isPresent + ';';
    prodSpec += (name || '') + ';';
    prodSpec += (name || '') + ';';
    prodSpec += 'integer;';
    prodSpec += '1;';
    prodSpec += jsonObj.measureUnits.trim() + ';';
    prodSpec += defaultValue + ';';
    prodSpec += (jsonObj.linearAttrMinValue || 0) + ';';
    prodSpec += (jsonObj.linearAttrMinValue || 0) + ';';
    prodSpec += (jsonObj.linearAttrMaxValue || 0) + ';';
    prodSpec += 'domainType' + ';';
    prodSpec += 'LINEAR';

    return prodSpec;
}

function addAdditionalData(jsonObj, ad, i) {
    var line;
    switch (ad[i]) {
        case 'elegible4Discount':
            line = ad[i] + ';' + (jsonObj['Committed Discount'] === 'YES') + ';';
            break;
        case 'specification':
            line = ad[i] + ';' + jsonObj.Specification.replace(/;/g, ',').replace(/\n/g, ' / ') + ';';
            break;
        case 'calculatorName':
            line = ad[i] + ';' + (jsonObj['calculator names'] || jsonObj.productName) + ';';
            break;
        default:
            line = ad[i] + ';' + jsonObj[ad[i]] + ';';
    }
    return line;
}

function generateProductsCSVPriceServer(inputFolder, fileName, multiple, inputFolder2, fileName2, category, ecsFile, rdsFile, baremetalFile, mrsFile, natGatewayFile, dedicatedHostFile, workspaceFile, callback) {
    var i = 0,
        ecs,
        rds,
        baremetal,
        mrs,
        nat,
        dedicated,
        workspace,
        aux,
        productCodes = [];
    var additionalDatas = ['resourceTypeCode', 'resourceSpecCode', 'elegible4Discount', 'accumulateFactorName', 'measureType', 'measureField', 'measureUnits', 'units', 'unitDivisor', 'specification', 'calculatorName', 'productId'];
    var prodSpecs = {
        opencloud: [{ name: 'image', description: 'OS', valueType: 'string', units: 'os', column: 'image',
            categories: ['hws.service.type.ec2', 'hws.service.type.rds', 'hws.service.type.baremetal', 'hws.service.type.bigdata', 'hws.service.type.workspace'] }, { name: 'cpu', description: 'CPU', valueType: 'string', units: 'cpu', column: 'cpu',
            categories: ['hws.service.type.baremetal', 'hws.service.type.deh'] }, { name: 'vcpu', description: 'vCPU', valueType: 'integer', units: 'cpu', column: 'vCPU',
            categories: ['hws.service.type.ec2', 'hws.service.type.rds', 'hws.service.type.bigdata', 'hws.service.type.deh', 'hws.service.type.workspace'] }, { name: 'mem', description: 'Memory', valueType: 'integer', units: 'gb', column: 'mem',
            categories: ['hws.service.type.ec2', 'hws.service.type.rds', 'hws.service.type.baremetal', 'hws.service.type.bigdata', 'hws.service.type.deh', 'hws.service.type.workspace'] }, { name: 'disk', description: 'disk(Instance Storage)', valueType: 'integer', units: 'gb', column: 'disk',
            categories: ['hws.service.type.baremetal'] }, { name: 'type', description: 'Type', valueType: 'string', units: 'type', column: 'type',
            categories: ['hws.service.type.workspace'] }, { name: 'dbMaxConnects', description: 'DBMaxConnects', valueType: 'integer', units: 'connection', column: 'dbMaxConnects',
            categories: ['hws.service.type.rds'] }, { name: 'serverType', description: 'ServerType', valueType: 'string', units: 'servertype', column: 'Server type',
            categories: ['hws.service.type.baremetal', 'hws.service.type.deh'] }, { name: 'maxConnection', description: 'Max Connection', valueType: 'integer', units: 'connection', column: 'max connection',
            categories: ['hws.service.type.vpc'] }, { name: 'newConnectionPerSec', description: 'New Connection per Second', valueType: 'integer', units: 'connection', column: 'new connection per second',
            categories: ['hws.service.type.vpc'] }, { name: 'dbType', description: 'DB Type', valueType: 'string', units: 'dbtype', column: 'dbType',
            categories: ['hws.service.type.rds'] }, { name: 'linearAttr', description: 'linearAttr', valueType: 'integer', units: 'gb', column: 'linearAttrName' }],
        cloudserver: [{ name: 'vcpu', description: 'vCPU', valueType: 'integer', units: 'cpu', column: 'cpu', categories: ['acens.service.type.ec2'] }]
    };

    function prodSpecChar(resourceSpecCode, jsonObj, index) {
        var spec = prodSpecs[category][index];

        if (spec.name === 'linearAttr') {
            return getLinearAttr(jsonObj, prodSpecs, category, index);
        }
        var cat = jsonObj.cloudServiceTypeCode.trim();
        var isPresent = spec.categories.indexOf(cat) != -1 ? 'true' : 'false';

        var defaultValue = false;
        var amount = '1';
        var value = '';
        switch (cat) {
            case 'hws.service.type.ec2':
                value = ecs[resourceSpecCode] ? ecs[resourceSpecCode][spec.column] || '' : '';
                break;
            case 'hws.service.type.rds':
                value = rds[resourceSpecCode] ? rds[resourceSpecCode][spec.column] || '' : '';
                break;
            case 'hws.service.type.baremetal':
                value = baremetal[resourceSpecCode] ? baremetal[resourceSpecCode][spec.column] || '' : '';
                break;
            case 'hws.service.type.bigdata':
                value = mrs[resourceSpecCode] ? mrs[resourceSpecCode][spec.column] || '' : '';
                break;
            case 'hws.service.type.vpc':
                value = nat[resourceSpecCode] ? nat[resourceSpecCode][spec.column] || '' : '';
                break;
            case 'hws.service.type.deh':
                value = dedicated[resourceSpecCode] ? dedicated[resourceSpecCode][spec.column] || '' : '';
                break;
            case 'hws.service.type.workspace':
                value = workspace[resourceSpecCode] ? workspace[resourceSpecCode][spec.column] || '' : '';
        }

        if (isPresent === 'true' && value === '') {
            // Product do not have specification for this property or product has been removed
            // return 'deleted';
            // Instead of not creating the product because of the lack of a characteristic, fill with a default value
            isPresent = false;
            value = '-';
        }

        var prodSpec = isPresent + ';';
        prodSpec += spec.name + ';';
        prodSpec += spec.description + ';';
        prodSpec += spec.valueType + ';';
        prodSpec += amount + ';';
        prodSpec += spec.units + ';';
        prodSpec += defaultValue + ';';
        prodSpec += value.replace(/;/g, ',').replace(/\n/g, ' / ') + ';';
        prodSpec += ';';
        prodSpec += ';';
        prodSpec += 'domainType' + ';';
        prodSpec += 'ENUMERATION';

        return prodSpec;
    }

    var lines = 'name;description;category/0/id;category/1/id;lifeCycleStatus;';

    for (i = 0; i < additionalDatas.length; i++) {
        lines += additionalDataHeader(i) + ';';
    }
    for (i = 0; i < prodSpecs[category].length - 1; i++) {
        lines += prodSpecCharHeader(i) + ';';
    }
    lines += prodSpecCharHeader(i) + '\n';

    getObjectFromCSV(inputFolder + ecsFile, function (err, result) {
        if (err) {
            console.log('error:', err);
            return callback(err);
        }
        ecs = result;
        getObjectFromCSV(inputFolder + rdsFile, function (err, result2) {
            if (err) {
                console.log('error:', err);
                return callback(err);
            }
            rds = result2;
            getObjectFromCSV(inputFolder + baremetalFile, function (err, result3) {
                if (err) {
                    console.log('error:', err);
                    return callback(err);
                }
                baremetal = result3;
                getObjectFromCSV(inputFolder + mrsFile, function (err, result4) {
                    if (err) {
                        console.log('error:', err);
                        return callback(err);
                    }
                    mrs = result4;
                    getObjectFromCSV(inputFolder + natGatewayFile, function (err, result5) {
                        if (err) {
                            console.log('error:', err);
                            return callback(err);
                        }
                        nat = result5;
                        getObjectFromCSV(inputFolder + dedicatedHostFile, function (err, result6) {
                            if (err) {
                                console.log('error:', err);
                                return callback(err);
                            }
                            dedicated = result6;
                            getObjectFromCSV(inputFolder + workspaceFile, function (err, result7) {
                                if (err) {
                                    console.log('error:', err);
                                    return callback(err);
                                }
                                workspace = result7;

                                (0, _csvtojson2.default)({ delimiter: ';' }).fromFile(inputFolder + fileName).on('json', function (jsonObj, rowIndex) {
                                    var productCode = getProductCode(jsonObj);
                                    var deleted = false;

                                    if (productCode) {
                                        productCodes.push(productCode);
                                        var newLine = productCode + ';';
                                        newLine += jsonObj.productName.trim() + ';';
                                        newLine += category + ';';
                                        newLine += jsonObj.cloudServiceTypeCode.trim() + ';';
                                        newLine += 'active' + ';';

                                        // add additionalData paramteres
                                        for (i = 0; i < additionalDatas.length; i++) {
                                            newLine += addAdditionalData(jsonObj, additionalDatas, i);
                                        }

                                        // add productSpecCharacteristic parameters
                                        for (i = 0; i < prodSpecs[category].length - 1; i++) {
                                            aux = prodSpecChar(jsonObj.resourceSpecCode.trim(), jsonObj, i);
                                            deleted = aux === 'deleted'; // Do not include removed products
                                            newLine += aux + ';';
                                        }
                                        aux = prodSpecChar(jsonObj.resourceSpecCode.trim(), jsonObj, i);
                                        deleted = aux === 'deleted'; // Do not include removed products
                                        newLine += aux + '\n';

                                        if (!deleted) {
                                            if (jsonObj.Fase && jsonObj.Fase === '2' && !jsonObj['PRICE BRAZIL']) {
                                                // avoid loading CS4VDC products ofr phase 2 until they would have a price defined
                                                newLine = '';
                                            }
                                            lines += newLine;
                                        }
                                    }
                                }).on('done', function (error) {
                                    if (error) {
                                        console.log('error:', error);
                                        return callback(error);
                                    }

                                    if (!multiple) {
                                        return callback(null, lines);
                                    }

                                    (0, _csvtojson2.default)({ delimiter: ';' }).fromFile(inputFolder2 + fileName2).on('json', function (jsonObj, rowIndex) {
                                        var productCode = getProductCode(jsonObj);
                                        var deleted = false;

                                        if (productCode && productCodes.indexOf(productCode) == -1) {
                                            var newLine = productCode + ';';
                                            newLine += jsonObj.productName.trim() + ';';
                                            newLine += category + ';';
                                            newLine += jsonObj.cloudServiceTypeCode.trim() + ';';
                                            newLine += 'active' + ';';

                                            // add additionalData paramteres
                                            for (i = 0; i < additionalDatas.length; i++) {
                                                newLine += addAdditionalData(jsonObj, additionalDatas, i);
                                            }

                                            // add productSpecCharacteristic parameters
                                            for (i = 0; i < prodSpecs[category].length - 1; i++) {
                                                aux = prodSpecChar(jsonObj.resourceSpecCode.trim(), jsonObj, i);
                                                deleted = aux === 'deleted'; // Do not include removed products
                                                newLine += aux + ';';
                                            }
                                            aux = prodSpecChar(jsonObj.resourceSpecCode.trim(), jsonObj, i);
                                            deleted = aux === 'deleted'; // Do not include removed products
                                            newLine += aux + '\n';

                                            if (!deleted) {
                                                if (jsonObj.Fase && jsonObj.Fase === '2' && !jsonObj['PRICE BRAZIL']) {
                                                    // avoid loading CS4VDC products ofr phase 2 until they would have a price defined
                                                    newLine = '';
                                                }
                                                lines += newLine;
                                            }
                                        }
                                    }).on('done', function (error) {
                                        if (error) {
                                            console.log('error:', error);
                                            return callback(error);
                                        }
                                        return callback(null, lines);
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}

function getObjectFromCSV(fileName, callback) {
    var result = {};
    (0, _csvtojson2.default)({ delimiter: ';' }).fromFile(fileName).on('json', function (jsonObj, rowIndex) {
        var aux = {};
        Object.keys(jsonObj).forEach(function (key) {
            aux[key] = jsonObj[key];
        });
        result[jsonObj.resourceSpecCode] = aux;
    }).on('done', function (error) {
        if (error) {
            console.log('error:', error);
            return callback(error);
        }
        return callback(null, result);
    });
}

// ProductCode should be in the Excel, but just in case, calculate the productCode based on resourceSpecCode following the
// algorithm defined in the specification doc
function getProductCode(jsonObj) {
    var productCode = jsonObj.productCode.trim();
    if (!productCode) {
        productCode = jsonObj.resourceSpecCode.trim();
        switch (productCode) {
            case '0':
            case 'vpn':
            case 'api':
            case 'queue':
                productCode = jsonObj.resourceTypeCode.trim().substring(jsonObj.resourceTypeCode.trim().lastIndexOf('.' + 1)) + '.' + jsonObj.accumulateFactorName.trim();
                break;
            case 'bgp':
            case 'private':
                productCode = jsonObj.cloudServiceTypeCode.trim().substring(jsonObj.cloudServiceTypeCode.trim().lastIndexOf('.' + 1)) + '.' + jsonObj.resourceTypeCode.trim().substring(jsonObj.resourceTypeCode.trim().lastIndexOf('.' + 1)) + '.' + jsonObj.accumulateFactorName.trim();
                break;
            case '21_normal':
                productCode = 'elb.internet';
                break;
            case '21_internal':
                productCode = 'elb.private';
                break;
            case 'vbs':
                productCode += '.' + jsonObj.accumulateFactorName.trim();
                break;
            case 'SATA':
            case 'SSD':
                productCode = jsonObj.resourceTypeCode.trim().substring(jsonObj.resourceTypeCode.trim().lastIndexOf('.' + 1)) + '.' + jsonObj.accumulateFactorName.trim().toLowerCase();
                break;
            case 'smn':
                productCode = jsonObj.cloudServiceTypeCode.trim().substring(jsonObj.cloudServiceTypeCode.trim().lastIndexOf('.' + 1)) + '.' + jsonObj.accumulateFactorName.trim();
                break;
        }
    }
    return productCode;
}

exports.default = function (_ref) {
    var itemsType = _ref.itemsType,
        category = _ref.category,
        inputFolder = _ref.inputFolder,
        inputFile = _ref.inputFile,
        ecsFile = _ref.ecsFile,
        rdsFile = _ref.rdsFile,
        baremetalFile = _ref.baremetalFile,
        mrsFile = _ref.mrsFile,
        natGatewayFile = _ref.natGatewayFile,
        dedicatedHostFile = _ref.dedicatedHostFile,
        workspaceFile = _ref.workspaceFile,
        outputPath = _ref.outputPath;

    if (inputFolder && inputFolder.substring(inputFolder.length - 1) !== '/') {
        inputFolder += '/';
    }
    return csvConversion(itemsType || 'products', category || 'opencloud', inputFolder || './files/input/', inputFile || 'price_catalog.csv', multiple || false, inputFolder2 || './files/input/', inputFile2 || 'price_catalog.csv', ecsFile || 'ecs_details.csv', rdsFile || 'rds_details.csv', baremetalFile || 'baremetal_details.csv', mrsFile || 'mrs_details.csv', natGatewayFile || 'natGateway_details.csv', dedicatedHostFile || 'dedicatedHost_details.csv', workspaceFile || 'workspace_details.csv', outputPath || './files/output/converted.csv');
};