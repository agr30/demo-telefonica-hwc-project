'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _csbPriceServerOrm = require('csb-price-server-orm');

var _q = require('q');

var _q2 = _interopRequireDefault(_q);

var _ = require('../');

var _2 = _interopRequireDefault(_);

var _config = require('../../config');

var _config2 = _interopRequireDefault(_config);

var _httpStatusCodes = require('http-status-codes');

var _httpStatusCodes2 = _interopRequireDefault(_httpStatusCodes);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Currency = _csbPriceServerOrm.models.Currency;

/**
 * Catchs an error, logs and disconnect db.
 * @param {String} error
 */
function catchError(error) {
  error && _2.default.log(`${_2.default.chalk.red('ERROR')} ${error.message}${error.description ? `: ${error.description}` : ''}`);
  _csbPriceServerOrm.db.disconnect();
}

/**
 * Calls openexchangerates api.
 * @param {Object} options Options object with needed options to call the endpoint.
 * @return {Promise} Promise-
 */
function callApi(options) {
  var qs = {
    app_id: _config2.default.currencies.appId
  },
      defer = new _q2.default.defer(),
      url = _config2.default.currencies.server + _config2.default.currencies.entities[options.entity];

  // If entity is historical, add date to url.
  if (options.entity === 'historical') {
    url += `/${options.dateHistorical}.json`;
  }
  (0, _request2.default)({
    url: url,
    qs: qs,
    method: 'GET',
    headers: {
      Accept: 'application/json'
    }
  }, function (err, response, body) {
    if (!err && response.statusCode === _httpStatusCodes2.default.OK) {
      defer.resolve(JSON.parse(body));
    } else {
      defer.reject(err || JSON.parse(body));
    }
  });

  return defer.promise;
}

/**
 * Checks if next date is valid.
 * @param {Moment} date Moment.js date
 * @return {boolean}
 */
function checkNext(date) {
  return date.isBefore((0, _moment2.default)());
}

/**
 * Gets record requested (Mixed recursive).
 * @param {number} date Date in format 'YYYYMMDD'.
 * @param {Object} defer Deferred object.
 * @return {Promise} */
function getRecord(date, defer) {
  var _nextDate = (0, _moment2.default)(date, 'YYYYMMDD').add(1, 'd');

  Currency.findOne({
    _id: date
  }, function (err, record) {
    if (!record) {
      return callApi({
        entity: 'historical',
        dateHistorical: (0, _moment2.default)(date, 'YYYYMMDD').format('YYYY-MM-DD')
      }).then(function (result) {
        return _csbPriceServerOrm.methods.save(Currency, Object.assign({}, result, { _id: date }));
      }).then(function () {
        if (checkNext(_nextDate)) {
          getRecord(parseInt((0, _moment2.default)(_nextDate).format('YYYYMMDD')), defer);
        } else {
          defer.resolve();
        }
      }).catch(function (error) {
        defer.reject(error);
      });
    } else if (checkNext(_nextDate)) {
      getRecord(parseInt((0, _moment2.default)(_nextDate).format('YYYYMMDD')), defer);
    } else {
      defer.resolve();
    }
  });

  return defer.promise;
}

exports.default = function (_ref) {
  var startDate = _ref.startDate,
      _ref$dbUri = _ref.dbUri,
      dbUri = _ref$dbUri === undefined ? _config2.default.dbUri : _ref$dbUri;

  return _csbPriceServerOrm.db.connect({ dbUri, dbOptions: _config2.default.dbOptions }, _2.default).then(function () {
    var _start = parseInt(startDate ? (0, _moment2.default)(startDate, 'YYYY-MM-DD').format('YYYYMMDD') : (0, _moment2.default)(_config2.default.currencies.startRecordingDateByDefault, 'YYYYMMDD').format('YYYYMMDD'), 10),
        defer = new _q2.default.defer();

    return getRecord(_start, defer);
  }).then(function () {
    _2.default.log('Synchronized!');
    _csbPriceServerOrm.db.disconnect();
  }).catch(catchError);
};