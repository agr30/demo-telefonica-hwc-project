const fs = require('fs');
const dynamoDBClient = require('./utils/dynamoDb/client');
const config = require('./commons/config');
const logger = require('logops');
const AWS = require('aws-sdk');
const codepipeline = new AWS.CodePipeline();
const main = require('./index');

let jobSuccess = false;

function putJobSuccess(jobId, context, message) {
    if (!jobId) {
        return;
    }
    var params = {
        jobId: jobId
    };
    logger.debug('putJobSuccess: before calling pipeline');
    jobSuccess = true;
    codepipeline.putJobSuccessResult(params, function(err) {
        if(err) {
            logger.debug('putJobSuccess: error: ', err);
            context.fail(err);
        } else {
            logger.debug('putJobSuccess: succeeded');
            context.succeed(message);
        }
    });
}

// Notify AWS CodePipeline of a failed job
function putJobFailure(jobId, context, message) {
    if (!jobId) {
        return;
    }
    var params = {
        jobId: jobId,
        failureDetails: {
            message: JSON.stringify(message),
            type: 'JobFailed',
            externalExecutionId: context.invokeid
        }
    };
    logger.debug('putJobFailure: before calling pipeline');
    codepipeline.putJobFailureResult(params, function(err) {
        logger.debug('putJobFailureResult: terminated with error: ', err);
        context.fail(message);
    });
}


exports.handler = async function handler(event, context, callback) {
    // Retrieve the Job ID from the Lambda action
    let jobId = (event["CodePipeline.job"]) ? event["CodePipeline.job"].id : 0;
    // Notify AWS CodePipeline of a successful job

    try {
        let dbTable = process.env.DB_TABLE || 'hwc-dev-HK-huaweiDataTable-17BW451I8D5B3';
        let params = {
            KeyConditionExpression: '#stage = :stage',
            ExpressionAttributeNames: {
                '#stage': 'stage'
            },
            ExpressionAttributeValues: {
                ':stage': 'Prod'
            }
        };
        let items = await dynamoDBClient.query(config.AWS_ACCOUNT_CONFIG, dbTable, params);
        if (items.length > 0) {
            let errorMsg =
                'Table is not empty. Please remove the current items' +
                ' in it manually before executing this lambda, to avoid overwriting value which may lead to a double charge in CSB';
            putJobSuccess(jobId, context, 'Table is not empty, DynamoDB not updated');  // Succeed the job (for pipeline)
            throw new Error(errorMsg);
        }
        // Read config file
        let configObject = JSON.parse(fs.readFileSync('config.json', 'utf8'));
        let envConfig = configObject.common; // Common config params
        let currentEnv = process.env.CURRENT_ENV;
        let ob = currentEnv.split('-')[1].toUpperCase(); // add OB parameter from env parameter
        // Read specific section of config for this OB
        Object.keys(configObject[ob]).forEach(key => {
            envConfig[key] = configObject[ob][key];
        });
        envConfig.obHuawei.secretName = process.env.PREFIX + currentEnv.toLowerCase();
        // Add env config parameters
        envConfig.env = currentEnv; // add env parameter to config
        envConfig.ob = ob;
        envConfig.temporalBucket = envConfig.temporalBucket + currentEnv.toLowerCase();
        envConfig.finalBucket = process.env.PREFIX + currentEnv.toLowerCase(); // Destiny bucket, change name?

        await dynamoDBClient.put(config.AWS_ACCOUNT_CONFIG, dbTable, envConfig);

        logger.info('Dataloader executed successfully');
        putJobSuccess(jobId, context, 'Config successfully stored in DynamoBB.');  // Succeed the job (for pipeline)
        return callback(null, main.generateResponse(200, {message: 'Config successfully stored in DynamoBB'}));
    } catch (err) {
        logger.error('Error in catch: ', err);
        if (!jobSuccess) {
            putJobFailure(jobId, context, err);  // Fail the job (for pipeline)
        }
        logger.debug('Calling callback with err');
        return callback(null, main.generateResponse(500, {'error': 'Internal server error',
            'message': 'An internal error ocurred'}));
    }
};
