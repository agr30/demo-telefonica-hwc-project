'use strict';

const appProvision = require('./AppProvision');
const billing = require('./lib/billing');
const config = require('./commons/config');
const contextProvision = require('./ContextProvision');
const dynamoClient = require('./utils/dynamoDb/client');
const Errors = require('./commons/errors');
const importing = require('./utils/hwc/importing');
const logger = require('logops');
const resourceClient = require('./services/resourceAPIClient.js');
const s3Client = require('./utils/s3/client');
const utils = require('./commons/utils');

// appCreation lambda entry point
exports.appCreation = function appCreation(event, context, callback) {
    logger.debug('index.appCreation begins');
    logger.debug('index.createApp begins');
    appProvision.appCreation(event, context, function(err, response) {
        if (err) {
            logger.error('Error creating app: ', err);
            return callback(null, err);
        } else {
            response = generateResponse(200, {});
            logger.debug('App created correctly: ', response);
            return callback(null, response);
        }
    });
};

// appUpdate lambda entry point
exports.appUpdate = function appUpdate(event, context, callback) {
    logger.debug('index.appUpdate begins');
    appProvision.appUpdate(event, context, function(err, response) {
        if (err) {
            logger.error('Error updating app: ', err);
            return callback(null, err);
        } else {
            response = generateResponse(200, {});
            logger.debug('App updated correctly: ', response);
            return callback(null, response);
        }
    });
};

// appRemoval lambda entry point
exports.appRemoval = function appRemoval(event, context, callback) {
    logger.debug('index.appUpdate begins');
    appProvision.appRemoval(event, context, function(err, response) {
        if (err) {
            logger.error('Error removing app: ', err);
            return callback(err);
        } else {
            response = generateResponse(200, {});
            logger.debug('App removed correctly: ', response);
            return callback(null, response);
        }
    });
};

// createContext lambda entry point
/** Function to create a customer
 */
exports.createContext = async function createContext(event, context, callback) {
    logger.info('index.createContext begins');
    if (!process.env.migrationMode) {
        callback(null, {});
    }
    logger.info('index.createContext mode migration begins');
    const stage = (event.requestContext) ? event.requestContext.stage || 'Prod' : 'Prod';
    logger.info('Event: ', event);
    logger.info('Stage: ', stage);
    await config.loadConfiguration(stage);

    let body = JSON.parse(event.body);
    let domainName = body.domainName;
    let accountId = body.accountId;
    let subscriptionId = body.subscriptionId;
    let dbTable = process.env.DB_MIGRATION || 'huawei-migration';
    if (!domainName) {
        throw new Errors.BadParameterError('Error in domainName param', 'Missing parameter');
    }
    if (!accountId) {
        throw new Errors.BadParameterError('Error in accountId param', 'Missing parameter');
    }
    if (!subscriptionId) {
        throw new Errors.BadParameterError('Error in subscriptionId param', 'Missing parameter');
    }
    let key = {
        domainName: domainName.toString()
    };
    let user = await dynamoClient.get(config.AWS_ACCOUNT_CONFIG, dbTable, key);
    logger.debug('ContextMigration with user : ', user);
    if (!user || !user.domainId) {
        throw new Errors.BadParameterError('Error in user', 'Missing user in table');
    }
    body.domainId = user.domainId;
    body.xAccountId = accountId + '_' + subscriptionId;
    if (body.priceFactor === undefined) {
        body.priceFactor = 0;
    }

    await contextProvision.contextMigrationUpdate(body);
    if (!process.env.fake_aps) {
        var modifyContextData = body;
        logger.debug('Updating context', modifyContextData);
        // Will update domainId and domainName in context
        await new Promise((resolve) => {
            resourceClient.modify(config, modifyContextData, function(err, updateData) {
                if (err) {
                    logger.error('CODE-61-0105 Error modifying context data', modifyContextData, err);
                } else {
                    logger.debug('Context %s updated successfully', modifyContextData.aps.id);
                }
                logger.debug('Context update ', user);
                resolve();
            });
        });
    }
    return callback(null, generateResponse(200, {}));
};

exports.updateContext = async function updateContext(event, context, callback) {
    try {
        logger.info('index.updateContext begins');
        const stage = (event.requestContext) ? event.requestContext.stage || 'Prod' : 'Prod';
        logger.info('Event: ', event);
        logger.info('Stage: ', stage);
        await config.loadConfiguration(stage);

        let body = JSON.parse(event.body);
        let domainId = body.domainId;
        let dbTable = process.env.DB_USERS || 'huawei-users';
        let user;
        // we check in dynamo if the customer has a domainid, if yes we only update the information ( price factor and/or email) and if email we send him a message
        if (domainId) {
            let key = {
                customerId: domainId.toString()
            };
            user = await dynamoClient.get(config.AWS_ACCOUNT_CONFIG, dbTable, key);
        }
        if (user && user.domainName) {
            logger.debug('ContextUpdate in Dyanamo init', user);
            await contextProvision.contextUpdate(body, user);
            return callback(null, generateResponse(200, {}));
        } else {
            logger.debug('Context Update/Creation in Huawei init');
            // if not we create the customer in huawei
            // create the customer
            let searchOptions = {
                Type: 'name',
                Key: body.domainName
            };
            if (await contextProvision.contextExist(searchOptions)) {
                return callback(null, generateResponse(409, 'The name you are choosing already exists, please choose another.'));
            }
            let created = await contextProvision.contextCreation(body);

            if (created && created.err) {
                logger.error('Error creating customer: ', created.err);
                // return callback(err);
                return callback(null, generateResponse(500, created.err));
            } else {
                if (!process.env.fake_aps) {
                    var modifyContextData = body;
                    modifyContextData.domainId = created.domainId;
                    if (modifyContextData.priceFactor === undefined) {
                        modifyContextData.priceFactor = 0;
                    }
                    logger.debug('Updating context', modifyContextData);
                    callback(null, generateResponse(200, {}));
                    // Will update domainId and domainName in context
                    await new Promise((resolve) => {
                        resourceClient.modify(config, modifyContextData, function(err, updateData) {
                            if (err) {
                                logger.error('CODE-61-0105 Error modifying context data', modifyContextData, err);
                            } else {
                                logger.debug('Context %s updated successfully', modifyContextData.aps.id);
                            }
                            logger.debug('Context created ', created);
                            resolve();
                        });
                    });
                    return;
                }
                return callback(null, generateResponse(200, {}));
            }
        }
    } catch (err) {
        logger.error(err);
        return callback(null, generateResponse(500, { error: 'Internal error', message: 'An internal error has occurred during execution' }));
    }
};

// enableContext lambda entry point
/** Function to enable a customer who was in suspended state,
 * we check his status, then we reset his limit and update his status in dynamo
 */
exports.enableContext = async function enableContext(event, context, callback) {
    try {
        logger.debug('index.enableContext begins');
        const stage = (event.requestContext) ? event.requestContext.stage || 'Prod' : 'Prod';
        logger.info('Event: ', event);
        logger.info('Stage: ', stage);
        await config.loadConfiguration(stage);
        let dbTable = process.env.DB_USERS || 'huawei-users';
        let contextId = event.pathParameters.id;
        let domainId;
        let user;
        let contextData = await new Promise((resolve, reject) => {
            resourceClient.get(config, contextId, (err, contextData) => {
                if (err) {
                    logger.error('CODE-61-0112 Error obtaining context data', err);
                    reject(new Error(err));
                } else {
                    resolve(contextData);
                }
            });
        });
        logger.info('context data', contextData);
        domainId = contextData.domainId;
        logger.info('domainId from context data', domainId);

        if (!domainId) {
            return callback(null, generateResponse(200, {}));
        }
        let key = {
            customerId: domainId.toString()
        };
        user = await dynamoClient.get(config.AWS_ACCOUNT_CONFIG, dbTable, key);
        logger.debug('User :', user);

        if (!user) {
            return callback(null, generateResponse(500, { error: 'Error', message: 'Error enabling context, user does not exist ' }));
        };
        if (user && user.accountStatus === 'Suspended') {
            logger.debug('UserStatus is  :', user.accountStatus);
            if (await contextProvision.setCustomerLimit(domainId, config.obHuawei.creditLimit)) {
                let updateContext = {
                    key: key,
                    expression: 'set accountStatus = :status ',
                    attributes: {
                        ':status': 'Active'
                    }
                };
                logger.debug('SET the customer Limit to  :', config.obHuawei.creditLimit);
                await dynamoClient.update(config.AWS_ACCOUNT_CONFIG, dbTable, updateContext);
                logger.debug('Update user in dynamo:');
                return callback(null, generateResponse(200, {}));
            } else {
                logger.debug('NOT set the customer limit');
                return callback(null, generateResponse(500, { error: 'Error', message: 'Error enabling context ' }));
            }
        } else {
            logger.debug('NOT CUSTOMER');
            return callback(null, generateResponse(500, { error: 'Error', message: 'Error enabling context, user was not suspended ' }));
        }
    } catch (err) {
        logger.error(err);
        return callback(null, generateResponse(500, { error: 'Internal error', message: 'An internal error has occurred during execution' }));
    }
};

// disableContext lambda entry point
/** Function to disable a customer who was in active state,
 * we check his status, then we set his limit to 0 and update his state in dynamo
 */
exports.disableContext = async function disableContext(event, context, callback) {
    try {
        logger.debug('index.disableContext begins');
        const stage = (event.requestContext) ? event.requestContext.stage || 'Prod' : 'Prod';
        logger.info('Event: ', event);
        logger.info('Stage: ', stage);
        await config.loadConfiguration(stage);
        let dbTable = process.env.DB_USERS || 'huawei-users';
        let contextId = event.pathParameters.id;
        let domainId;
        let user;
        let contextData = await new Promise((resolve, reject) => {
            resourceClient.get(config, contextId, (err, contextData) => {
                if (err) {
                    logger.error('CODE-61-0112 Error obtaining context data', err);
                    reject(new Error(err));
                } else {
                    resolve(contextData);
                }
            });
        });

        logger.info('context data', contextData);
        domainId = contextData.domainId;
        logger.info('domainId from context data', domainId);

        if (!domainId) {
            return callback(null, generateResponse(200, {}));
        }
        let key = {
            customerId: domainId.toString()
        };
        user = await dynamoClient.get(config.AWS_ACCOUNT_CONFIG, dbTable, key);
        logger.debug('User :', user);

        if (!user) {
            return callback(null, generateResponse(500, { error: 'Error', message: 'Error disabling context, user does not exist ' }));
        };
        if (user && user.accountStatus === 'Active') {
            logger.debug('UserStatus :', user.accountStatus);
            if (await contextProvision.setCustomerLimit(domainId, 0)) {
                let updateContext = {
                    key: key,
                    expression: 'set accountStatus = :status ',
                    attributes: {
                        ':status': 'Suspended'
                    }
                };
                logger.debug('SET the customer limit to :', 0);
                await dynamoClient.update(config.AWS_ACCOUNT_CONFIG, dbTable, updateContext);
                logger.debug('Update user in dynamo:');
                return callback(null, generateResponse(200, {}));
            } else {
                logger.debug('NOT set the limit to 0 :');
                return callback(null, generateResponse(500, { error: 'Error', message: 'Error disabling context ' }));
            }
        } else {
            logger.debug('NOT CUSTOMER');
            return callback(null, generateResponse(500, { error: 'Error', message: 'Error enabling context, user was not active ' }));
        }
    } catch (err) {
        logger.error(err);
        return callback(null, generateResponse(500, { error: 'Internal error', message: 'An internal error has occurred during execution' }));
    }
};

// removeContext lambda entry point
/** Function to remove a customer
 * we check the status of the customer to set it to suspended if needed
 * and set his limit to 0
 * anyway we always send him an email to follow the instructions
 */
exports.removeContext = async function removeContext(event, context, callback) {
    try {
        // SEND AN EMAIL TO THE USER
        logger.debug('index.removingContext begins');
        const stage = (event.requestContext) ? event.requestContext.stage || 'Prod' : 'Prod';
        logger.info('Event: ', event);
        logger.info('Stage: ', stage);
        await config.loadConfiguration(stage);
        let dbTable = process.env.DB_USERS || 'huawei-users';
        let contextId = event.pathParameters.id;
        let domainId;
        let user;
        let email;
        let contextData = await new Promise((resolve, reject) => {
            resourceClient.get(config, contextId, (err, contextData) => {
                if (err) {
                    logger.error('CODE-61-0112 Error obtaining context data', err);
                    reject(new Error(err));
                } else {
                    resolve(contextData);
                }
            });
        });

        logger.info('context data', contextData);
        domainId = contextData.domainId;
        email = contextData.email;
        let domainName = contextData.domainName;
        logger.info('domainId from context data', domainId);

        if (!domainId) {
            return callback(null, generateResponse(200, {}));
        }
        if (!email) {
            return callback(null, generateResponse(200, {}));
        }

        let key = {
            customerId: domainId.toString()
        };
        user = await dynamoClient.get(config.AWS_ACCOUNT_CONFIG, dbTable, key);

        if (user && user.accountStatus === 'Active') {
            let updateContext = {
                key: key,
                expression: 'set accountStatus = :status ',
                attributes: {
                    ':status': 'Suspended'
                }
            };
            await dynamoClient.update(config.AWS_ACCOUNT_CONFIG, dbTable, updateContext);
        }
        await contextProvision.setCustomerLimit(domainId, 0);
        let params = {
            email: process.env.operatorEmail,
            domainName: domainName,
            template: 'deletion',
            mailFolder: 'global'
        };
        await contextProvision.sendEmail(params);
        return callback(null, generateResponse(200, {}));

        // logger.debug('Context removed');
    } catch (err) {
        logger.error(err);
        return callback(null, generateResponse(500, { error: 'Internal error', message: 'An internal error has occurred during execution' }));
    };
};

// setPriceFactor lambda entry point
/** Function to set a price factor
 */
exports.setPriceFactor = async function setPriceFactor(event, context, callback) {
    try {
        logger.debug('index.setPriceFactor begins');
        const stage = (event.requestContext) ? event.requestContext.stage || 'Prod' : 'Prod';
        logger.info('Event: ', event);
        logger.info('Stage: ', stage);
        await config.loadConfiguration(stage);

        let dbTable = process.env.DB_USERS || 'huawei-users';
        let contextId = event.pathParameters.id;
        let body = JSON.parse(event.body);
        let priceFactor = body.priceFactor;
        if (priceFactor === undefined) {
            throw new Errors.BadParameterError('Error in price factor param', 'Missing parameter');
        }

        let contextData = await new Promise((resolve, reject) => {
            resourceClient.get(config, contextId, (err, contextData) => {
                if (err) {
                    logger.error('CODE-61-0112 Error obtaining context data', err);
                    reject(new Error(err));
                } else {
                    resolve(contextData);
                }
            });
        });

        logger.info('context data', contextData);
        domainId = contextData.domainId;
        logger.info('domainId from context data', domainId);

        if (!domainId) {
            return callback(null, generateResponse(200, {}));
        }
        let key = {
            customerId: domainId.toString()
        };
        user = await dynamoClient.get(config.AWS_ACCOUNT_CONFIG, dbTable, key);
        if (!user) {
            return callback(null, generateResponse(500, { error: 'Error', message: 'Error setting price factor, user does not exist ' }));
        };
        if (user) {
            let updateContext = {
                key: key,
                expression: 'set priceFactor = :priceFactor ',
                attributes: {
                    ':priceFactor': priceFactor
                }
            };
            await dynamoClient.update(config.AWS_ACCOUNT_CONFIG, dbTable, updateContext);
        }

        if (!process.env.fake_aps) {
            var modifyContextData = body;
            modifyContextData.priceFactor = priceFactor;

            logger.debug('Updating context', modifyContextData);
            // Will update the priceFactor in context
            await new Promise((resolve, reject) => {
                resourceClient.modify(config, modifyContextData, function(err, updateData) {
                    if (err) {
                        logger.error('CODE-61-0105 Error modifying context data', modifyContextData, err);
                        reject();
                    } else {
                        logger.debug('Context %s updated successfully', modifyContextData.aps.id);
                    }
                    logger.debug('Context created ', created);
                    resolve();
                });
            });
        }
        return callback(null, generateResponse(200, {}));
    } catch (err) {
        logger.error(err);
        return callback(null, generateResponse(500, { error: 'Internal error', message: 'An internal error has occurred during execution' }));
    };
};

// appUpgrade lambda entry point
exports.appUpgrade = function appUpgrade(event, context, callback) {
    logger.debug('index.appUpgrade begins');
    logger.info('index.appUpgrade App Headers: ', JSON.stringify(event.headers));
    logger.info('index.appUpgrade Body: ', JSON.stringify(event.body));
    logger.info('index.appUpgrade Path Parameters: ', JSON.stringify(event.pathParameters));

    let response = generateResponse(204, {});
    logger.info('index.appUpgrade ends. response : ' + JSON.stringify(response));
    return callback(null, response);
};

// downloadFile lambda entry point
exports.downloadFile = function downloadFile(event, context, callback) {
    logger.debug('index.downloadFile begins');
    contextProvision.downloadFile(event, context, function(err, response) {
        if (err) {
            return callback(null, generateResponse(500, { error: err, message: 'Error download App' }));
        } else {
            return callback(null, generateResponse(200, { error: err, message: 'File downloaded correctly' }));
        }
    });
};

// listBillingFiles lambda entry point
exports.listBillingFiles = async function listBillingFiles(event, context, callback) {
    try {
        logger.debug('index.listBillingFiles begins');
        const stage = event.requestContext ? event.requestContext.stage || 'Prod' : 'Prod';
        logger.info('Event: ', event);
        logger.info('Stage: ', stage);
        logger.debug('Config before: ', config);
        await config.loadConfiguration(stage);
        logger.debug('Config after: ', config);

        let contextId = event.pathParameters.id;
        if (!contextId) {
            return callback(null, generateResponse(400, { error: 'Error', message: 'Missing contextId ' }));
        }

        let response = await new Promise((resolve, reject) => {
            contextProvision.listBillingFiles(contextId, config, function(err, response) {
                if (err) {
                    let errorMessage = 'Error index.listBillingFiles from context' + contextId + ' Error: ' + err;
                    reject(new Error(errorMessage));
                } else {
                    logger.info('index.listBillingFiles obtained response %s ', response);
                    resolve(response);
                }
            });
        });
        return callback(null, generateResponse(200, response));
    } catch (err) {
        logger.error(err);
        return callback(null, generateResponse(500, {
            error: 'Internal error',
            message: 'An internal error has occurred during execution'
        }));
    }
};

// retrieve() the value of the counter amount
// Get the total amount from csv. Need organizationId and realmName ¿?
exports.retrieve = async function retrieve(event, context, callback) {
    try {
        logger.debug('index.retrieve begins');
        const stage = (event.requestContext) ? event.requestContext.stage || 'Prod' : 'Prod';
        logger.info('Event: ', event);
        logger.info('Stage: ', stage);
        await config.loadConfiguration(stage);

        let contextId = event.pathParameters.id;
        if (!contextId) {
            return callback(null, generateResponse(400, { error: 'error', message: 'Missing contextId' }));
        }

        // Get charge of cvs_summary for domainId
        let response = await new Promise((resolve, reject) => {
            contextProvision.getTotalAmountForContextId(event, config, context, contextId, function(err, response) {
                if (err) {
                    let errorMessage = 'Error retrieve from context ' + contextId + ' Error: ' + err;
                    reject(new Error(errorMessage));
                } else {
                    logger.info('Retrieve obtained response %s ', response);
                    resolve(response);
                }
            });
        });
        return callback(null, generateResponse(200, response));
    } catch (err) {
        logger.error(err);
        return callback(null, generateResponse(500, {
            error: 'Internal error',
            message: 'An internal error has occurred during execution'
        }));
    }
};

function generateResponse(statusCode, body) {
    // The output from a Lambda proxy integration must be
    // of the following JSON object. The 'headers' property
    // is for custom response headers in addition to standard
    // ones. The 'body' property  must be a JSON string. For
    // base64-encoded payload, you must also set the 'isBase64Encoded'
    // property to 'true'.
    return {
        statusCode: statusCode,
        body: JSON.stringify(body)
    };
}
exports.generateResponse = generateResponse;

// generateBilling lambda entry point, generates billing files of last finished month.
exports.generateBillingInfo = async function generateBilling(event, context, callback) {
    try {
        logger.info('Generate Billing');
        logger.debug('Event: ', event);
        let stage = event.requestContext ? event.requestContext.stage || 'Prod' : 'Prod';
        logger.info('Stage: ', stage);
        logger.info('Config: ', config);
        await config.loadConfiguration(stage); // Load configuration of dynamoDB.
        let forceBilling = false;
        let billingOffset = null;

        if (event && event.queryStringParameters) {
            let queryParameters = event.queryStringParameters;
            if (queryParameters.exchangeRate) {
                if (isNaN(queryParameters.exchangeRate) || queryParameters.exchangeRate < 0) {
                    throw new Errors.BadParameterError('Error in exchangeRate param', 'It should be a number and not lower than 0');
                }
                logger.info('Using exchangeRate provided: ', queryParameters.exchangeRate);
                config.exchangeRate = queryParameters.exchangeRate; // Overwrite exchangeRate with the one provided in the request
            }

            if (queryParameters.force) {
                if (queryParameters.force !== 'true' && queryParameters.force !== 'false') {
                    throw new Errors.BadParameterError('Error in force param', 'It should be boolean');
                }
                if (queryParameters.force === 'true') {
                    logger.info('Force billing');
                    forceBilling = true;
                }
            }
            if (queryParameters.offset) {
                if (isNaN(queryParameters.offset) || parseInt(queryParameters.offset) <= 0) {
                    throw new Errors.BadParameterError('Error in offset param', 'It should be a number and greater than 0');
                }
                logger.info('Offset:', queryParameters.offset);
                billingOffset = parseInt(queryParameters.offset);
            }
        }
        let billingPeriod = await utils.getBillingPeriod(billingOffset);
        if (forceBilling !== true && config.lastHWCInvoiceDate !== 'none' && billingPeriod <= config.lastHWCInvoiceDate) {
            logger.info('Billing not being called, as there are no new billing periods data');
            return callback(
                null,
                generateResponse(200, { message: 'Billing not being called, as there are no new billing periods data' })
            );
        }
        // We delete all the files before importing them, preventive case
        await s3Client.deleteAllObject(config.AWS_ACCOUNT_CONFIG, config.temporalBucket);
        logger.info('Importing Files from Huawei Cloud Bucket');
        // To transfer the file from huawei cloud to aws cloud, if we have no file to transfer we send the response empty.
        if (!(await importing.importsFile(billingPeriod))) {
            logger.info('Billing not being called, as there are no new billing in huawei cloud');
            return callback(
                null,
                generateResponse(200, { message: 'Billing not being called, as there are no new billing in huawei cloud' })
            );
        }
        await billing.generateBilling(billingPeriod);
        config.lastHWCInvoiceDate = billingPeriod;
        logger.debug('Current config: ', config);
        await config.updateConfiguration(stage);
        // When finish our billing, we delete all the files inside the bucket
        await s3Client.deleteAllObject(config.AWS_ACCOUNT_CONFIG, config.temporalBucket);
        return callback(
            null,
            generateResponse(200, { message: 'Billing generated correctly' })
        );
    } catch (err) {
        if (err instanceof Errors.BadParameterError) {
            return callback(
                null,
                generateResponse(400, { error: err.error, message: err.message })
            );
        } else if (err instanceof Errors.NotFoundError) {
            return callback(
                null,
                generateResponse(404, { error: err.error, message: err.message })
            );
        } else {
            logger.error(err);
            return callback(null, generateResponse(500, { error: 'Internal error', message: 'An internal error has occurred during execution' }));
        }
    }
};
