'use strict';

var errors = {
    INVALID_PAYLOAD: {
        exceptionId: 'INVALID_PAYLOAD',
        exceptionMessage: 'Some of the parameters were missing or incorrect. Please check it and try again',
        code: 400
    },
    APSC_CONNECTION_ERROR: {
        exceptionId: 'APSC_CONNECTION_ERROR',
        exceptionMessage: 'There was an error connecting to the ASPC Controller. Please contact your administrator',
        code: 500
    },
    APS_USER_NOT_FOUND: {
        exceptionId: 'APS_USER_NOT_FOUND',
        exceptionMessage: 'The licence was created with a user ID that was not found in the APS Controller (or was ' +
            'deleted before the APS Endpoint was able to check it)',
        code: 404
    },
    ACENS_CONNECTION_ERROR: {
        exceptionId: 'ACENS_CONNECTION_ERROR',
        exceptionMessage: 'There was an error connecting to the Huawei endpoint. Please contact your administrator',
        code: 500
    },
    BODY_NOT_FOUND: {
        exceptionId: 'BODY_NOT_FOUND',
        exceptionMessage: 'Internal error: body not found for response.',
        code: 500
    },
    USER_DATA_MISSING: {
        exceptionId: 'USER_DATA_MISSING',
        exceptionMessage: 'The service user is missing some of the mandatory fields. Please, fill in the user details.',
        code: 400
    },
    EVENT_ALREADY_CAPTURED: {
        exceptionId: 'EVENT_ALREADY_CAPTURED',
        exceptionMessage: 'The selected event is already captured for that resource and the same provided action.',
        code: 400
    },
    ACENS_APPLICATION_ERROR: {
        exceptionId: 'ACENS_APPLICATION_ERROR',
        exceptionMessage: 'The request to McAfee failed due to an application error',
        code: 500
    },
    ELEMENT_NOT_FOUND: {
        exceptionId: 'ELEMENT_NOT_FOUND',
        exceptionMessage: 'The operation could not be completed because the requested element was not found',
        code: 404
    },
    APS_SUBSCRIPTION_QUERY_ERROR: {
        exceptionId: 'APS_SUBSCRIPTION_QUERY_ERROR',
        exceptionMessage: 'Couldn\'t retrieve the subscriptions for the selected resource.',
        code: 404
    },
    APS_SUBSCRIPTION_CREATION_ERROR: {
        exceptionId: 'APS_SUBSCRIPTION_CREATION_ERROR',
        exceptionMessage: 'Couldn\'t create a subscription for the selected resource.',
        code: 404
    },
    RPC_RESPONSE_ERROR: {
        exceptionId: 'RPC_RESPONSE_ERROR',
        exceptionMessage: 'There was an error in the response from RPC.',
        code: 500
    }
};

var events = {
    LICENCE_CREATION_START: 'LICENCE_CREATION_START',
    LICENCE_CREATION_END: 'LICENCE_CREATION_END',
    LICENCE_REMOVAL_START: 'LICENCE_REMOVAL_START',
    LICENCE_REMOVAL_END: 'LICENCE_REMOVAL_END'
};

exports.errors = errors;
exports.events = events;
