const logger = require('logops');
const _ = require('lodash');
const utils = require('../commons/utils');
const Stream = require('stream');
const config = require('../commons/config');
const S3Client = require('../utils/s3/client');

const ACCOUNT_ID_HEADER = 'Customer ID';

const REPORT_HEADERS = [
    'Customer ID',
    'Product ID',
    'Product type code',
    'Resource type code',
    'Cloud service region code',
    'Billing mode',
    'Official website price',
    'Fee generation time',
    'Order ID',
    'Usage',
    'Usage Unit',
    'Whether a spot ECS',
    'Cash coupon amount',
    'Flexi-purchase coupon amount',
    'Stored-value card amount',
    'Settlement product type',
    'Partner preference ratio',
    'Currency',
    'Billing cycle',
    'Customer account',
    'Customer name',
    'Product name',
    'Product type name',
    'Resource type name',
    'Cloud service region name'
];

/**
 * Read included files in a report between two dates throught an unique stream in order asynchronusly
 * @param {string[]} headers
 * @param {object} reportQuery
 * @returns {Stream} Stream
 */
function getReportStream(headers = [], reportQuery = {}) {
    logger.debug('Get report stream');
    const reportStream = new Stream.PassThrough({ objectMode: true });
    // Read the reports asyncronously allowing us return the report stream
    readReportsThroughtStream();
    return reportStream;
    async function readReportsThroughtStream() {
        try {
            // Read the reports included in periods between startDate and endDate
            // Read all reports of current period in order and send their content throught reportStream
            let listObject = await S3Client.listObject(config.AWS_ACCOUNT_CONFIG, config.temporalBucket);

            for (const file of listObject) {
                let queryStream = await S3Client.selectObjectStream(config.AWS_ACCOUNT_CONFIG, config.temporalBucket, file.Key, reportQuery, {
                    headers: headers
                });
                // Send queryStream content throught reportStream
                // Need pipe with option { end: false } because without it, when the first queryStrem finish
                // reportStream finish also
                queryStream.pipe(reportStream, { end: false });
                // Wait until queryStream is fully proccessed
                await new Promise((resolve, reject) => {
                    queryStream.on('error', reject);
                    queryStream.on('end', resolve);
                });
            }
            endReportStream(reportStream);
        } catch (err) {
            reportStream.emit('error', err);
        }
    }
}

/**
 * Read a complete report stream and return it as JSON Array
 * @async
 * @param {Date} startDate
 * @param {Date} endDate
 * @param {string[]} headers
 * @param {object} reportQuery
 * @returns {JSON[]} Report
 */
async function getReport(headers = [], reportQuery = {}) {
    let reportStream = getReportStream(headers, reportQuery);
    let report = await getReportFromStream(reportStream);
    return report;
}

async function getReportFromStream(reportStream) {
    logger.debug('Get report from stream');
    let report = [];
    reportStream.on('data', function(reportRecord) {
        report.push(reportRecord);
    });
    // Wait until stream is fully proccessed
    // We need listen error event into the promise to allow handle it
    await new Promise((resolve, reject) => {
        reportStream.on('error', reject);
        reportStream.on('end', resolve);
    });
    logger.debug('Complete report readed');
    return report;
}

/**
 * Get the report stream for an account for a month
 * @param {string} accountId
 * @returns {Readable} Report stream
 */
function getAccountMonthlyReportStream(accountId) {
    let reportQuery = {
        'Customer ID': accountId
    };
    const reportStream = getReportStream(REPORT_HEADERS, reportQuery);
    return reportStream;
};

exports.getAccountMonthlyReportStream = getAccountMonthlyReportStream;

/**
 * Get the report for an account for a month
 * @async
 * @param {string} accountId
 * @param {number} month
 * @param {number} year
 * @returns {JSON[]} Report
 */
async function getAccountMonthlyReport(accountId, month, year) {
    let accountReportStream = getAccountMonthlyReportStream(accountId, month, year);
    let accountReport = await getReportFromStream(accountReportStream);
    return accountReport;
};

exports.getAccountMonthlyReport = getAccountMonthlyReport;

/**
 * Get an array with the distinct accounts in a monthly report
 * @async
 * @param {number} month
 * @param {number} year
 * @returns {string[]} Accounts
 */
async function getMonthlyReportAccounts() {
    logger.debug('getMonthlyReportAccounts');
    const rawAccountIds = await getReport([ACCOUNT_ID_HEADER]);
    let accountIds = _.compact(_.uniq(_.map(rawAccountIds, ACCOUNT_ID_HEADER)));
    return accountIds;
}

exports.getMonthlyReportAccounts = getMonthlyReportAccounts;

/**
 * Get the path inside AWS reports bucket where are stored the reports for a given month and year
 * @param {number} month
 * @param {number} year
 */
function getReportsPeriodPath(month, year) {
    logger.debug('getReportsPeriodPath');
    if (!month && !year) {
        let currentDate = new Date();
        month = currentDate.getMonth();
        year = currentDate.getFullYear();
    } else if (month && !year) {
        throw new Error('Invalid month without year');
    } else if (year && !month) {
        throw new Error('Invalid year without month');
    }
    let periodString = utils.getPeriodString(month, year);
    let reportsPeriodPath = config.obHuawei.reportPrefix + '/' + config.obHuawei.reportName + '/' + periodString;
    logger.debug('Period path: ' + reportsPeriodPath);
    return reportsPeriodPath;
}

exports.getReportsPeriodPath = getReportsPeriodPath;

/**
 * Check if exists reports for a month and a year
 * @param {number} month
 * @param {number} year
 * @returns {boolean} Result
 */
async function existsReports(month, year) {
    logger.debug('existsReportsPeriod');
    // Check in the temporal bucket to see if we have the csv to process
    let result = await S3Client.listObject(config.AWS_ACCOUNT_CONFIG, config.temporalBucket);
    return result.length > 0;
}

exports.existsReports = existsReports;

function endReportStream(reportStream) {
    reportStream.on('newListener', (event, listener) => {
        if (event === 'end' || event === 'finish') {
            listener();
        }
    });
    reportStream.push(null);
}
