const reports = require('./reports');
const users = require('./users');
const config = require('../commons/config');
const utils = require('../commons/utils');
const AccountInvoiceFile = require('../billingFiles/accountInvoiceFile');
const AccountDetailedInvoiceFile = require('../billingFiles/accountDetailedInvoiceFile');
const SummaryFile = require('../billingFiles/summaryFile');
const logger = require('logops');

const MAX_SIMULTANEOUS_ACCOUNTS = 100;

/**
 * Generate billing files for all accounts for a given month and year
 * @async
 * @param {number} month
 * @param {number} year
 */
async function generateBilling(date) {
    logger.debug('generateBilling - Date', date);
    let billingDate = getBillingDate(date);
    let billingSummaryFile = await SummaryFile.getFile(billingDate);
    let accountIds = await reports.getMonthlyReportAccounts();
    for (let processedAccounts = 0; processedAccounts < accountIds.length; processedAccounts += MAX_SIMULTANEOUS_ACCOUNTS) {
        let batchAccountIds = accountIds.slice(processedAccounts, processedAccounts + MAX_SIMULTANEOUS_ACCOUNTS);
        let accountsBillingProccessPromises = batchAccountIds.map(accountId => generateAccountBilling(accountId, date));
        let accountBillingSummaryRecords = await Promise.all(accountsBillingProccessPromises);
        billingSummaryFile.addAllRecords(accountBillingSummaryRecords);
    }
    await billingSummaryFile.send(); // Ends the stream
}

exports.generateBilling = generateBilling;

/**
 * Generate billing files for an account for a given month and year
 * @async
 * @param {string} accountId
 * @param {string} billDate
 */
async function generateAccountBilling(accountId, billDate) {
    logger.debug('Generate account billing:', accountId, 'Date', billDate);
    let billingDate = getBillingDate(billDate);
    let userConfig = await users.getUserConfig(accountId);
    let accountInvoiceFileStream = await AccountInvoiceFile.getFileStream(userConfig, billingDate);
    let accountDetailedInvoiceFileStream = await AccountDetailedInvoiceFile.getFileStream(userConfig, billingDate);
    let totalCost = 0;
    let totalVendorCost = 0;
    let accountReportStream = reports.getAccountMonthlyReportStream(accountId);
    let domain = '';
    let date = '';
    // Pipe content from report stream to invoice files
    accountReportStream.pipe(accountInvoiceFileStream);
    accountReportStream.pipe(accountDetailedInvoiceFileStream);
    await Promise.all([
        utils.promesifyStream(accountReportStream, { onData: processReportRecord }),
        utils.promesifyStream(accountInvoiceFileStream, { endEvent: 'uploaded' }),
        utils.promesifyStream(accountDetailedInvoiceFileStream, { endEvent: 'uploaded' })
    ]);
    let transactionId = 'N/A';
    let accountSummary = getAccountSummary(transactionId);
    return accountSummary;

    function processReportRecord(reportRecord) {
        totalCost += parseFloat((reportRecord['Official website price'] - reportRecord['Cash coupon amount']) || 0);
        totalVendorCost += parseFloat(((reportRecord['Official website price'] - reportRecord['Cash coupon amount']) * reportRecord['Partner preference ratio']) || 0);
        domain = reportRecord['Customer account'] || '-';
        date = utils.formatDateFromString(reportRecord['Fee generation time'], 'YYYY-MM-DDTHH:mm:ss[Z]') || '-';
    }

    /**
     * Get the account summary data
     * @param {string} transactionId
     */
    function getAccountSummary(transactionId) {
        let accountSummary = {
            ob: config.ob,
            accountIdCsb: userConfig.accountId,
            subscriptionIdCsb: userConfig.subscriptionId,
            customerId: userConfig.customerId,
            domain: domain,
            currency: config.obCurrency,
            exchangeRate: config.exchangeRate,
            priceFactor: userConfig.priceFactor,
            rebateFactor: userConfig.rebate,
            charge: totalCost * userConfig.correctionFactor * config.exchangeRate,
            chargeVendor: totalVendorCost * config.exchangeRate,
            rebate_vendor: ((totalVendorCost * userConfig.rebate) / 100) * config.exchangeRate,
            chargeRef: totalCost * userConfig.correctionFactor,
            invoiceDate: date,
            chargeVendorRef: totalVendorCost,
            margin: parseFloat((1 - (totalVendorCost/(totalCost * userConfig.correctionFactor))) * 100) || 0,
            rebate_vendor_ref: (totalVendorCost * userConfig.rebate) / 100,
            currencyRef: config.refCurrency,
            transactionId: transactionId
        };
        return accountSummary;
    }
}

/**
 * Get the billing date for a month and year.
 * If is a regeneration, the billing date will be the date of the fist time of that billing.
 * Otherwise the billing date will be the current date.
 * @param {string} date
 * @returns {Date} BillingDate
 */
function getBillingDate(date) {
    logger.debug('getBillingDate');
    let billingDate = config.billingFileDate[date];
    if (!billingDate) {
        billingDate = new Date();
        config.billingFileDate[date] = billingDate.getTime();
        config.forcedBillingRegeneration = false;
    } else {
        billingDate = new Date(billingDate);
        config.forcedBillingRegeneration = true;
    }
    logger.debug('Billing date:', billingDate.toISOString());
    return billingDate;
}
