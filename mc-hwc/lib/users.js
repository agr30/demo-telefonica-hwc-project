const dynamoDBClient = require('../utils/dynamoDb/client');
const logger = require('logops');
const config = require('../commons/config');

/**
 * Load user configuration data from database
 * @param {string} customerId
 * @returns {object} User Config
 */
async function getUserConfig(customerId) {
    logger.debug('GetUserConfig - AccountId:', customerId);
    let dbTable = process.env.DB_USERS || 'huawei-users';
    let key = {
        customerId: customerId.toString()
    };
    let user = await dynamoDBClient.get(config.AWS_ACCOUNT_CONFIG, dbTable, key);
    let userConfig = {
        accountId: customerId,
        subscriptionId: customerId,
        customerId: customerId,
        priceFactor: 0,
        correctionFactor: 1 + (config.taxRate || 0) / 100,
        rebate: 0
    };
    if (user) {
        // If accountId not found in dataTable, use customerId as accountId and subscription (it should not happen)
        if (user.accountId) {
            userConfig.accountId = user.accountId;
        }
        if (user.subscriptionId) {
            userConfig.subscriptionId = user.subscriptionId;
        }
        if (user.priceFactor) {
            userConfig.priceFactor = user.priceFactor;
            userConfig.correctionFactor = (1 + user.priceFactor / 100) * (1 + (config.taxRate || 0) / 100);
        }
        if (user.rebate) {
            userConfig.rebate = user.rebate;
        }
    } else {
        logger.warn('User info not found for customerId:', customerId);
    }
    return userConfig;
}

/**
 * Get the HWC customer id from CSB subscriptionId and accountId
 * @async
 * @param {string} subscriptionId
 * @param {string} accountId
 * @return {string} customerId
 */
async function getCustomerId(subscriptionId, accountId) {
    logger.debug('GetCustomerId - subscriptionId:', subscriptionId, 'accountId:', accountId);
    const dbTable = process.env.DB_USERS || 'huawei-users';
    let params = {
        ProjectionExpression: 'customerId',
        FilterExpression: '#accountId = :accountId and #subsciptionId = :subscriptionId',
        ExpressionAttributeNames: {
            '#accountId': 'accountId',
            '#subsciptionId': 'subscriptionId'
        },
        ExpressionAttributeValues: {
            ':accountId': accountId.toString(),
            ':subscriptionId': subscriptionId.toString()
        }
    };
    let users = await dynamoDBClient.scan(config.AWS_ACCOUNT_CONFIG, dbTable, params);
    if (users.length > 0 && users[0].customerId) {
        let accountId = users[0].customerId;
        return accountId;
    } else {
        throw new Error('CustomerId not found');
    }
}

/**
 * Check if exists an user associated to subscriptionId and accountId
 * @param {string} subscriptionId
 * @param {string} accountId
 * @returns {boolean}
 */
async function existsUser(subscriptionId, accountId) {
    logger.debug('ExistsUser - subscriptionId:', subscriptionId, 'accountId:', accountId);
    const dbTable = process.env.DB_USERS || 'huawei-users';
    let params = {
        ProjectionExpression: 'customerId',
        FilterExpression: '#accountId = :accountId and #subsciptionId = :subscriptionId',
        ExpressionAttributeNames: {
            '#accountId': 'accountId',
            '#subscriptionId': 'subscriptionId'
        },
        ExpressionAttributeValues: {
            ':accountId': accountId.toString(),
            ':subscriptionId': subscriptionId.toString()
        }
    };
    let users = await dynamoDBClient.scan(config.AWS_ACCOUNT_CONFIG, dbTable, params);
    return users.length > 0;
}

/**
 * Check if exists an user with given accountId
 * @param {string} customerId
 * @returns {boolean}
 */
async function existsAccount(customerId) {
    logger.debug('ExistsAccount - customerId:', customerId);
    const dbTable = process.env.DB_USERS || 'huawei-users';
    let params = {
        ProjectionExpression: 'customerId',
        FilterExpression: '#customerId = :customerId',
        ExpressionAttributeNames: {
            '#customerId': 'customerId'
        },
        ExpressionAttributeValues: {
            ':customerId': customerId.toString()
        }
    };
    let users = await dynamoDBClient.scan(config.AWS_ACCOUNT_CONFIG, dbTable, params);
    return users.length > 0;
}

exports.getUserConfig = getUserConfig;
exports.getCustomerId = getCustomerId;
exports.existsUser = existsUser;
exports.existsAccount = existsAccount;
