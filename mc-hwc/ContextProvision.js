'use strict';

const logger = require('logops');
// const async = require('async');
// const fs = require('fs');
const AWS = require('aws-sdk');
// const utils = require('./utils');
const constants = require('./constants');
const s3Management = require('./services/s3/s3Management.js');
const resourceClient = require('./services/resourceAPIClient.js');
// const uuidv1 = require('uuid/v1');

const Errors = require('./commons/errors');
const dynamoClient = require('./utils/dynamoDb/client');
const config = require('./commons/config');
const huaweiApi = require('./services/huaweiCloud/huaweiCloudAPIClient');
const mail = require('./mailSender');

exports.listBillingFiles = function listBillingFiles(contextId, config, callback) {
    logger.info('Processing listBillingFiles request');

    resourceClient.get(config, contextId, function (err, contextData) {
        if (err) {
            logger.error('CODE-61-0112 Error obtaining context data', err);
            return callback(err);
        }

        logger.info('context data', contextData);
        let xAccountId = contextData.xAccountId;
        logger.info('xAccountId from context data', xAccountId);

        let accountId = xAccountId.split('_')[0];
        logger.info('accountId from xAccountId data', accountId);
        let subscriptionId = xAccountId.split('_')[1];
        logger.info('subscriptionId from xAccountId data', subscriptionId);

        logger.debug('accountId %s and subscriptionId %s', accountId, subscriptionId);

        if (!accountId || !subscriptionId) {
            logger.error('CODE-61-0113 Could not obtain accountId or suscriptionId from context', contextData);
            return callback(contextData);
        }

        s3Management.listBillingFiles(config, accountId, subscriptionId, function (err, response) {
            if (err) {
                logger.error('contextProvision.listBillingFiles. BILLING ERROR: ', err);
                return callback(err, generateResponse(500, { error: err, message: '' }));
            } else {
                logger.info('contextProvision.listBillingFiles response %s', response);
                return callback(null, generateResponse(200, response));
            }
        });
    });
};

function generateResponse(statusCode, body, headersResponse) {
    // The output from a Lambda proxy integration must be
    // of the following JSON object. The 'headers' property
    // is for custom response headers in addition to standard
    // ones. The 'body' property  must be a JSON string. For
    // base64-encoded payload, you must also set the 'isBase64Encoded'
    // property to 'true'.
    return {
        statusCode: statusCode,
        headers: headersResponse,
        body: JSON.stringify(body)
    };
}

function generateAsyncResponse(req, res, next) {
    if (res.jsonData) {
        logger.debug('generateAsyncResponse 202' + JSON.stringify(res.jsonData));
        res.header('APS-Info', res.jsonData.order.orderID);
        res.header('APS-Retry-Timeout', '300');
        let response = {
            orderId: res.jsonData.order.orderID,
            orderStatus: res.jsonData.order.status
        };
        res.status(202).json(response);
    } else {
        next(constants.errors.BODY_NOT_FOUND);
    }
}

function generateSyncResponseForUpdate(req, res, next) {
    logger.debug('generateSyncResponseForUpdate 200');
    res.status(200).json({});
}

function handleDelete(req, res, next) {
    logger.info('Returning from the delete handler');
    res.status(204).send('');
    logger.debug('Status code: %d', res.statusCode);
}

exports.downloadFile = function downloadFile(event, context, callback) {
    logger.info('Event: ', event);
    let stage = (event.requestContext) ? event.requestContext.stage || 'Prod' : 'Prod';
    logger.info('Stage: ', stage);
    utils.loadConfiguration(stage, function (err, configData) {
        if (err || !configData) {
            logger.error('CODE-61-0280 Error getting stored params: ', err);
            return callback(null, generateResponse(400, { 'error': err, 'message': 'Missing configuration' }));
        }

        exportConf(configData);

        let path_file = event.pathParameters.fileName;
        logger.info('DownloadFile %s', path_file);
        logger.info('**************************');

        csService.downloadFile(path_file, config, function (err, message) {
            if (err) {
                callback(err);
            } else {
                // res.jsonData = message.response;
                // res.jsonData = "";

                let date = new Date();
                let dateF = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
                let filename = 'usage_' + dateF + '.csv';

                res.setHeader('content-type', 'text/csv');
                res.setHeader('content-disposition', 'attachment; filename=' + filename);
                res.send(req.body.csvContent, 200);

                console.log('*******************');
                callback();
            }
        });
    });
};

exports.contextUpdate = async function contextUpdate(customerInfo, customerDynamo) {
    let updateContext;
    let dbTable = process.env.DB_USERS || 'huawei-users';
    logger.debug('ContextUpdate in dynamo  user from dynamo:', customerDynamo);
    logger.debug('ContextUpdate in dynamo  user data to update:', customerInfo);
    if (customerInfo.priceFactor && customerDynamo.priceFactor !== customerInfo.priceFactor) {
        updateContext = {
            expression: 'set priceFactor = :priceFactor',
            attributes: {
                ':priceFactor': customerInfo.priceFactor
            }
        };
    }
    logger.debug('UpdateContext after check priceFactor is : ', updateContext);
    if (customerInfo.email && customerDynamo.email !== customerInfo.email) {
        if (updateContext) {
            updateContext.expression += ', email = :email';
            updateContext.attributes[':email'] = customerInfo.email;
        } else {
            updateContext = {
                expression: 'set email = :email',
                attributes: {
                    ':email': customerInfo.email
                }
            };
        }
        let params = {
            email: customerInfo.email,
            password: '',
            domainName: customerInfo.domainName,
            template: 'update',
            mailFolder: config.obHuawei.mailFolder
        };
        await sendEmail(params);
    }
    logger.debug('UpdateContext after check email is : ', updateContext);

    if (updateContext) {
        updateContext.key = { customerId: customerInfo.domainId.toString() };
        logger.debug('UpdateContext final is : ', updateContext);
        await dynamoClient.update(config.AWS_ACCOUNT_CONFIG, dbTable, updateContext);
    }
    // we have to notify the customer that if he wants to change the email has to open a ticket in huawei
};


exports.contextMigrationUpdate = async function contextMigrationUpdate(customer) {
    let dbTable = process.env.DB_USERS || 'huawei-users';
    let dbTableControl = process.env.DB_MIGRATION || 'huawei-migration'
    logger.debug('ContextMigrationUpdate in dynamo  user:', customer);

    let updateContext = {
        key: {
            customerId: customer.domainId.toString()
        },
        expression: 'set domainName = :domainName, xAccountId = :xAccountId, priceFactor = :priceFactor, accountStatus = :status',
        attributes: {
            ':domainName': customer.domainName,
            ':xAccountId': customer.xAccountId,
            ':priceFactor': customer.priceFactor || 0,
            ':status': 'Active'
        }
    };
    if (customer.email) {
        updateContext.expression += ", email= :email";
        updateContext.attributes[':email'] = customer.email;
    }

    logger.debug('MigrationUpdateContext in huawei-user final is : ', updateContext);
    await dynamoClient.update(config.AWS_ACCOUNT_CONFIG, dbTable, updateContext);

    updateContext = {
        key: {
            domainName: customer.domainName.toString()
        },
        expression: 'set xAccountId = :xAccountId, migrationTimeStamp = :migrationTimeStamp',
        attributes: {
            ':xAccountId': customer.xAccountId,
            ':migrationTimeStamp': new Date().getTime(),
        }
    };

    logger.debug('MigrationUpdateContext in control table final is : ', updateContext);
    await dynamoClient.update(config.AWS_ACCOUNT_CONFIG, dbTableControl, updateContext);

};

exports.contextExist = async function contextExist(searchOptions) {
    logger.info('Check if user exist begins:', searchOptions);
    logger.debug('huaweiApi.getHuaweiToken() init');
    let token = await huaweiApi.getHuaweiToken();
    logger.debug('huaweiApi.getHuaweiToken() end');
    if (!token) {
        logger.error('Error getting token param');
        throw new Errors.BadParameterError('Error in token', 'Missing parameter');
    }
    let options = {
        method: 'POST',
        uri: config.obHuawei.createEndpoint + config.version + config.obHuawei.account.partnerId + config.checkExistCustomer,
        json: true,
        headers: {
            'X-Auth-Token': token
        },
        body: {
            "searchType": searchOptions.Type,
            "searchKey": searchOptions.Key
        }
    };
    try {
        logger.debug('huaweiApi.apiCallHuawei() init, options', options);
        let resp = await huaweiApi.apiCallHuawei(options);
        if (resp.error_code === 'CBC.0000' && resp.status === '1') {
            return true;
        }
        return false;
    } catch (err) {
        logger.error('CODE-61-0244 Error checking if user exist: ', err);
        throw new Errors.ProvisionError('Error checking if user exist');
    }
}

exports.contextCreation = async function contextCreation(body) {
    let dbTable = process.env.DB_USERS || 'huawei-users';

    logger.debug('huaweiApi.getHuaweiToken() init');
    let token = await huaweiApi.getHuaweiToken();
    logger.debug('huaweiApi.getHuaweiToken() end');
    if (!token) {
        logger.error('Error getting token param');
        throw new Errors.BadParameterError('Error in token', 'Missing parameter');
    }
    // pedirle a huawei un metodo para saber si existe un determinado usuario o dependiendo del error que de la creacion de usuario si ese codigo es pq existe un usuario
    logger.debug('huaweiApi.generatePassword() init');
    let password = huaweiApi.generatePassword();
    logger.debug('huaweiApi.generatePassword() end');
    let options = {
        method: 'POST',
        uri: config.obHuawei.createEndpoint + config.version + config.obHuawei.account.partnerId + config.createContextUrl,
        json: true,
        headers: {
            'X-Auth-Token': token
        },
        body: {
            'domainName': body.domainName,
            'domainArea': config.obHuawei.domainArea,
            'xAccountId': body.xAccountId,
            'xAccountType': config.obHuawei.xAccountType,
            'password': password
        }
    };
    try {
        // we create the customer in huawei
        logger.debug('huaweiApi.apiCallHuawei() init, options', options);
        let resp = await huaweiApi.apiCallHuawei(options);
        logger.debug('huaweiApi.apiCallHuawei() end');
        let domainId = resp.domainId;
        let domainName = resp.domainName;
        let updateContext = {
            key: {
                customerId: domainId.toString()
            },
            expression: 'set domainName = :domainName, xAccountId = :xAccountId, priceFactor = :priceFactor, email = :email, accountStatus = :status',
            attributes: {
                ':domainName': domainName,
                ':xAccountId': body.xAccountId,
                ':priceFactor': body.priceFactor || 0,
                ':email': body.email,
                ':status': 'Active'
            }
        };
        logger.debug('contextCreation: dynamoDB updating');
        await dynamoClient.update(config.AWS_ACCOUNT_CONFIG, dbTable, updateContext);
        logger.debug('contextCreation: dynamoDB updated');
        // send the domainId to dynamo, update the user /// the email ?
        await setCustomerLimit(domainId, config.obHuawei.creditLimit); // poner como clave domainId
        logger.debug('contextCreation: customer limit updated');
        // send the email to the user with the PASSWORD
        // TODO : configurate the message to send  like so :

        let params = {
            email: body.email,
            password: password,
            domainName: domainName,
            template: 'user-welcome',
            mailFolder: config.obHuawei.mailFolder
        };
        logger.debug('contextCreation: sending email');
        await sendEmail(params);
        return resp;
    } catch (err) {
        logger.error('CODE-61-0240 Error in ContextProvision context creation: ', err);
        throw new Errors.ProvisionError('Error in ContextProvision context creation');
    }
};

// this function only can be call in two scenario, when we created a customer or when we're gonna suspend a customer and set the limit to 0
const setCustomerLimit = exports.setCustomerLimit = async function setCustomerLimit(domainId, limit = 0) {
    if (!domainId) {
        logger.error('Error missing parameter domainId');
        throw new Errors.BadParameterError('Error in domainId', 'Missing parameter');
    }
    let token = await huaweiApi.getHuaweiToken();
    if (!token) {
        logger.error('Error getting token param');
        throw new Errors.BadParameterError('Error in token', 'Missing parameter');
    }
    let options = {
        method: 'POST',
        uri: config.obHuawei.createEndpoint + config.version + config.obHuawei.account.partnerId + config.setLimitUrl,
        json: true,
        headers: {
            'X-Auth-Token': token
        },
        body: {
            'customerId': domainId,
            'adjustmentAmount': limit,
            'measureId': 1
        }
    };
    try {
        await huaweiApi.apiCallHuawei(options);
        return true;
    } catch (err) {
        logger.error('CODE-61-0241 Error setting limits: ', err);
        throw new Errors.ProvisionError('Error setting limits using Huawei Api');
    }
};

exports.queryingCustomerLimit = async function queryingCustomerLimit(domainId) {
    if (!domainId) {
        logger.error('Error missing parameter customerId');
        throw new Errors.BadParameterError('Error in domainId', 'Missing parameter');
    }
    let token = await huaweiApi.getHuaweiToken();
    if (!token) {
        logger.error('Error getting token param');
        throw new Errors.BadParameterError('Error in token', 'Missing parameter');
    }
    let options = {
        method: 'GET',
        uri: config.obHuawei.createEndpoint + config.version + config.obHuawei.account.partnerId + config.queryingLimitUrl + domainId,
        json: true,
        headers: {
            'X-Auth-Token': token
        }
    };
    try {
        return await huaweiApi.apiCallHuawei(options);
    } catch (err) {
        logger.error(err, 'Error querying limiter');
        throw new Errors('Error in calling querying limite huawei Api');
    }
};

const sendEmail = exports.sendEmail = async function sendEmail(params) {
    logger.debug('sendEmail: init sending email');
    const smtpOptions = config.smtp;
    const mailOptions = {
        from: config.emailFrom,
        to: params.email,
        template: params.template,
        mailFolder: params.mailFolder
    };
    const placeholders = {
        'domainName': params.domainName,
        'url': config.urlPortal,
        'password': params.password || ''
    };
    await new Promise((resolve, reject) => {
        logger.debug('sendEmail: inside promise');
        mail.sendCustomMail(smtpOptions, mailOptions, placeholders, function (err) {
            if (err) {
                logger.error('CODE-61-0242 Error send email in context creation: ', err);
                reject(new Errors.ProvisionError('Error in send email in context creation'));
            }
            logger.debug('sendEmail no error');
            resolve();
        });
    });
    logger.debug('sendEmail out promise');
    return true;
};

exports.getTotalAmountForContextId = async function getTotalAmountForContextId(event, config, context, contextId, callback) {
    try {
        logger.info('Event: ', event);
        let stage = (event.requestContext) ? event.requestContext.stage || 'Prod' : 'Prod';
        logger.info('Stage: ', stage);

        let totalAmount = 0;

        resourceClient.get(config, contextId, async function (err, contextData) {
            if (err) {
                logger.error('CODE-61-0112 Error obtaining context data', err);
                return callback(err);
            }

            logger.info('context data', contextData);
            let domainId = contextData.domainId;
            logger.info('domainId from context data', domainId);

            logger.debug('resourceClient domainId: ' + domainId);

            let lastHWCInvoiceDate = config.lastHWCInvoiceDate;
            let lastHWCRetrieveDate = config.lastHWCRetrieveDate;
            logger.info('lastHWCRetrieveDate: ' + lastHWCRetrieveDate + '.lastHWCInvoiceDate: ' + lastHWCInvoiceDate);

            if ((lastHWCInvoiceDate !== 'none' && lastHWCRetrieveDate === 'none') || (new Date(lastHWCInvoiceDate) > new Date(lastHWCRetrieveDate))) {
                let billingDate = config.billingFileDate[lastHWCInvoiceDate];
                let fd = formatDate(billingDate);
                let pattern = 'hwc_summary' + config.delimiter + config.ob + config.delimiter + fd;
                logger.debug('filename pattern ', pattern);

                s3Management.getFileFromBucket(config, domainId, pattern, async function (err, totalAmount) {
                    if (err) {
                        logger.error('CODE-61-0114 retrieve.listing files from s3 ERROR: ', err);
                        totalAmount = 0;
                        return callback(null, { amount: { usage: totalAmount } });
                    } else {
                        let retrieveDate = config.retrieveFileDate;
                        if (!retrieveDate[lastHWCInvoiceDate]) {
                            retrieveDate[lastHWCInvoiceDate] = (new Date()).getTime();
                            config.retrieveFileDate = retrieveDate;

                        }
                        logger.info('amount: ' + totalAmount);
                        config.lastHWCRetrieveDate = lastHWCInvoiceDate;
                        await config.updateConfiguration(stage);

                        totalAmount = totalAmount <= 0 ? 0 : totalAmount;
                        logger.info('lastHWCRetrieveDate updated to ' + lastHWCInvoiceDate + ' before sending amount ' + (100 * totalAmount));
                        return callback(null, { amount: { usage: 100 * totalAmount } });
                    }
                });
            } else {
                totalAmount = 0;
                logger.info('amount: ' + totalAmount + '. lastHWCRetrieveDate:' + lastHWCRetrieveDate + ' lastHWCInvoiceDate:' + lastHWCInvoiceDate);
                return callback(null, { amount: { usage: totalAmount } });
            }
        });
    } catch (err) {
        logger.error(err);
        return callback(null, generateResponse(500, { error: 'Internal error', message: 'An internal error has occurred during execution' }));
    };
};

function formatDate(date) {
    let d = new Date(date);
    let fd = d.getFullYear();
    fd += formatToTwo(d.getMonth() + 1).toString();
    fd += formatToTwo(d.getDate());
    fd += formatToTwo(d.getHours());
    fd += formatToTwo(d.getMinutes());
    fd += formatToTwo(d.getSeconds());
    return fd;
}

function formatToTwo(element) {
    if (element.toString().length === 2) {
        return element;
    }
    return '0' + element;
}

