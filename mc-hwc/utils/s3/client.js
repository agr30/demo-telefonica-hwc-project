'use strict';

const logger = require('logops');
const S3 = require('aws-sdk/clients/s3');
const s3SelectObject = require('./s3SelectObject');
const awsUtils = require('../utils');
const Stream = require('stream');
const Errors = require('../../commons/errors');

/**
 * Get an object from AWS S3 from the given path in the bucket configured in bucket
 * @param {object} awsConfig
 * @param {string} bucket
 * @param {string} path
 * @return {any} Object
 */
async function getObject(awsConfig, bucket, path) {
    try {
        logger.debug('S3 Get object');
        let awsOptions = awsUtils.getS3Options(awsConfig);
        let s3 = new S3(awsOptions);
        let params = {
            Bucket: bucket,
            Key: path
        };
        let s3Object = await s3.getObject(params).promise();
        return s3Object;
    } catch (err) {
        throw new Errors.AwsError('CODE-61-0214 Error getting S3 Object', err);
    }
}
exports.getObject = getObject;

/**
 * Get an object stream from AWS S3 from the given path in the bucket configured
 * @param {object} awsConfig
 * @param {string} bucket
 * @param {string} path
 * @return {any} Object
 */
async function getObjectStream(awsConfig, bucket, path) {
    try {
        logger.debug('S3 Get object stream');
        let awsOptions = awsUtils.getS3Options(awsConfig);
        let s3 = new S3(awsOptions);
        let params = {
            Bucket: bucket,
            Key: path
        };
        let s3ObjectStream = s3.getObject(params).createReadStream();
        return s3ObjectStream;
    } catch (err) {
        throw new Errors.AwsError('CODE-61-0214 Error getting S3 Object', err);
    }
}
exports.getObjectStream = getObjectStream;

/**
 * Check if an object exist in AWS S3 at the given path in the bucket configured in bucket
 * @param {object} awsConfig
 * @param {string} bucket
 * @param {string} path
 * @returns {boolean} Result
 */
exports.existsObject = async function(awsConfig, bucket, path) {
    try {
        let awsOptions = awsUtils.getS3Options(awsConfig);

        let s3 = new S3(awsOptions);
        let params = {
            Bucket: bucket,
            Key: path
        };
        await s3.headObject(params).promise();
        logger.debug('S3 Exists object (' + bucket + ':/' + path + '): true');
        return true;
    } catch (err) {
        if (err.code === 'NotFound') {
            logger.debug('S3 Exists object (' + bucket + ':/' + path + '): false');
            return false;
        } else {
            throw new Errors.AwsError('CODE-61-0215 Error checking if exists S3 Object', err);
        }
    }
};

/**
 * Copy an object from AWS S3 from the given path to another given path in the same bucket configured in bucket
 * @param {object} awsConfig
 * @param {string} bucket
 * @param {string} path
 * @param {string} newPath
 */
exports.copyObject = async function(awsConfig, bucket, path, newPath) {
    try {
        logger.debug('S3 Copy object');
        let awsOptions = awsUtils.getS3Options(awsConfig);
        let s3 = new S3(awsOptions);
        let paramsCopy = {
            Bucket: bucket,
            CopySource: path,
            Key: newPath
        };
        await s3.copyObject(paramsCopy).promise();
    } catch (err) {
        throw new Errors.AwsError('CODE-61-0216 Error copying S3 Object', err);
    }
};

/**
 * Delete an object in AWS S3 at the given path in the bucket configured in bucket
 * @param {object} awsConfig
 * @param {string} bucket
 * @param {string} path
 */
const deleteObject = exports.deleteObject = async function(awsConfig, bucket, path) {
    try {
        logger.debug('S3 Delete object');
        let awsOptions = awsUtils.getS3Options(awsConfig);
        let s3 = new S3(awsOptions);
        let params = {
            Bucket: bucket,
            Key: path
        };
        await s3.deleteObject(params).promise();
    } catch (err) {
        throw new Errors.AwsError('CODE-61-0217 Error deleting S3 Object', err);
    }
};

/**
 * Upload an object throught a stream to AWS S3 to given path in the bucket configured in bucket
 * @param {object} awsConfig
 * @param {string} bucket
 * @param {string} path
 * @returns {Readable} UploadStream
 */
exports.getUploadStream = function getUploadStream(awsConfig, bucket, path) {
    try {
        const uploadStream = new Stream.PassThrough();
        let awsOptions = awsUtils.getS3Options(awsConfig);
        let s3 = new S3(awsOptions);
        let params = {
            Bucket: bucket,
            Key: path,
            Body: uploadStream
        };
        s3.upload(params, function(err) {
            if (err) {
                uploadStream.emit('error', new Error(err));
            } else {
                uploadStream.emit('uploaded');
            }
        });
        return uploadStream;
    } catch (err) {
        throw new Errors.AwsError('CODE-61-0218 Error uploading stream to S3', err);
    }
};
exports.uploadStream = function uploadStream(awsConfig, bucket, path, stream) {
    try {
        logger.debug('S3 Upload stream');
        let awsOptions = awsUtils.getS3Options(awsConfig);
        let s3 = new S3(awsOptions);
        let params = {
            Bucket: bucket,
            Key: path,
            Body: stream
        };
        s3.upload(params, function(err) {
            if (err) {
                stream.emit('error', new Error(err));
            } else {
                stream.emit('uploaded');
            }
        });
    } catch (err) {
        throw new Errors.AwsError('CODE-61-0218 Error uploading stream to S3', err);
    }
};

exports.selectObject = s3SelectObject.selectObject;
exports.selectObjectStream = s3SelectObject.selectObjectStream;

const listObject = exports.listObject = async function(awsConfig, bucket) {
    try {
        logger.debug('S3 List objects in bucket');
        let awsOptions = awsUtils.getS3Options(awsConfig);
        let s3 = new S3(awsOptions);
        let params = {
            Bucket: bucket
        };
        let obj = await s3.listObjects(params).promise();
        if (obj && obj.Contents && obj.Contents.length > 0) {
            obj = obj.Contents;
        } else {
            return [];
        }
        return obj;
    } catch (err) {
        throw new Errors.AwsError('CODE-61-0221 Error listing objects in S3 Object', err);
    }
};

exports.deleteAllObject = async function(awsConfig, bucket) {
    try {
        logger.debug('S3 Delete all objects');
        let listObj = await listObject(awsConfig, bucket);
        if (listObj.length > 0) {
            listObj.forEach(element => {
                deleteObject(awsConfig, bucket, element.Key);
            });
        }
    } catch (err) {
        throw new Errors.AwsError('CODE-61-0222 Error deleting all objects in S3 bucket', err);
    }
};
