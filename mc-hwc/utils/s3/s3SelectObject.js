const _ = require('lodash');
const S3 = require('aws-sdk/clients/s3');
const logger = require('logops');
const SelectObjectJsonStream = require('./selectObjectJsonStream');
const awsUtils = require('../utils');
const Stream = require('stream');
const Errors = require('../../commons/errors');

/**
 * Get a stream with the results of a query over a S3 file in CSV format compressed using GZIP
 * @async
 * @param {object} awsConfig
 * @param {string} bucket
 * @param {string} path
 * @param {object} query
 * @param {object} options
 * @returns {Stream} Stream
 */
async function selectObjectStream(awsConfig, bucket, path, query, options = {}) {
    logger.debug('S3 Select object stream:', path);
    const JSON_DELIMITER = '\n';
    try {
        let awsOptions = awsUtils.getS3Options(awsConfig);
        let s3 = new S3(awsOptions);
        let params = {
            Bucket: bucket,
            Key: path,
            ExpressionType: 'SQL',
            Expression: 'SELECT ' + getHeaders(options) + ' FROM S3Object' + getQuery(query),
            InputSerialization: {
                CSV: {
                    FileHeaderInfo: 'USE',
                    RecordDelimiter: '\n',
                    FieldDelimiter: ','
                }
            },
            OutputSerialization: {
                JSON: {
                    RecordDelimiter: JSON_DELIMITER
                }
            }
        };

        let selectObjectContent = await s3.selectObjectContent(params).promise();
        let selectObjectContentStream = selectObjectContent.Payload;
        // AWS returns a stream of objects and the string content is into Records.Payload
        // We create our own stream and send through it the string content
        let selectObjectRowStream = new Stream.Transform({
            objectMode: true,
            transform: function(data, encoding, callback) {
                if (data.Records && data.Records.Payload) {
                    callback(null, data.Records.Payload);
                } else {
                    callback();
                }
            }
        });
        if (selectObjectContentStream) {
            selectObjectContentStream.pipe(selectObjectRowStream);
            selectObjectContentStream.on('error', err => selectObjectRowStream.emit('error', new Errors.AwsError('CODE-61-0219 Error querying S3 CSV', err)));
        } else {
            selectObjectRowStream.emit('end');
        }
        // Once we have data throught string stream, we do a pipe from string stream (dataStream) to a JSON stream
        // Using this, we have a stream which each chunk is a JSON of CSV line
        let selectObjectJsonStream = new SelectObjectJsonStream(JSON_DELIMITER);
        selectObjectRowStream.pipe(selectObjectJsonStream);
        selectObjectRowStream.on('error', err => selectObjectJsonStream.emit('error', err));
        return selectObjectJsonStream;
    } catch (err) {
        throw new Errors.AwsError('CODE-61-0219 Error querying S3 CSV', err);
    }
};

/**
 * Get the results of a query over a S3 file in CSV format compressed using GZIP
 * @async
 * @param {object} awsConfig
 * @param {string} bucket
 * @param {string} path
 * @param {object} query
 * @param {object} options
 * @returns {object[]} Results
 */
async function selectObject(awsConfig, bucket, path, query, options = {}) {
    logger.debug('S3 Select object');
    let objectContent = [];
    let objectStream = await selectObjectStream(awsConfig, bucket, path, query, options);
    objectStream.on('data', function(data) {
        objectContent.push(data);
    });
    // Wait until stream is fully processed
    await new Promise((resolve, reject) => {
        objectStream.on('error', reject);
        objectStream.on('end', resolve);
    });
    return objectContent;
};

/**
 * Extract the fields for the query SELECT from the options
 * If there are headers in options, return headers in format '"header1", "header2", ...'
 * Else return '*'
 * @param {object} options
 * @returns {string} Headers
 */
function getHeaders(options) {
    let sanitizedHeaders = [];

    if (options.headers && _.isArray(options.headers) && options.headers.length > 0) {
        for (let header of options.headers) {
            let sanitizedHeader = header;
            if (!sanitizedHeader.startsWith('"')) {
                sanitizedHeader = '"' + sanitizedHeader;
            }
            if (!sanitizedHeader.endsWith('"')) {
                sanitizedHeader = sanitizedHeader + '"';
            }
            sanitizedHeaders.push(sanitizedHeader);
        }
        sanitizedHeaders = sanitizedHeaders.join(',');
    } else {
        sanitizedHeaders = '*';
    }
    logger.debug('SELECT', sanitizedHeaders);
    return sanitizedHeaders;
}

/**
 * Transform the query JSON in SQL WHERE clause
 * @param {object} query
 * @returns {string} WHERE Clause
 */
function getQuery(query) {
    let queryElements = [];
    if (query && _.isObject(query) && Object.keys(query).length > 0) {
        for (let queryElementKey in query) {
            if (_.isObject(query[queryElementKey])) {
                let queryElementOperations = query[queryElementKey];
                for (let queryElementOperator in queryElementOperations) {
                    let queryElementOperationValue = queryElementOperations[queryElementOperator];
                    let queryElement = getQueryElement(queryElementKey, queryElementOperator, queryElementOperationValue);
                    queryElements.push(queryElement);
                }
            } else {
                let queryElementValue = query[queryElementKey];
                let queryElement = getQueryElement(queryElementKey, '$eq', queryElementValue);
                queryElements.push(queryElement);
            }
        }
    }
    if (queryElements.length > 0) {
        logger.debug(' WHERE ' + queryElements.join(' AND '));
        return ' WHERE ' + queryElements.join(' AND ');
    } else {
        return '';
    }
}

/**
 * Get the WHERE clause logic operation from key, operator and value
 * Applying the necessary casting to the values
 * @param {string} key
 * @param {string} operator
 * @param {any} value
 * @returns {string} Logic Operation
 */
function getQueryElement(key, operator, value) {
    if (!key.startsWith('"')) {
        key = '"' + key;
    }
    if (!key.endsWith('"')) {
        key = key + '"';
    }
    operator = parseQueryOperator(operator);
    if (_.isString(value)) {
        return key + operator + "'" + value + "'";
    } else if (_.isNumber(value)) {
        return 'CAST(' + key + ' AS FLOAT) ' + operator + ' ' + parseFloat(value);
    } else if (_.isDate(value)) {
        let utcDate = new Date(Date.UTC(value.getFullYear(), value.getMonth(), value.getDate()));
        return 'CAST(' + key + ' AS TIMESTAMP) ' + operator + " CAST('" + utcDate.toISOString() + "' AS TIMESTAMP)";
    } else if (_.isBoolean(value)) {
        return 'CAST(' + key + ' AS BOOLEAN) ' + operator + ' ' + value;
    } else {
        throw new Error('Cannot parse query element - Invalid value type');
    }
}

/**
 * Transform query JSON operator in SQL WHERE clause operator
 * @param {string} operator
 * @returns {string} SQL Operator
 */
function parseQueryOperator(operator) {
    switch (operator) {
        case '$gt':
            return '>';
        case '$lt':
            return '<';
        case '$gte':
            return '>=';
        case '$lte':
            return '<=';
        case '$eq':
            return '=';
        case '$in':
            return 'IN';
        default:
            throw new Error('Cannot parse query operator - Invalid query operator');
    }
}

exports.selectObjectStream = selectObjectStream;
exports.selectObject = selectObject;
