const Stream = require('stream');

// Stream that read buffer chunks and parse the chunks to get lines data
// and parse their to JSON before write in the stream
class SelectObjectJsonStream extends Stream.Transform {
    constructor(delimiter) {
        super({ objectMode: true });
        this.acc = '';
        this.delimiter = delimiter;
    }
    _transform(chunk, encode, callback) {
        let chunkString = this.acc + chunk.toString('utf8');
        let chunkData = chunkString.substr(0, chunkString.lastIndexOf(this.delimiter) + 1);
        this.acc = chunkString.substr(chunkString.lastIndexOf(this.delimiter) + 1, chunkString.length);
        if (chunkData.length > 0) {
            let chunkDataRecords = chunkData.split(this.delimiter);
            for (let chunkDataRecord of chunkDataRecords) {
                if (chunkDataRecord.length > 0) {
                    let record = JSON.parse(chunkDataRecord);
                    this.push(record); // Write to stream
                }
            }
        }
        callback();
    }
}

module.exports = SelectObjectJsonStream;
