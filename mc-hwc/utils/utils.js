
function getAwsCredentials(awsConfig) {
    let awsCredentials = {};
    if (awsConfig && awsConfig.accessKeyId) {
        awsCredentials.accessKeyId = awsConfig.accessKeyId;
    }
    if (awsConfig && awsConfig.secretAccessKey) {
        awsCredentials.secretAccessKey = awsConfig.secretAccessKey;
    }
    if (awsConfig && awsConfig.sessionToken) {
        awsCredentials.sessionToken = awsConfig.sessionToken;
    }
    return awsCredentials;
};

exports.getS3Options = function(awsConfig) {
    let awsOptions = {};
    let awsCredentials = getAwsCredentials(awsConfig);
    Object.assign(awsOptions, awsCredentials);
    if (awsConfig && awsConfig.endpoint) {
        awsOptions.endpoint = awsConfig.endpoint;
    }
    return awsOptions;
};

exports.getDynamoDbOptions = function(awsConfig) {
    let awsOptions = {};
    let awsCredentials = getAwsCredentials(awsConfig);
    Object.assign(awsOptions, awsCredentials);
    if (awsConfig && awsConfig.region) {
        awsOptions.region = awsConfig.region;
    }
    return awsOptions;
};

exports.getSecretsManagerOptions = function(awsConfig) {
    let awsOptions = {};
    let awsCredentials = getAwsCredentials(awsConfig);
    Object.assign(awsOptions, awsCredentials);
    if (awsConfig && awsConfig.region) {
        awsOptions.region = awsConfig.region;
    }
    return awsOptions;
};
