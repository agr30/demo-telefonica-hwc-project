'use strict';

var fs = require('fs');

function readJsonFile(name, raw) {
    var text = fs.readFileSync(name, 'UTF8');

    if (raw) {
        return text;
    } else {
        return JSON.parse(text);
    }
}

function removeWithoutException(name) {
    return function(callback) {
        fs.unlink(name, function() {
            callback();
        });
    };
}

module.exports.readJsonFile = readJsonFile;
module.exports.removeWithoutException = removeWithoutException;
