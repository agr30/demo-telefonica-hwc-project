const SecretsManager = require('aws-sdk/clients/secretsmanager');
const Errors = require('../../commons/errors');
const awsUtils = require('../utils');

/**
 * Get a secret by name from secrets manager
 * @param {any} awsConfig - JSON with AWS configuration
 * @param {string} secretName - Name of secret
 * @param {string} [stage] - Stage of secret
 * @returns {any} Secret
 */
exports.get = async function get(awsConfig, secretName) {
    try {
        let awsOptions = awsUtils.getSecretsManagerOptions(awsConfig);
        const secretsManager = new SecretsManager(awsOptions);
        let params = {
            SecretId: secretName
        };
        let data = await secretsManager.getSecretValue(params).promise();
        if (data && data.SecretString) {
            return data.SecretString;
        } else if (data && data.SecretBinary) {
            return data.SecretBinary;
        } else {
            return null;
        }
    } catch (err) {
        throw new Errors.AwsError('CODE-61-0220 Error getting secret from SecretsManager', err);
    }
};
