const DynamoDB = require('aws-sdk/clients/dynamodb');
const awsUtils = require('../utils');
const Errors = require('../../commons/errors');

/**
 * Get an item by key from DynamoDB from the given table
 * @param {any} awsConfig - JSON with AWS configuration
 * @param {string} tableName - Name of table
 * @param {any} key - JSON with key attributes
 * @returns {any} Item
 */
exports.get = async function get(awsConfig, tableName, key) {
    try {
        let awsOptions = awsUtils.getDynamoDbOptions(awsConfig);
        let docClient = new DynamoDB.DocumentClient(awsOptions);

        let params = {
            TableName: tableName,
            Key: key
        };
        let response = await docClient.get(params).promise();
        if (response && response.Item) {
            return response.Item;
        } else {
            return null;
        }
    } catch (err) {
        throw new Errors.AwsError('CODE-61-0210 Error getting data from DynamoDB', err);
    }
};

/**
 * Get items from DynamoDB using params specifications
 * @param {any} awsConfig - JSON with AWS configuration
 * @param {string} tableName - Name of table
 * @param {any} params - DynamoDB params with query expression
 * @returns {any[]} Items
 */
exports.query = async function query(awsConfig, tableName, params = {}) {
    try {
        let awsOptions = awsUtils.getDynamoDbOptions(awsConfig);
        let docClient = new DynamoDB.DocumentClient(awsOptions);

        params.TableName = tableName;
        let response = await docClient.query(params).promise();
        if (response && response.Items) {
            return response.Items;
        } else {
            return [];
        }
    } catch (err) {
        throw new Errors.AwsError('CODE-61-0211 Error querying data from DynamoDB', err);
    }
};

/**
 * Query a table of DynamoDB using params specifications to get items
 * @param {any} awsConfig - JSON with AWS configuration
 * @param {string} tableName - Name of table
 * @param {any} params - DynamoDB params with scan expression
 * @returns {any[]} Items
 */
exports.scan = async function scan(awsConfig, tableName, params = {}) {
    try {
        let awsOptions = awsUtils.getDynamoDbOptions(awsConfig);
        let docClient = new DynamoDB.DocumentClient(awsOptions);

        params.TableName = tableName;
        let response = await docClient.scan(params).promise();
        if (response && response.Items) {
            return response.Items;
        } else {
            return [];
        }
    } catch (err) {
        throw new Errors.AwsError('CODE-61-0212 Error scanning data from DynamoDB', err);
    }
};

/**
 * Put an item at DynamoDB in the given table, if another item has the same key,
 * the new item replaces the previous one
 * @param {any} awsConfig - JSON with AWS configuration
 * @param {string} tableName - Name of table
 * @param {any} item - JSON with item attributes
 */
exports.put = async function put(awsConfig, tableName, item) {
    try {
        let awsOptions = awsUtils.getDynamoDbOptions(awsConfig);
        let docClient = new DynamoDB.DocumentClient(awsOptions);
        let params = {
            TableName: tableName,
            Item: item
        };
        await docClient.put(params).promise();
    } catch (err) {
        throw new Errors.AwsError('CODE-61-0213 Error putting data into DynamoDB', err);
    }
};

exports.update = async function updateCustomer(awsConfig, tableName, options) {
    try {
        let awsOptions = awsUtils.getDynamoDbOptions(awsConfig);
        let docClient = new DynamoDB.DocumentClient(awsOptions);
        let params = {
            TableName: tableName,
            Key: options.key,
            UpdateExpression: options.expression,
            ExpressionAttributeValues: options.attributes,
            ReturnValues: 'UPDATED_NEW'
        };
        await docClient.update(params).promise();
        return true;
    } catch (err) {
        throw new Errors.AwsError('CODE-61-0209 Error updating data into DynamoDB', err);
    }
};
