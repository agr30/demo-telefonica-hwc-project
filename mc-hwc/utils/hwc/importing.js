'use strict';

const tar = require('tar-stream');
const zlib = require('zlib');
const config = require('../../commons/config');
const s3Client = require('../../utils/s3/client');
const Errors = require('../../commons/errors');
const stripBomStream = require('strip-bom-stream');

exports.importsFile = async function(date) {
    try {
        if (!date) {
            throw new Errors.BadParameterError('Error in date param', 'Missing parameter date');
        }
        let expectedFile = config.obHuawei.huaweiFilePrefix + date + config.huaweiFileSufix;
        let huaweiConfig = {
            accessKeyId: config.obHuawei.account.accessKeyId,
            secretAccessKey: config.obHuawei.account.secretAccessKey,
            endpoint: config.obHuawei.endpoint
        };
        let existtar = await s3Client.existsObject(huaweiConfig, config.obHuawei.originBucket, expectedFile);
        let tarFile = existtar ? expectedFile : undefined;
        if (!tarFile) {
            return false;
            // throw new Errors.BadParameterError('Error in tar file', `No file to untar for this date ${month}-${year} and generate billings from`);
        }
        let dataStream = await s3Client.getObjectStream(huaweiConfig, config.obHuawei.originBucket, tarFile);
        let gunzip = zlib.createGunzip();
        let extract = tar.extract();
        dataStream.pipe(gunzip).pipe(extract);

        await new Promise((resolve, reject) => {
            extract.on('entry', function(header, inputStream, next) {
                if (!header.name.startsWith('./.')) {
                    // header is the tar header
                    // stream is the content body (might be an empty stream)
                    // call next when you are done with this entry
                    let uploadStream = s3Client.getUploadStream(config.AWS_ACCOUNT_CONFIG, config.temporalBucket, header.name);
                    inputStream.pipe(stripBomStream()).pipe(uploadStream);
                    inputStream.on('error', reject);
                    uploadStream.on('uploaded', function() {
                        next(); // ready for next entry
                    });
                    inputStream.resume(); // just auto drain the stream
                } else {
                    next();
                }
            });

            dataStream.on('error', reject);
            gunzip.on('error', reject);
            extract.on('error', reject);
            extract.on('finish', resolve);
            extract.on('end', resolve);
        });
        return true;
    } catch (err) {
        throw new Errors.BillingError('CODE-61-0243 Error importing files from huawei cloud', err);
    }
};
