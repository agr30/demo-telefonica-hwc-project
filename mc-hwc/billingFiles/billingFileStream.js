const Stream = require('stream');
const BaseFile = require('./baseFile');

class BillingFileStream extends Stream.Transform {
    constructor(file) {
        if (!(file instanceof BaseFile)) {
            throw new Error('BillingFileStream constructor must receive a BillingFile');
        }
        super({
            writableObjectMode: true,
            transform: function(record, encode, callback) {
                let fileLine = file._getRecordLine(record);
                if (fileLine) {
                    let lineBuffer = Buffer.from(fileLine, 'utf8');
                    this.push(lineBuffer); // Write to stream
                }
                callback();
            },
            final: function(callback) {
                file._endFile();
                callback();
            }
        });
    }
}

module.exports = BillingFileStream;
