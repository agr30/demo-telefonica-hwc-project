const config = require('../commons/config');
const utils = require('../commons/utils');
const s3Client = require('../utils/s3/client');
const language = require('../i18n');
const BillingFile = require('./billingFile');
const BillingFileStream = require('./billingFileStream');

class AccountInvoiceFile extends BillingFile {
    constructor(user, billingDate) {
        let filename =
            'hwc_client_' +
            config.ob + config.delimiter +
            user.accountId + config.delimiter +
            user.subscriptionId + config.delimiter +
            utils.formatDate(billingDate, 'YYYYMMDDHHmmss') +
            '.csv';
        super(billingDate, filename);
        this.user = user;
    }

    async _initFile() {
        await this._init();
        let languageStrings = language.getStrings(config.lang);

        this.fileStream = new BillingFileStream(this);
        s3Client.uploadStream(config.AWS_ACCOUNT_CONFIG, config.finalBucket, this.filename, this.fileStream);
        let invoiceHeaders = [
            languageStrings.accountIdCsb,
            languageStrings.subscriptionIdCsb,
            languageStrings.chargeType,
            languageStrings.region,
            languageStrings.service,
            languageStrings.resourceName,
            languageStrings.resourceId,
            languageStrings.price,
            languageStrings.quantity,
            languageStrings.unit,
            languageStrings.discount,
            languageStrings.charge,
            languageStrings.currency,
            languageStrings.startDate,
            languageStrings.endDate,
            languageStrings.invoiceDate
        ];
        if (config.includeRef) {
            invoiceHeaders.push('priceRef');
            invoiceHeaders.push('chargeRef');
            invoiceHeaders.push('currencyRef');
        }
        let invoiceHeadersString = invoiceHeaders.join(config.csvDelimiter) + '\n';
        this.fileStream.push(invoiceHeadersString);
    }

    _getRecordLine(reportRecord) {
        let billingMode = reportRecord['Billing mode'];
        if (billingMode === '0' || billingMode === '1') {
            let price = reportRecord['Official website price'];
            let quantity = 1;
            if (billingMode === '1') {
                price = (price / reportRecord['Usage']) || 0;
                quantity = reportRecord['Usage'] || 0;
            }
            price = parseFloat(price);
            let invoiceLine = [
                '"' + this.user.accountId + '"',
                '"' + this.user.subscriptionId + '"',
                '"' + 'Usage' + '"',
                '"' + (reportRecord['Cloud service region code'] || '-') + '"',
                '"' + (reportRecord['Product type name'] || '-') + '"',
                '"' + (reportRecord['Product name'] || '-') + '"',
                '"' + (reportRecord['Product ID'] || '-') + '"',
                price * this.user.correctionFactor * config.exchangeRate,
                quantity,
                '"' + (this.getUnit(reportRecord['Usage Unit']) || '-') + '"',
                '"' + (reportRecord['Cash coupon amount'] || 0) + '"',
                parseFloat((reportRecord['Official website price'] - reportRecord['Cash coupon amount']) || 0) * this.user.correctionFactor * config.exchangeRate,
                '"' + config.obCurrency + '"',
                '"' + (utils.formatDateFromString((reportRecord['Fee generation time']), 'YYYY-MM-DDTHH:mm:ss[Z]') || '-') + '"',
                '"' + (utils.nextMonthDate((reportRecord['Fee generation time']), 'YYYY-MM-DDTHH:mm:ss[Z]') || '-') + '"',
                '"' + (utils.nextMonthDate((reportRecord['Fee generation time']), 'YYYY-MM-DDTHH:mm:ss[Z]') || '-') + '"'
            ];

            if (config.includeRef) {
                invoiceLine.push(price * this.user.correctionFactor);
                invoiceLine.push(parseFloat((reportRecord['Official website price'] - reportRecord['Cash coupon amount']) || 0) * this.user.correctionFactor),
                invoiceLine.push('"' + config.refCurrency + '"');
            }

            let invoiceLineString = invoiceLine.join(config.csvDelimiter) + '\n';
            return invoiceLineString;
        }
        return;
    }
}


async function getFile(user, billingDate) {
    let accountInvoiceFile = new AccountInvoiceFile(user, billingDate);
    await accountInvoiceFile._initFile();
    return accountInvoiceFile;
};

exports.getFile = getFile;

async function getFileStream(user, billingDate) {
    let file = await getFile(user, billingDate);
    return file.getFileStream();
}

exports.getFileStream = getFileStream;
