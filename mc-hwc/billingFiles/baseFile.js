
class BaseFile {
    constructor(filename) {
        this.filename = filename;
        this.init = false;
        this.end = false;
    }

    _init() {
        if (this.init) {
            throw new Error('File is already initialized');
        }
        if (this.end) {
            throw new Error('File is already finished');
        }
        this.init = true;
    }

    _verifyFile() {
        if (!this.init) {
            throw new Error('File is not initialized');
        }
        if (this.end === true) {
            throw new Error('File is already finished');
        }
    }

    _endFile() {
        if (!this.init) {
            throw new Error('File is not initialized');
        }
        if (this.end === true) {
            throw new Error('File is already finished');
        }
        this.end = true;
    }

    _getRecordLine() {
        throw new Error('Method _getRecordLine not implemented');
    }

    addRecord(reportRecord) {
        this._verifyFile();
        this.fileStream.write(reportRecord);
    }

    addAllRecords(reportRecords) {
        for (let reportRecord of reportRecords) {
            this.addRecord(reportRecord);
        }
    }

    async send() {
        this._verifyFile();
        this.fileStream.push(null);
        await new Promise(resolve => this.fileStream.on('uploaded', resolve));
    }

    getFileStream() {
        this._verifyFile();
        return this.fileStream;
    }

    getUnit(code) {
        const UsageUnitCodeToString = {
            0: 'DAY',
            1: 'YUAN',
            2: 'JIAO',
            3: 'FEN',
            4: 'Hour',
            5: 'MINUTE',
            6: 'SECOND',
            7: 'EB',
            8: 'PB',
            9: 'TB',
            10: 'GB',
            11: 'MB',
            12: 'KB',
            13: 'Byte',
            14: 'amount',
            15: 'Mbps',
            16: 'Byte',
            17: 'GB',
            18: 'KLOC',
            19: 'YEAR',
            20: 'MONTH',
            21: 'MB',
            22: 'GHz',
            23: 'CORE',
            24: 'DAY',
            25: 'HOUR',
            30: 'number',
            31: 'Thousand times',
            32: 'Million times',
            33: 'Billion times',
            34: 'bps',
            35: 'kbps',
            36: 'Mbps',
            37: 'Gbps',
            38: 'Tbps',
            39: 'GB-seconds',
            40: 'times',
            41: 'PCS',
            42: 'Thousand PCS',
            43: 'PCS',
            44: 'Thousand PCS',
            45: 'Query Per Second',
            46: 'Man/Day',
            47: 'TB',
            48: 'PB'
        };
        return UsageUnitCodeToString[code]
    }
}

module.exports = BaseFile;
