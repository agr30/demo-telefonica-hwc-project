const config = require('../commons/config');
const utils = require('../commons/utils');
const s3Client = require('../utils/s3/client');
const BillingFile = require('./billingFile');
const BillingFileStream = require('./billingFileStream');

class AccountDetailedInvoiceFile extends BillingFile {
    constructor(user, billingDate) {
        let filename =
            'hwc_detail_' +
            config.ob + config.delimiter +
            user.accountId + config.delimiter +
            user.subscriptionId + config.delimiter +
            utils.formatDate(billingDate, 'YYYYMMDDHHmmss') +
            '.csv';
        super(billingDate, filename);
        this.user = user;
    }

    async _initFile() {
        await super._init();
        this.fileStream = new BillingFileStream(this);
        s3Client.uploadStream(config.AWS_ACCOUNT_CONFIG, config.finalBucket, this.filename, this.fileStream);
        let detailedInvoiceHeaders = [
            'ob',
            'accountIdCsb',
            'subscriptionIdCsb',
            'customerId',
            'chargeType',
            'region',
            'service',
            'resourceName',
            'resourceId',
            'price',
            'quantity',
            'unit',
            'discount',
            'charge',
            'currency',
            'startDate',
            'endDate',
            'invoiceDate',
            'priceRef',
            'chargeRef',
            'currencyRef'
        ];
        let detailedInvoiceHeadersString = detailedInvoiceHeaders.join(config.csvDelimiter) + '\n';
        this.fileStream.push(detailedInvoiceHeadersString);
    }

    _getRecordLine(reportRecord) {
        let billingMode = reportRecord['Billing mode'];
        if (billingMode === '0' || billingMode === '1') {
            let price = reportRecord['Official website price'];
            let quantity = 1;
            if (billingMode === '1') {
                price = (price / reportRecord['Usage']) || 0;
                quantity = reportRecord['Usage'] || 0;
            }
            price = parseFloat(price);
            let detailedInvoiceLine = [
                '"' + config.ob + '"',
                '"' + this.user.accountId + '"',
                '"' + this.user.subscriptionId + '"',
                '"' + this.user.customerId + '"',
                '"' + 'Usage' + '"',
                '"' + (reportRecord['Cloud service region code'] || '-') + '"',
                '"' + (reportRecord['Product type name'] || '-') + '"',
                '"' + (reportRecord['Product name '] || '-') + '"',
                '"' + (reportRecord['Product ID'] || '-') + '"',
                price * this.user.correctionFactor * config.exchangeRate,
                quantity,
                '"' + (this.getUnit(reportRecord['Usage Unit']) || '-') + '"',
                '"' + (reportRecord['Cash coupon amount'] || 0) + '"',
                parseFloat((reportRecord['Official website price'] - reportRecord['Cash coupon amount']) || 0) * this.user.correctionFactor * config.exchangeRate,
                '"' + config.obCurrency + '"',
                '"' + (utils.formatDateFromString((reportRecord['Fee generation time']), 'YYYY-MM-DDTHH:mm:ss[Z]') || '-') + '"',
                '"' + (utils.nextMonthDate((reportRecord['Fee generation time']), 'YYYY-MM-DDTHH:mm:ss[Z]') || '-') + '"',
                '"' + (utils.nextMonthDate((reportRecord['Fee generation time']), 'YYYY-MM-DDTHH:mm:ss[Z]') || '-') + '"',
                price * this.user.correctionFactor,
                parseFloat((reportRecord['Official website price'] - reportRecord['Cash coupon amount']) || 0) * this.user.correctionFactor,
                '"' + config.refCurrency + '"'
            ];
            let detailedInvoiceLineString = detailedInvoiceLine.join(config.csvDelimiter) + '\n';
            return detailedInvoiceLineString;
        }
        return;
    }
}

async function getFile(user, billingDate) {
    let accountDetailedInvoiceFile = new AccountDetailedInvoiceFile(user, billingDate);
    await accountDetailedInvoiceFile._initFile();
    return accountDetailedInvoiceFile;
};

exports.getFile = getFile;

async function getFileStream(user, billingDate) {
    let file = await getFile(user, billingDate);
    return file.getFileStream();
}

exports.getFileStream = getFileStream;
