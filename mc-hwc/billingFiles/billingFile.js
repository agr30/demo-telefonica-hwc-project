const config = require('../commons/config');
const s3Client = require('../utils/s3/client');
const BaseFile = require('./baseFile');

class BillingFile extends BaseFile {
    constructor(billingDate, filename) {
        super(filename);
        this.billingDate = billingDate;
    }

    async _init() {
        super._init();
        let existsPreviousBillingFile = await s3Client.existsObject(config.AWS_ACCOUNT_CONFIG, config.finalBucket, this.filename);
        if (existsPreviousBillingFile) {
            await s3Client.copyObject(config.AWS_ACCOUNT_CONFIG, config.finalBucket, '/' + config.finalBucket + '/' + this.filename, 'old_' + Date.now() + '_' + this.filename);
            await s3Client.deleteObject(config.AWS_ACCOUNT_CONFIG, config.finalBucket, this.filename);
        }
    }
}

module.exports = BillingFile;
