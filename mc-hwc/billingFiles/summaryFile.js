const config = require('../commons/config');
const utils = require('../commons/utils');
const s3Client = require('../utils/s3/client');
const BillingFile = require('./billingFile');
const BillingFileStream = require('./billingFileStream');

class SummaryFile extends BillingFile {
    constructor(billingDate) {
        let filename = 'hwc_summary_' + config.ob + config.delimiter + utils.formatDate(billingDate, 'YYYYMMDDHHmmss') + '.csv';
        super(billingDate, filename);
    }

    async _initFile() {
        await super._init();
        this.fileStream = new BillingFileStream(this);
        s3Client.uploadStream(config.AWS_ACCOUNT_CONFIG, config.finalBucket, this.filename, this.fileStream);
        let summaryHeaders = [
            'ob',
            'accountIdCsb',
            'subscriptionIdCsb',
            'customerId',
            'domain',
            'currency',
            'priceFactor',
            'charge',
            'chargeVendor',
            'invoiceDate',
            'chargeRef',
            'chargeVendorRef',
            'margin',
            'currencyRef',
            'exchangeRate'
        ];
        let summaryHeadersString = summaryHeaders.join(config.csvDelimiter) + '\n';
        this.fileStream.push(summaryHeadersString);
        return null;
    }

    _getRecordLine(record) {
        let summaryLine = [
            '"' + record['ob'] + '"',
            '"' + record['accountIdCsb'] + '"',
            '"' + record['subscriptionIdCsb'] + '"',
            '"' + record['customerId'] + '"',
            '"' + record['domain'] + '"',
            '"' + record['currency'] + '"',
            '"' + record['priceFactor'] + '"',
            '"' + record['charge'].toFixed(2) + '"',
            '"' + record['chargeVendor'].toFixed(2) + '"',
            '"' + record['invoiceDate'] + '"',
            '"' + record['chargeRef'].toFixed(2) + '"',
            '"' + record['chargeVendorRef'].toFixed(2) + '"',
            '"' + record['margin'].toFixed(2) + '"',
            '"' + record['currencyRef'] + '"',
            '"' + record['exchangeRate'] + '"'
        ];
        let summaryLineString = summaryLine.join(config.csvDelimiter) + '\n';
        return summaryLineString;
    }
}

async function getFile(billingDate) {
    let summaryFile = new SummaryFile(billingDate);
    await summaryFile._initFile();
    return summaryFile;
};

exports.getFile = getFile;

async function getFileStream(billingDate) {
    let file = await getFile(billingDate);
    return file.getFileStream();
};

exports.getFileStream = getFileStream;
