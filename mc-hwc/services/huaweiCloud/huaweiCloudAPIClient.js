'use strict';

const logger = require('logops');
const signer = require('../../utils/huaweiCloud/security/signer');
const config = require('../../commons/config');
const rp = require('request-promise-native');
const generator = require('generate-password');
const request = require('request');

require('request-debug')(request, function(type, data, r) {
    var toLog = {};
    toLog[type] = data;
    logger.debug(toLog);
});

exports.getHuaweiToken = async function getHuaweiToken() {
    let tokenBody = {
        'auth': {
            'identity': {
                'methods': [
                    'password'
                ],
                'password': {
                    'user': {
                        'name': config.obHuawei.account.huaweiName, // from secretManager
                        'password': config.obHuawei.account.huaweiPassword, // from secretManager
                        'domain': {
                            'name': config.obHuawei.account.huaweiName
                        }
                    }
                }
            },
            'scope': {
                'domain': {
                    'name': config.obHuawei.account.huaweiName
                }
            }
        }
    };
    let token;
    let options = {
        'method': 'POST',
        'uri': config.obHuawei.IAMEndpoint,
        'json': true,
        'body': tokenBody,
        transform: function(body, response, resolveWithFullResponse) {
            return response.headers;
        }
    };
    token = await apiCallHuawei(options);
    if (token) {
        token = token['x-subject-token'];
    } else {
        logger.error('Error getting token param');
    }
    return token;
};

const apiCallHuawei = exports.apiCallHuawei = async function apiCallHuawei(options) {
    return await rp(options); // TODO: Refactor. Don't return await functions.
};

exports.generatePassword = function generatePassword() {
    let password = generator.generate({
        length: 11,
        numbers: true
    });
    return password;
};

function getToken(config, context, clientIP, callback) {
    // timeStamp: Marca de tiempo con el formato “yyyy-MM-dd HH:mm:ss.SSS”
    // mac: SHA(body + timeStamp + secreto)
    logger.info('huaweiAPIClient: getToken for context ' + JSON.stringify(context));

    var authorization = {
        'user': config.huawei.user,
        'pass': config.huawei.pass
    };

    var body = {
        'accountID': context.accountId,
        'OB_ID': config.ob,
        'name': context.name,
        'surname': context.surname,
        'phone': context.phone,
        'email': context.email,
        'tienda': config.huawei.tienda,
        'additionalData': context.additionalData
    };

    if (clientIP) {
        body.IP = clientIP;
        logger.debug('BODY WITH IP: ' + JSON.stringify(body));
    }

    var bodyString = JSON.stringify(body);
    var timeStamp = signer.generateTimestamp(true);
    logger.debug('huaweiAPIClient: bodyString ' + bodyString);
    logger.debug('huaweiAPIClient: timeStamp ' + timeStamp);

    var payload = {
        body: bodyString,
        timeStamp: timeStamp
    };

    var queryString = signer.generateQuery(payload);
    var mac = signer.generateSignature(queryString, config.huawei.secret);

    var huaweiHeaders = {
        timeStamp: timeStamp,
        mac: mac
    };

    let options = {};
    if (process.env.fake_huawei) {
        options = {
            uri: config.huawei.apiRoot + '/getToken',
            method: 'POST',
            body: bodyString,
            headers: huaweiHeaders
        };
    } else {
        options = {
            uri: config.huawei.apiRoot + '/getToken',
            method: 'POST',
            body: bodyString,
            headers: huaweiHeaders,
            auth: authorization
        };
    }

    request(options, function(error, response, body) {
        if (error) {
            logger.error('huaweiCloud.getToken:' + error);
            callback(error);
        } else if (response.statusCode === 200) {
            let result = JSON.parse(body);
            result.mac = mac;
            logger.debug('huaweiCloud.getToken:' + JSON.stringify(result));
            callback(null, result);
        } else {
            if (process.env.fake_huawei) {
                let result = { 'token': 'fake_token' };
                callback(null, result);
            } else {
                logger.error('huaweiCloud.getToken:' + body);
                try {
                    let err = JSON.parse(body);
                    err.error.code = response.statusCode;
                } catch (e) {
                    let err = { error: { message: body } };
                    err.error.code = response.statusCode;
                }
                logger.error('huaweiCloud.getToken. err:' + error);
                callback(error);
            }
        }
    });
}

function purchase(config, accountId, subscriptionId, purchaseItems, token, callback) {
    // timeStamp: Marca de tiempo con el formato “yyyy-MM-dd HH:mm:ss.SSS”
    // mac: SHA(body + timeStamp + secreto)
    logger.info('Purchase order for accountId ' + JSON.stringify(accountId));

    let authorization = {
        'user': config.huawei.user,
        'pass': config.huawei.pass
    };

    let body = {
        order: {
            'accountID': accountId,
            'subscriptionID': subscriptionId,
            'OB_ID': config.ob,
            'token': token.token,
            'items': purchaseItems
        }
    };

    let bodyString = JSON.stringify(body);
    let timeStamp = signer.generateTimestamp(true);

    let payload = {
        body: bodyString,
        timeStamp: timeStamp
    };

    let queryString = signer.generateQuery(payload);
    let mac = signer.generateSignature(queryString, config.huawei.secret);

    let huaweiHeaders = {
        timeStamp: timeStamp,
        mac: mac
    };

    let options = {
        uri: config.huawei.apiRoot + '/purchase/' + accountId,
        method: 'POST',
        body: bodyString,
        headers: huaweiHeaders,
        auth: authorization
    };

    if (process.env.fake_huawei) {
        let jBody = { order: { orderID: 'orderId-001', status: 'ActivationInProgress' } };
        logger.debug('huaweiCloud.purchase:' + jBody);
        callback(null, jBody);
    } else {
        request(options, function(error, response, body) {
            if (error) {
                logger.error('huaweiCloud.purchase:' + error);
                callback(error);
            } else if (response.statusCode === 202) {
                let jBody = JSON.parse(body);
                logger.debug('huaweiCloud.purchase:' + body);
                callback(null, jBody);
            } else {
                logger.error('huaweiCloud.purchase:' + body);
                try {
                    let err = JSON.parse(body);
                    err.error.code = response.statusCode;
                } catch (e) {
                    let err = { error: { message: body } };
                    err.error.code = response.statusCode;
                }
                logger.error('huaweiCloud.purchase. err:' + error);
                callback(error);
            }
        });
    }
}

function getOrderStatus(config, accountId, orderId, callback) {
    logger.debug('###getOrderStatus### accountId: ' + accountId + ' orderId: ' + orderId);

    var authorization = {
        'user': config.huawei.user,
        'pass': config.huawei.pass
    };

    var timeStamp = signer.generateTimestamp(true);

    var payload = {
        accountID: accountId,
        orderID: orderId,
        timeStamp: timeStamp
    };

    var queryString = signer.generateQuery(payload);
    var mac = signer.generateSignature(queryString, config.huawei.secret);

    var huaweiHeaders = {
        timeStamp: timeStamp,
        mac: mac
    };

    var options = {
        uri: config.huawei.apiRoot + '/order/' + orderId + '/status?accountId=' + accountId,
        method: 'GET',
        headers: huaweiHeaders,
        auth: authorization,
        json: payload
    };

    if (process.env.fake_huawei) {
        var jbody = { order: { orderID: 'orderId-001', status: 'ACTIVATED' } };
        logger.debug('huaweiCloud.purchase:' + jbody);
        callback(null, jbody);
    } else {
        request(options, function(error, response, body) {
            if (error) {
                logger.error('huaweiCloud.getOrder:' + error);
                callback(error);
            } else if (response.statusCode === 200) {
                logger.debug('huaweiCloud.getOrder:' + JSON.stringify(body));
                callback(null, JSON.parse(JSON.stringify(body)));
            } else {
                logger.error('huaweiCloud.getOrder:' + JSON.stringify(body));
                callback(body);
            }
        });
    }
}

function updateOrderStatus(config, accountId, orderId, action, callback) {
    let authorization = {
        'user': config.huawei.user,
        'pass': config.huawei.pass
    };

    let timeStamp = signer.generateTimestamp(true);

    let body = {
        accountID: accountId,
        OB_ID: config.ob,
        orderID: orderId,
        action: action
    };

    let bodyString = JSON.stringify(body);

    let payload = {
        body: bodyString,
        timeStamp: timeStamp
    };

    let queryString = signer.generateQuery(payload);
    let mac = signer.generateSignature(queryString, config.huawei.secret);

    let huaweiHeaders = {
        timeStamp: timeStamp,
        mac: mac
    };

    let options = {
        uri: config.huawei.apiRoot + '/order/' + orderId + '/status',
        method: 'PUT',
        headers: huaweiHeaders,
        auth: authorization,
        json: body
    };

    if (process.env.fake_huawei) {
        let jBody = { order: {} };
        logger.debug('huaweiCloud.updateOrderStatus:' + jBody);
        callback(null, jBody);
    } else {
        request(options, function(error, response, body) {
            if (error) {
                logger.error('huaweiCloud.updateOrderStatus:' + error);
                callback(error);
            } else if (response.statusCode === 202 || response.statusCode === 200) {
                logger.debug('huaweiCloud.updateOrderStatus:' + JSON.stringify(body));
                callback(null, body);
            } else {
                logger.error('huaweiCloud.updateOrderStatus:' + JSON.stringify(body));
                try {
                    var err = JSON.parse(body);
                    err.error.code = response.statusCode;
                } catch (e) {
                    var err = { error: { message: body } };
                    err.error.code = response.statusCode;
                }
                logger.error('huaweiCloud.updateOrderStatus. err:' + err);
                callback(err);
            }
        });
    }
}

function getOrderId(response, callback) {
    logger.info('ORDER:', JSON.stringify(response));
    logger.info('ORDERID:', response.order.orderID);
    return callback(null, response.order.orderID);
}

function suspendOrder(config, accountId, orderId, callback) {
    logger.info('suspendOrder:' + ' accountId:' + accountId + ' orderId:' + orderId);
    return updateOrderStatus(config, accountId, orderId, 'SUSPEND', callback);
}

function resumeOrder(config, accountId, orderId, callback) {
    logger.info('resumeOrder:' + ' accountId:' + accountId + ' orderId:' + orderId);
    return updateOrderStatus(config, accountId, orderId, 'RESUME', callback);
}

function cancelOrder(config, accountId, orderId, callback) {
    logger.info('cancelOrder:' + ' accountId:' + accountId + ' orderId:' + orderId);
    return updateOrderStatus(config, accountId, orderId, 'CANCEL', callback);
}

exports.getToken = getToken;
exports.purchase = purchase;
exports.getOrderId = getOrderId;
exports.getOrderStatus = getOrderStatus;
exports.suspendOrder = suspendOrder;
exports.resumeOrder = resumeOrder;
exports.cancelOrder = cancelOrder;
