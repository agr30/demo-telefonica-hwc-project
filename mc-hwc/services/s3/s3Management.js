'use strict';

const logger = require('logops');
const AWS = require('aws-sdk');

function listFiles(config, pattern, callback) {
    logger.debug('s3Management.listFiles begins');

    let s3 = new AWS.S3({
        accessKeyId: config.AWS_ACCOUNT_CONFIG.accessKeyId,
        secretAccessKey: config.AWS_ACCOUNT_CONFIG.secretAccessKey,
        sessionToken: config.AWS_ACCOUNT_CONFIG.sessionToken
    });

    let bucket = config.finalBucket;

    // Call S3 to list current buckets
    let params = {
        Bucket: bucket,
        Delimiter: '/',
        Prefix: pattern
    };

    let allKeys = [];

    s3.listObjectsV2(params, function(err, data) {
        if (err) {
            logger.error('s3Management.listFiles. pattern ' + pattern + ' error: ', err);
            callback(err);
        } else {
            logger.debug('s3Management.listFilesPattern ' + pattern + ' files back: ', data);
            data.Contents.forEach(function(dataKey) {
                // Filter folders, only allow files
                if ((dataKey.Key.lastIndexOf('/') !== dataKey.Key.length - 1)) {
                    logger.debug('File: ', dataKey.Key);
                    allKeys.push(dataKey.Key);
                }
            });
            let numberOfElementsPattern = allKeys.length;
            logger.debug('Number of Files obtained with pattern ' + pattern + ' is ' + numberOfElementsPattern);
            callback(null, allKeys);
        }
    });
}

exports.getFileFromBucket = function getFileFromBucket(config, domainId, pattern, callback) {
    logger.debug('s3Management.getFileFromBucket begins');

    let s3 = new AWS.S3({
        accessKeyId: config.AWS_ACCOUNT_CONFIG.accessKeyId,
        secretAccessKey: config.AWS_ACCOUNT_CONFIG.secretAccessKey,
        sessionToken: config.AWS_ACCOUNT_CONFIG.sessionToken
    });

    // Call S3 to list current buckets
    let params = {
        Bucket: config.finalBucket,
        Delimiter: '/',
        Prefix: pattern
    };

    s3.listObjectsV2(params, function(err, data) {
        if (err) {
            logger.error('s3Management.getFileFromBucket. error: ', err);
            callback(err);
        } else {
            logger.debug('s3Management.getFileFromBucket: ', JSON.stringify(data));
            if (!data.Contents[0]) {
                let errorMessage = 'No Files with pattern ' + pattern + ' in bucket ' + config.finalBucket;
                logger.error('retrieve.listing files from s3 ERROR: ', errorMessage);
                let err = { error: { message: errorMessage } };
                err.error.code = 404;
                return callback(err);
            }

            let filename = data.Contents[0].Key;
            logger.debug('s3Management.getFileFromBucket. Filename: ', filename);

            logger.info('getTotalAmountForContextId file from s3 %s', filename);

            readContent(config, filename, function(err, data) {
                if (err) {
                    logger.error('getTotalAmountForContextId files from s3 ERROR: ', err);
                    return callback(err, generateResponse(500, { error: err, message: '' }));
                }
                logger.debug('getTotalAmountForContextId files data: ', data.body.toString());
                let totalAmount = getTotalAmountFromSummary(domainId, data.body.toString());
                if (totalAmount === 'NOT FOUND') {
                    return callback(null, generateResponse(404, { error: ' Not Found', message: 'customerId ' + domainId + ' not found in CSV' + filename }));
                }
                return callback(null, totalAmount);
            });
        }
    });
};

function readContent(config, csvKey, callbackReading) {
    logger.debug('s3Management.readContent begins csvKey %s', csvKey);
    let s3 = new AWS.S3({
        accessKeyId: config.AWS_ACCOUNT_CONFIG.accessKeyId,
        secretAccessKey: config.AWS_ACCOUNT_CONFIG.secretAccessKey,
        sessionToken: config.AWS_ACCOUNT_CONFIG.sessionToken
    });

    s3.getObject({ Bucket: config.finalBucket, Key: csvKey }, function(error, data) {
        if (error) {
            logger.error('Failed to retrieve an object: ' + error);
            return callbackReading(error);
        } else {
            logger.debug('s3Management.CSV Content: ', data.Body.toString());
            logger.debug('s3Management.CSV Metadata: ', JSON.stringify(data.Metadata));

            return callbackReading(null, { body: data.Body, metadata: data.Metadata });
        }
    });
}

function getTotalAmountFromSummary(domainId, csvContent) {
    // TODO: Change to csvToJson
    logger.debug('csvContent: ', csvContent);
    // Fill the array with date from CSV
    logger.debug('domainId' + domainId);
    var arr = csvContent.toString().split('\n');
    logger.debug('arr: ', arr);
    var myData = [];
    for (var i = 0; i < arr.length; i++) {
        logger.debug('arr[' + i + ']:' + arr[i]);
        var arr2 = arr[i].toString().split(',');
        logger.debug('csvRow[' + i + ']:' + arr2);
        if (!((arr2 == '') || (arr2 == undefined))) {
            logger.debug('myData.push(' + i + '):' + arr2 + '.');
            myData.push(arr2);
        }
    }
    logger.debug('Arrays: ', myData);

    let totalAmount = 'NOT FOUND';
    // Find the charge at position 7
    // domainId (customerId) at position 3
    for (var j = 0; j < myData.length; j++) {
        if (myData[j] !== undefined) {
            if ((myData[j][3].replace(/\"/g, '') == domainId)) {
                logger.debug('Total amount: ' + myData[j][5]);
                totalAmount = myData[j][7];
            }
        }
    }
    return totalAmount;
}

exports.listBillingFiles = function listBillingFiles(config, accountId, subscriptionId, callback) {
    const csvNameDelimiter = config.delimiter;
    let pattern = 'hwc' + csvNameDelimiter + 'client' + csvNameDelimiter + config.ob + csvNameDelimiter + accountId + csvNameDelimiter + subscriptionId;
    logger.debug('listBillingFiles. pattern :' + pattern);
    let textJsonResponse = [];

    listFiles(config, pattern, function(err, files) {
        if (err) {
            logger.error(err);
            return callback(err);
        } else {
            logger.debug('s3Management.listBillingFiles. files encounter in bucket ' + config.finalBucket + '. Files: ' + files);
            logger.debug('s3Management.listBillingFiles. Number of files found ' + files.length);

            let numberOfFiles = files.length;
            let pending = 0;
            let contId = 0;
            let errorThrown = false;

            // There are not files in bucket
            if (numberOfFiles === 0) {
                logger.info('s3Management.processFile. NO CSV Files in bucket ' + config.finalBucket);
                return callback(null, []);
            }
            // Have to create a function that process all files to return a callback
            files.forEach(function(filename) {
                logger.debug('filename: ' + filename);
                pending++;
                readContent(config, filename, function(error, response) {
                    if (errorThrown) {
                        return;
                    }
                    if (error || !response.body) {
                        logger.error('s3Management.processFile. Error reading the CSV file');
                        errorThrown = true;
                        return callback('Error reading the CSV file');
                    } else {
                        pending--;
                        contId++;
                        logger.debug('s3Management.readContent filename ' + filename + ' response %s', response);
                        if (filename.indexOf('client') !== -1) {
                            var csvData = getInfoFromClient(config, accountId, subscriptionId, response.body);
                        } else {
                            return callback('No client or daily csv files FOUND in bucket ' + config.finalBucket);
                        }
                        let textJson = {
                            id: contId,
                            dateBegin: csvData.dateBegin,
                            dateEnd: csvData.dateEnd,
                            filePath: filename,
                            amount: csvData.amount,
                            currency: csvData.currency,
                            CSVcontent: response.body.toString('base64')
                        };

                        logger.debug('s3Management.listBillingFiles.filename: ' + filename + ' jsonResponse ' + textJson);
                        textJsonResponse.push(textJson);
                        logger.debug('s3Management.listBillingFiles. pending: ' + pending + ' textJsonResponse: ' + textJsonResponse);

                        if (pending === 0) {
                            logger.debug('s3Management.listBillingFiles. pending: ' + pending + ' textJsonResponse: ' + textJsonResponse.toString());
                            return callback(null, textJsonResponse);
                        }
                    }
                });
            });
        }
    });
};

function getInfoFromClient(config, accountId, subscriptionId, csvContent) {
    logger.debug('csvContent: ', csvContent);
    // Fill the array with date from CSV
    logger.debug('accountId: ' + accountId + ' subscriptionId: ' + subscriptionId);
    let arr = csvContent.toString().split('\n');
    logger.debug('arr: ', arr);
    let myData = [];
    for (let i = 0; i < arr.length; i++) {
        logger.debug('arr[' + i + ']:' + arr[i]);
        let arr2 = arr[i].toString().split(config.csvDelimiter);
        logger.debug('csvRow[' + i + ']:' + arr2);
        if (!((arr2 == '') || (arr2 == undefined))) {
            logger.debug('myData.push(' + i + '):' + arr2 + '.');
            myData.push(arr2);
        }
    }
    logger.debug('Arrays: ', myData);

    let dateBegin = 'NOT FOUND';
    let dateEnd = 'NOT FOUND';
    let amount = 0;
    let currency = '';
    // Find the charge at position 11, dateBegin at 13 and dateEnd at 14
    // organizationId (accountId) at position 0
    // realmName (subscriptionId) at position 1
    for (var j = 1; j < myData.length; j++) {
        if (myData[j] != undefined) {
            amount = parseInt(amount) + parseInt(myData[j][11]);
            currency = (myData[j][12].replace(/\"/g, '')).replace(/\n|\r/g, '');
            dateBegin = (myData[j][13].replace(/\"/g, '')).replace(/\n|\r/g, '');
            dateEnd = (myData[j][14].replace(/\"/g, '')).replace(/\n|\r/g, '');
            logger.debug('dateBegin: ' + dateBegin + ', dateEnd:' + dateEnd + ', amount:' + amount);
        }
    }
    return { dateBegin: dateBegin, dateEnd: dateEnd, amount: amount, currency: currency };
}

function generateResponse(statusCode, body) {
    // The output from a Lambda proxy integration must be
    // of the following JSON object. The 'headers' property
    // is for custom response headers in addition to standard
    // ones. The 'body' property  must be a JSON string. For
    // base64-encoded payload, you must also set the 'isBase64Encoded'
    // property to 'true'.
    return {
        statusCode: statusCode,
        body: JSON.stringify(body)
    };
}
