'use strict';

const texts = {
    es: {
        ob: 'ob',
        accountIdCsb: 'Id de organizacion (en CSB)',
        subscriptionIdCsb: 'Nombre de la suscripción (en CSB)',
        customerId: 'Id de dominio (en Huawei)',
        customerName: 'Nombre del cliente',
        domain: 'Dominio',
        currency: 'Moneda',
        priceFactor: 'Factor de precio (en %)',
        rebateFactor: 'Factor de rebate (en %)',
        charge: 'Cargo a cliente',
        chargeVendor: 'Cargo cobrado por Amazon',
        rebate_vendor: 'Rebate generado',
        invoiceDate: 'Fecha de factura',
        chargeRef: 'Cargo en moneda usada por Amazon',
        chargeVendorRef: 'Cargo cobrado por Amazon en moneda usada por AWS',
        rebate_vendor_ref: 'Rebate generado en moneda usada por AWS',
        currencyRef: 'Moneda usada por AWS',
        chargeType: 'Tipo de cargo',
        regionCode: 'Región',
        azCode: 'Zons de disponibilidad',
        cloudServiceTypeCode: 'Familia',
        resourceTypeCode: 'Categoría',
        resourceSpecCode: 'Nombre de recurso',
        resourceInstanceId: 'Id de recurso',
        productName: 'Nombre del producto',
        region: 'Región',
        service: 'Servicio',
        resourceName: 'Nombre de recurso',
        resourceId: 'Id de recurso',
        price: 'Precio unitario',
        quantity: 'Cantidad',
        unit: 'Unidad',
        billingUnit: 'Unidad',
        measureUnit: 'Unidad',
        startDate: 'Fecha de inicio',
        endDate: 'Fecha de fin',
        priceRef: 'Precio unitario en la moneda usada por AWS',
        cost: 'Coste',
        cost_ref: 'Coste  en moneda usada por MS',
        totalCount: 'Número de registros',
        discount: 'Cupón de descuento que hace Huawei por concepto facturable',
    },
    pt: {
        ob: 'ob',
        accountIdCsb: 'organizationId',
        subscriptionIdCsb: 'subscriptionId',
        customerId: 'customerId',
        customerName: 'customerName',
        domain: 'domain',
        currency: 'currency',
        priceFactor: 'priceFactor',
        rebateFactor: 'rebateFactor',
        charge: 'charge',
        chargeVendor: 'chargeVendor',
        rebate_vendor: 'rebate_vendor',
        invoiceDate: 'invoiceDate',
        chargeRef: 'chargeRef',
        chargeVendorRef: 'chargeVendorRef',
        rebate_vendor_ref: 'rebate_vendor_ref',
        currencyRef: 'currencyRef',
        chargeType: 'chargeType',
        region: 'region',
        service: 'service',
        regionCode: 'regionCode',
        azCode: 'azCode',
        cloudServiceTypeCode: 'cloudServiceTypeCode',
        resourceTypeCode: 'resourceTypeCode',
        resourceSpecCode: 'resourceSpecCode',
        resourceInstanceId: 'resourceInstanceId',
        productName: 'productName',
        billingUnit: 'billingUnit',
        measureUnit: 'measureUnit',
        resourceName: 'resourceName',
        resourceId: 'resourceId',
        price: 'price',
        quantity: 'quantity',
        unit: 'unit',
        startDate: 'startDate',
        endDate: 'endDate',
        priceRef: 'priceRef',
        cost: 'cost',
        cost_ref: 'cost_ref',
        totalCount: 'totalCount',
        discount: 'coupon_discount',
    },
    en: {
        ob: 'ob',
        accountIdCsb: 'organizationId',
        subscriptionIdCsb: 'subscriptionId',
        customerId: 'customerId',
        customerName: 'customerName',
        domain: 'domain',
        currency: 'currency',
        priceFactor: 'priceFactor',
        rebateFactor: 'rebateFactor',
        charge: 'charge',
        chargeVendor: 'chargeVendor',
        rebate_vendor: 'rebate_vendor',
        invoiceDate: 'invoiceDate',
        chargeRef: 'chargeRef',
        chargeVendorRef: 'chargeVendorRef',
        rebate_vendor_ref: 'rebate_vendor_ref',
        currencyRef: 'currencyRef',
        chargeType: 'chargeType',
        region: 'region',
        service: 'service',
        regionCode: 'regionCode',
        azCode: 'azCode',
        cloudServiceTypeCode: 'cloudServiceTypeCode',
        resourceTypeCode: 'resourceTypeCode',
        resourceSpecCode: 'resourceSpecCode',
        resourceInstanceId: 'resourceInstanceId',
        productName: 'productName',
        billingUnit: 'billingUnit',
        measureUnit: 'measureUnit',
        resourceName: 'resourceName',
        resourceId: 'resourceId',
        price: 'price',
        quantity: 'quantity',
        unit: 'unit',
        startDate: 'startDate',
        endDate: 'endDate',
        priceRef: 'priceRef',
        cost: 'cost',
        cost_ref: 'cost_ref',
        totalCount: 'totalCount',
        discount: 'coupon_discount',
    }
};

exports.getStrings = function getStrings(language) {
    if (language && texts[language.toLowerCase()]) {
        return texts[language.toLowerCase()];
    } else {
        return texts['es'];
    }
};
