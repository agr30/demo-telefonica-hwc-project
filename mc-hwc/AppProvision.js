'use strict';

var logger = require('logops');
var AWS = require('aws-sdk');
const config = require('./commons/config');

/**
 * Manage the app creation, creating the config folders and storing certificates and connection data.
 */
// appCreation lambda entry point
exports.appCreation = async function appCreation(event, context, callback) {
    try {
        logger.info('AppProvision.appCreation begins');
        logger.debug('Event: ', event);
        var stage = (event.requestContext) ? event.requestContext.stage || 'Prod' : 'Prod';
        logger.info('Stage: ', stage);
        await config.loadConfiguration(stage);

        var instanceId = event.headers['APS-Instance-ID'];
        logger.info('Creating new App with instanceId: ', instanceId);
        if (instanceId === null || instanceId === undefined) {
            return callback(generateResponse(400, { error: 'Error', message: 'Missing parameter instanceId' }));
        }

        var controllerUri = event.headers['APS-Controller-URI'];
        logger.info('Creating new App with controllerUri: ', controllerUri);
        if (controllerUri === null || controllerUri === undefined) {
            return callback(generateResponse(400, { error: 'Error', message: 'Missing parameter controllerUri' }));
        }

        logger.info('Creating new App with event.body: ', event.body);
        var body = JSON.parse(event.body);
        // var key = body.aps.x509.self

        var keyContent = body.aps.x509.self.split('-----END CERTIFICATE-----\n');
        logger.info('Creating new App with keyContent: ', keyContent);

        var key = keyContent[1];
        logger.info('Creating new App with key: ', key);
        var cert = keyContent[0] + '-----END CERTIFICATE-----\n';
        logger.info('Creating new App with cert: ', cert);

        var ca = body.aps.x509.controller;
        logger.info('Creating new App with ca: ', ca);

        // Last version private key, pub key and ca were stored in two different places
        // Now these three variables will be stored on database
        config.apsc.instanceId = instanceId;
        config.apsc.controllerUri = controllerUri;
        config.apsc.key = key;
        config.apsc.cert = cert;
        config.apsc.ca = ca;

        await config.updateConfiguration(stage);

        logger.info('App created');
        callback(null, generateResponse(200, { message: 'App created' }));
    } catch (err) {
        logger.error(err);
        return callback(null, generateResponse(500, { error: 'Internal error', message: 'An internal error has occurred during execution' }));
    }
}

/**
 * Remove an application, removing the certificates and connection data in the process.
 */
// appRemoval lambda entry point
exports.appRemoval = async function appRemoval(event, context, callback) {
    try {
        logger.debug('Event: ', event);
        const stage = (event.requestContext) ? event.requestContext.stage || 'Prod' : 'Prod';
        logger.info('Stage: ', stage);
        await config.loadConfiguration(stage);

        var instanceId = event.headers['APS-Instance-ID'];
        logger.info('Creating new App with instanceId: ', instanceId);
        if (instanceId === null || instanceId === undefined) {
            return callback(generateResponse(400, { error: 'Error', message: 'Missing parameter instanceId' }));
        }

        // Last version private key, pub key and ca were stored in two different places
        // Now these three variables will be stored on database
        config.apsc.instanceId = null;
        config.apsc.controllerUri = null;
        config.apsc.key = null;
        config.apsc.cert = null;
        config.apsc.ca = null;

        await config.updateConfiguration(stage);

        logger.info('App removed');
        callback(null, generateResponse(200, { message: 'App removed' }));
    } catch (err) {
        logger.error(err);
        return callback(null, generateResponse(500, { error: 'Internal error', message: 'An internal error has occurred during execution' }));
    }
}

/**
 * Update app creation, removing and after creating the config folders and storing certificates and connection data.
 */
// appUpdate lambda entry point
exports.appUpdate = async function appUpdate(event, context, callback) {
    try {
        logger.debug('Event: ', event);
        var stage = (event.requestContext) ? event.requestContext.stage || 'Prod' : 'Prod';
        logger.info('Stage: ', stage);
        await config.loadConfiguration(stage);

        var instanceId = event.headers['APS-Instance-ID'];
        logger.info('Creating new App with instanceId: ', instanceId);
        if (instanceId === null || instanceId === undefined) {
            return callback(generateResponse(400, { error: 'Error', message: 'Missing parameter instanceId' }));
        }

        var controllerUri = event.headers['APS-Controller-URI'];
        logger.info('Creating new App with controllerUri: ', controllerUri);
        if (controllerUri === null || controllerUri === undefined) {
            return callback(generateResponse(400, { error: 'Error', message: 'Missing parameter controllerUri' }));
        }

        var body = JSON.parse(event.body);
        // var key = body.aps.x509.self

        var keyContent = body.aps.x509.self.split('-----END CERTIFICATE-----\n');
        logger.info('Creating new App with keyContent: ', keyContent);

        var key = keyContent[1];
        logger.info('Creating new App with key: ', key);
        var cert = keyContent[0] + '-----END CERTIFICATE-----\n';
        logger.info('Creating new App with cert: ', cert);

        var ca = body.aps.x509.controller;
        logger.info('Creating new App with ca: ', ca);

        // Last version private key, pub key and ca were stored in two different places
        // Now these three variables will be stored on database
        config.apsc.instanceId = instanceId;
        config.apsc.controllerUri = controllerUri;
        config.apsc.key = key;
        config.apsc.cert = cert;
        config.apsc.ca = ca;

        await config.updateConfiguration(stage);

        logger.info('App updated');
        callback(null, generateResponse(200, { message: 'App updated' }));
    } catch (err) {
        logger.error(err);
        return callback(null, generateResponse(500, { error: 'Internal error', message: 'An internal error has occurred during execution' }));
    }
}

function generateResponse(statusCode, body) {
    // The output from a Lambda proxy integration must be
    // of the following JSON object. The 'headers' property
    // is for custom response headers in addition to standard
    // ones. The 'body' property  must be a JSON string. For
    // base64-encoded payload, you must also set the 'isBase64Encoded'
    // property to 'true'.
    return {
        statusCode: statusCode,
        body: JSON.stringify(body)
    };
}
