class BaseError extends Error {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
    }
}

class ConfigError extends BaseError {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
    }
}

class AwsError extends BaseError {
    constructor(message, err) {
        super(message);
        this.name = this.constructor.name;
        if (err) {
            this.stack += '\nFrom previous error: ' + err.stack;
        }
    }
}

class BillingError extends BaseError {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
    }
}

class ProvisionError extends BaseError {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
    }
}

class CurError extends BaseError {
    constructor(message) {
        super(message);
        this.name = this.constructor.name;
    }
}

class BadParameterError extends BaseError {
    constructor(error, info) {
        super(error + ': ' + info);
        this.error = error;
        this.info = info;
        this.name = this.constructor.name;
    }
}

class NotFoundError extends BaseError {
    constructor(error, info) {
        super(error + ': ' + info);
        this.error = error;
        this.info = info;
        this.name = this.constructor.name;
    }
}

exports.AwsError = AwsError;
exports.ConfigError = ConfigError;
exports.BillingError = BillingError;
exports.ProvisionError = ProvisionError;
exports.CurError = CurError;
exports.BadParameterError = BadParameterError;
exports.NotFoundError = NotFoundError;

