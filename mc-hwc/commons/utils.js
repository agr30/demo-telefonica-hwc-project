'use strict';
const logger = require('logops');
const moment = require('moment');

/**
 * Print in log the memory used
 * @param {string} codePoint
 */
function logMemoryUsed(codePoint) {
    var used = process.memoryUsage();
    logger.debug('Used memory ', codePoint);
    for (var key in used) {
        logger.debug('%s: %s MB', key, Math.round((used[key] / 1024 / 1024) * 100) / 100);
    }
}
exports.logMemoryUsed = logMemoryUsed;

/**
 * Get the period string from month and year as fist day of month - first day of next month in format YYYYMMDD-YYYYMMDD
 * @param {number} month
 * @param {number} year
 * @returns {string} PeriodString
 */
function getPeriodString(month, year) {
    let startDatePeriod = moment({ day: 1, month: month - 1, year: year }); // le restas 1 para q genere la fecha correcta (0-11), formato month(1-12)
    let endDatePeriod = moment(startDatePeriod).add(1, 'M');
    let monthPeriodString = startDatePeriod.format('YYYYMMDD') + '-' + endDatePeriod.format('YYYYMMDD');
    return monthPeriodString;
}
exports.getPeriodString = getPeriodString;

/**
 * Returns a Date as String with a given format
 * @param {string} date YYYY-MM
 * @param {string} format
 * @returns {string} Date formated
 */
function formatDate(date, format) {
    let momentDate = moment(date);
    return momentDate.format(format);
}
exports.formatDate = formatDate;

/**
 * Returns a Date as String with a given format
 * @param {string} date YYYY-MM
 * @param {string} format
 * @returns {string} Date formated
 */
function formatDateFromString(date, format) {
    let momentDate = moment(date + '-01');
    return momentDate.format(format);
}
exports.formatDateFromString = formatDateFromString;

/**
 * Returns the first day of next month of the period to billing with a given format
 * @param {string} date YYYY-MM
 * @param {string} format
 * @returns {string} Date formated
 */
function nextMonthDate(date, format) {
    let momentDate = moment(date + '-01');
    momentDate.month(momentDate.month() + 1);
    return momentDate.format(format);
}
exports.nextMonthDate = nextMonthDate;

/**
 * Returns a Date as String in UTC with a given format
 * @param {Date} date
 * @param {string} format
 * @returns {string} Date formatted
 */
function formatUTCDate(date, format) {
    let momentDate = moment(date);
    return momentDate.utc().format(format);
}
exports.formatUTCDate = formatUTCDate;

/**
 * Check if a date string have format YYYY/MM/DD
 * @param {string} dateString
 * @returns {boolean} Result
 */
function isValidDate(dateString) {
    let regex = /^\d{4}-\d{2}-\d{2}$/;
    if (!dateString.match(regex)) {
        return false;
    }
    var date = new Date(dateString);
    if (Number.isNaN(date.getTime())) {
        return false;
    }
    return true;
}
exports.isValidDate = isValidDate;

/**
 * Get the last closed billing period (month and year). A period is closed after the day of the month configured
 * @returns {{month:number, year:number}} Period
 */
function getLastClosedPeriod() {
    logger.info('getLastClosedPeriod');
    let lastClosedPeriodDate;
    let currentDate = new Date();
    lastClosedPeriodDate = new Date(currentDate.getFullYear(), currentDate.getMonth() - 1, 1);
    let lastClosedPeriod = {
        month: lastClosedPeriodDate.getMonth() + 1,
        year: lastClosedPeriodDate.getFullYear()
    };
    logger.debug('lastClosedPeriod:', lastClosedPeriod);
    return lastClosedPeriod; // returns date of the last generated billing
}

exports.getLastClosedPeriod = getLastClosedPeriod;

/**
 * Get billing period from a given offset
 * @param {number} offset
 * @returns {{month:number,year:number}} Period
 */
function getBillingPeriod(offset) {
    logger.debug('getBillingPeriod');
    if (process.env.offset !== undefined) {
        offset = process.env.offset;
    }
    if (!offset) {
        offset = 0;
    }
    let lastClosedPeriod = getLastClosedPeriod();
    let billingDate = new Date(lastClosedPeriod.year, lastClosedPeriod.month - offset);
    let billingPeriod = billingDate.getFullYear().toString() + (billingDate.getMonth() < 10 ? '0' + billingDate.getMonth() : billingDate.getMonth());
    logger.debug('billingPeriod:', billingPeriod);
    return billingPeriod;
}
exports.getBillingPeriod = getBillingPeriod;

// see http://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
exports.generateGUID = function () {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = (Math.random() * 16) | 0;
        var v = c === 'x' ? r : (r & 0x3) | 0x8;
        return v.toString(16);
    });
};

exports.getValidationMessage = function (validationResult) {
    if (validationResult.error) {
        if (validationResult.error.details) {
            return validationResult.error.details[0].message;
        } else {
            return 'Invalid object';
        }
    }
    return null;
};

exports.isValidJson = function isValidJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
};

exports.promesifyStream = function (stream, options = {}) {
    return new Promise((resolve, reject) => {
        const endEvent = options.endEvent || 'end';
        if (options.onData) {
            stream.on('data', options.onData);
        }
        if (options.onEnd) {
            stream.on(endEvent, () => {
                options.onEnd();
                resolve();
            });
        } else {
            stream.on(endEvent, resolve);
        }
        if (options.onError) {
            stream.on('error', err => {
                options.onError(err);
                reject(err);
            });
        } else {
            stream.on('error', reject);
        }
    });
};
