const ConfigError = require('./errors').ConfigError;
const dynamoDBClient = require('../utils/dynamoDb/client');
const secretsManagerClient = require('../utils/secretsmanager/client');
const logger = require('logops');
const utils = require('./utils');
const Joi = require('joi');

const AWS_ACCOUNT_CONFIG = {
    region: process.env.AWS_REGION,
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    sessionToken: process.env.AWS_SESSION_TOKEN
};
exports.AWS_ACCOUNT_CONFIG = AWS_ACCOUNT_CONFIG;

const dbConfigSchema = Joi.object().keys({
    ob: Joi.string().regex(/^[A-Z]+$/).min(2).max(2).required(),
    billingFileDate: Joi.object().required(),
    createContextUrl: Joi.string().required(),
    finalBucket: Joi.string().required(),
    temporalBucket: Joi.string().required(),
    lastHWCInvoiceDate: Joi.string().regex(/^(none)|([0-9]{6})$/).required(),
    lastHWCRetrieveDate: Joi.string().regex(/^(none)|([0-9]{6})$/).required(),
    taxRate: Joi.number().min(0).required(),
    refCurrency: Joi.string().regex(/^[A-Z]+$/).min(3).max(3).required(),
    obCurrency: Joi.string().regex(/^[A-Z]+$/).min(3).max(3).required(),
    exchangeRate: Joi.number().min(0).required(),
    includeRef: Joi.boolean().required(),
    lang: Joi.string().regex(/^[a-zA-Z]+$/).min(2).max(2).required(),
    delimiter: Joi.string().min(1).max(1).required(),
    csvDelimiter: Joi.string().min(1).max(1).required(),
    obHuawei: Joi.object().keys({
        createEndpoint: Joi.string().required(),
        creditLimit: Joi.number().required(),
        domainArea: Joi.string().required(),
        endpoint: Joi.string().required(),
        huaweiFilePrefix: Joi.string().required(),
        IAMEndpoint: Joi.string().required(),
        originBucket: Joi.string().required(),
        reportName: Joi.string().required(),
        reportPrefix: Joi.string().required(),
        secretName: Joi.string().required(),
        xAccountType: Joi.string().required()
    }).required(),
    queryingLimitUrl: Joi.string().required(),
    retrieveFileDate: Joi.object().required(),
    setLimitUrl: Joi.string().required(),
    smtp: Joi.object().keys({
        host: Joi.string().required(),
        port: Joi.number().required(),
        secure: Joi.boolean().required(),
        auth: Joi.object().keys({
            user: Joi.string().required(),
            pass: Joi.string().required()
        })
    }),
    version: Joi.string().required()
});

const awsSecretSchema = Joi.object().keys({
    accessKeyId: Joi.string().required(),
    secretAccessKey: Joi.string().required()
});

const huaweiSecretSchema = Joi.object().keys({
    accessKeyId: Joi.string().required(),
    secretAccessKey: Joi.string().required(),
    partnerId: Joi.string().required(),
    huaweiName: Joi.string().required(),
    huaweiPassword: Joi.string().required(),
    emailUser: Joi.string().required(),
    emailPassword: Joi.string().required()
});

async function loadConfiguration(stage) {
    let config = {};

    logger.debug('Load configuration');
    let dbConfig = await getDataStored(stage); // get the config of dynamo
    if (!dbConfig) {
        throw new ConfigError('CODE-61-0230 Error loading configuration: Configuration not loaded');
    }
    let dbConfigValidationResult = Joi.validate(dbConfig, dbConfigSchema, { allowUnknown: true });
    if (dbConfigValidationResult.error) {
        throw new ConfigError('CODE-61-0231 Error loading configuration ' + utils.getValidationMessage(dbConfigValidationResult));
    }

    Object.assign(config, dbConfig);

    let obHuaweiSecretName = dbConfig.obHuawei.secretName;
    let obHuaweiSecret = await secretsManagerClient.get(AWS_ACCOUNT_CONFIG, obHuaweiSecretName, stage);
    if (!obHuaweiSecret) {
        throw new ConfigError('CODE-61-0232 Error loading AWS secret: AWS Secret not loaded');
    }
    if (!utils.isValidJson(obHuaweiSecret)) {
        throw new ConfigError('CODE-61-0233 Error loading AWS secret: Malformed AWS Secret. Must be a JSON');
    }
    obHuaweiSecret = JSON.parse(obHuaweiSecret);

    let awsSecretValitationResult = Joi.validate(obHuaweiSecret, awsSecretSchema, { allowUnknown: true });
    if (awsSecretValitationResult.error) {
        throw new ConfigError('CODE-61-0234 Error loading AWS secret ' + utils.getValidationMessage(awsSecretValitationResult));
    }

    let huaweiSecretValidationResult = Joi.validate(obHuaweiSecret, huaweiSecretSchema, { allowUnknown: true });
    if (huaweiSecretValidationResult.error) {
        throw new ConfigError('CODE-61-0234 Error loading AWS secret ' + utils.getValidationMessage(huaweiSecretValidationResult));
    }

    config.obHuawei['account'] = obHuaweiSecret;
    config.smtp['auth'] = {
        user: obHuaweiSecret.emailUser,
        pass: obHuaweiSecret.emailPassword
    }
    config['executionId'] = utils.generateGUID();
    Object.assign(exports, config);
}
exports.loadConfiguration = loadConfiguration;

async function updateConfiguration(stage) {
    let config = exports;
    logger.debug('Update configuration. Current config in memory: ', config);
    let dbConfig = await getDataStored(stage);
    dbConfig['billingFileDate'] = config['billingFileDate'];
    dbConfig['retrieveFileDate'] = config['retrieveFileDate'];
    dbConfig['lastHWCInvoiceDate'] = config['lastHWCInvoiceDate'];
    dbConfig['lastHWCRetrieveDate'] = config['lastHWCRetrieveDate'];
    dbConfig['apsc'] = config['apsc'];
    logger.debug('dbConfig to update is: ', dbConfig);
    await updateDataStored(dbConfig, stage);
}
exports.updateConfiguration = updateConfiguration;

async function getDataStored(stage) {
    logger.debug('Get configuration from DynamoDB');
    const dbTable = process.env.DB_TABLE || 'mc-hwc-dev-HK-huaweiDataTable-16SRZVY7WMSLM';
    let key = {
        stage: stage
    };
    let config = await dynamoDBClient.get(AWS_ACCOUNT_CONFIG, dbTable, key);
    return config;
}

async function updateDataStored(config, stage) {
    logger.debug('Update configuration in DynamoDB');
    const dbTable = process.env.DB_TABLE || 'mc-hwc-dev-HK-huaweiDataTable-16SRZVY7WMSLM';
    await dynamoDBClient.put(AWS_ACCOUNT_CONFIG, dbTable, config);
    logger.info('Configuration updated in database');
}
