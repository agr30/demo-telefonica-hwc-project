# Demo Telefonica-Huawei Back-End Project
This repo is only intended to be a demo about my code experience in other projects.
It doesn't contains all the code, and is not deployable or installable for development.

Please, if you want a demo of, and I'm able to do it, contact me by e-mail.

## Explaination of demo
In thid repo you will find 2 folders: csb-price-server-cli & mc-hwc

### csb-price-server-cli Explaination
This project is a CLI based on **Vorpal** for manage, update and delete a price-server of Huawei Cloud reselling.

It's written in **Node.JS** and **Mongoose ORM**, with some libraries like *moment.js* and *csv* for read *.CSV files. The app is able to read, delete bootstrap and update the MongoDB-based database for selling Products with prices for many OB's.

### mc-hwc Explaination
This project is a **Node.JS** environment for use like endpoint and prebilling platform of Huawei Cloud reselling.

It's written in **Node.JS** and **AWS-SDK** with many libraries like *joi*, *csv* and others. Because is deployed over Amazon Web Services Cloud,
 and intended to be a serverless platform, you can watch in the code many operations with the AWS API using technologies like:
  - AWS DynamoDB
  - AWS CloudFormation
  - AWS CloudPipelines
  - AWS Secrets manager

As I said, this code is incomplete and only pretends to be a code demo of the entire project.

**If you have any doubt, don't hesitate in contact me at my e-mail. Thank you for watching**  